<?php

namespace frontend\controllers;

use common\helpers\HtmlSettingsHelper;
use common\models\Tariff;
use common\models\TariffToUser;
use common\models\Transaction;
use common\models\transactions\FinanceInvestMoneyTransaction;
use common\models\transactions\FinanceWithdrawalMoneyTransaction;
use common\models\transactions\InvestTransaction;
use common\models\transactions\ReferralFromInvestTransaction;
use common\models\transactions\TariffProfitTransaction;
use common\models\User;
use frontend\helpers\HtmlThemeHelper;
use frontend\helpers\NotifyMessages;
use frontend\models\profile\FinanceInvestForm;
use frontend\models\profile\FinanceWithdrawalForm;
use frontend\models\profile\InvestBuyForm;
use frontend\models\profile\SettingsMyDataForm;
use frontend\models\profile\SettingsPasswordForm;
use frontend\models\profile\SettingsPaySysForm;
use frontend\models\profile\SettingsPictureForm;
use frontend\models\profile\SettingsSocsForm;
use frontend\models\profile\VerifyForm;
use yii\base\Exception;
use yii\data\Pagination;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;

class ProfileController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {

        $user = User::getCurrentUser();
        $searcher = TariffToUser::find()->where(['user_id' => $user->id]);

        // get the total number of articles (but do not fetch the article data yet)
        $count = $searcher->count();

        // create a pagination object with the total count
        $pagination = new Pagination(['totalCount' => $count]);

        // limit the query using the pagination and retrieve the articles
        $data = $searcher->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        return $this->render('index', [
            'data' => $data,
            'pagination' => $pagination,
            'count' => $count
        ]);
    }


    public function actionInvest()
    {
        return $this->render('invest');
    }

    public function actionInvestBuy($type)
    {

        $model = new InvestBuyForm();
        $model->type = $type;
        if ($model->load(\Yii::$app->request->post())
            && $model->validate()
            && $model->finish()
        ) {


            NotifyMessages::addPushState('success', "Тариф куплен");

            return $this->redirect(Url::to(['profile/invest']));
        }


        if ($model->hasErrors()) {
            foreach ($model->getErrors() as $fieldErrors) {
                foreach ($fieldErrors as $fieldError) {
                    NotifyMessages::addPushState('error', $fieldError);
                }
            }
        }

        return $this->render('invest-buy', ['model' => $model]);
    }


    public function actionFinances()
    {
        $pay_form = new FinanceInvestForm();

        $pay_form->pay_system = 'perfect_money';
        if ($pay_form->load(\Yii::$app->request->post())
            && $pay_form->validate()
            && $pay_form->finish()
        ) {


//            NotifyMessages::addPushState('success', "Запрос отправлен");

            return $this->redirect(['payment/redirect-form',
                'amount' => $pay_form->amount,
                'pay_system' => $pay_form->pay_system,
            ]);
        }


        if ($pay_form->hasErrors()) {
            foreach ($pay_form->getErrors() as $fieldErrors) {
                foreach ($fieldErrors as $fieldError) {
                    NotifyMessages::addPushState('error', $fieldError);
                }
            }
        }

        $withdrawal_form = new FinanceWithdrawalForm();

        $withdrawal_form->pay_system = 'perfect_money';
        if ($withdrawal_form->load(\Yii::$app->request->post())
            && $withdrawal_form->validate()
            && $withdrawal_form->finish()
        ) {
            NotifyMessages::addPushState('success', "Запрос отправлен");

            return $this->refresh();
        }


        if ($withdrawal_form->hasErrors()) {
            foreach ($withdrawal_form->getErrors() as $fieldErrors) {
                foreach ($fieldErrors as $fieldError) {
                    NotifyMessages::addPushState('error', $fieldError);
                }
            }
        }


        $searcher = (new Query())->select('*')
            ->from(Transaction::tableName())
            ->where(['user_id'=>User::getCurrentUser()->id])
            ->andWhere(['in', 'type', array_merge(
                array_values(FinanceWithdrawalMoneyTransaction::types()),
                array_values(FinanceInvestMoneyTransaction::types())
            )])
            ->orderBy('id desc');


        // get the total number of articles (but do not fetch the article data yet)
        $count = $searcher->count();

        // create a pagination object with the total count
        $pagination = new Pagination(['totalCount' => $count]);

        // limit the query using the pagination and retrieve the articles
        $models = $searcher->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('finances', [
            'pay_form' => $pay_form,
            'withdrawal_form' => $withdrawal_form,
            'models' => $models,
            'pagination' => $pagination,
            'count' => $count
        ]);
    }


    public function actionPrograms()
    {

        $user = User::getCurrentUser();


        $searcher = ReferralFromInvestTransaction::query()
            ->andWhere(['user_id' => $user->id]);

        // get the total number of articles (but do not fetch the article data yet)
        $count = $searcher->count();

        // create a pagination object with the total count
        $pagination = new Pagination(['totalCount' => $count]);

        // limit the query using the pagination and retrieve the articles
        $models = $searcher->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('programs', [
            'models' => $models,
            'pagination' => $pagination,
            'count' => $count
        ]);
    }


    public function actionHistory($type = 'in-out')
    {

        $columns = [];

        $searcher = (new Query())->from(Transaction::tableName());


        switch ($type) {
            case 'in-out':
                $searcher->where(['in', 'type',
                    array_merge(FinanceInvestMoneyTransaction::typeId(),
                        FinanceWithdrawalMoneyTransaction::typeId())
                ]);

                break;
            case 'partners':
                $searcher->where(['type' => Transaction::TYPE_STRUCTURE_INVEST_PROFIT
                ]);

                break;
            case 'invest':
                $searcher->where(['type' => Transaction::TYPE_PROFIT_TARIFF
                ]);

                break;
        }


        $searcher->andWhere(['user_id'=>User::getCurrentUser()->id]);

        // get the total number of articles (but do not fetch the article data yet)
        $count = $searcher->count();

        // create a pagination object with the total count
        $pagination = new Pagination(['totalCount' => $count]);

        // limit the query using the pagination and retrieve the articles
        $models = $searcher->offset($pagination->offset)
            ->limit($pagination->limit)
            ->orderBy('updated_at desc')
            ->all();

        $data = [];
        switch ($type) {
            case 'in-out':
                $columns = [
                    'id' => '#',
                    'amount' => 'Начисления',
                    'vector' => 'Направление',
                    'date' => 'Дата',
                    'pay_sys' => 'Платёжная система',
                    'state' => 'Статус'
                ];

                foreach ($models as $model) {
                    /** @var $model [] */
                    $row = [];

                    $isOut = in_array($model['type'], FinanceWithdrawalMoneyTransaction::typeId());
                    $rowData = json_decode($model['data'], true);
                    $row['id'] = $model['id'];
                    $row['amount'] = "<span class='" . ($isOut ? 'text-danger' : 'text-success') . "'>" . ($isOut ? '-' : '') . number_format($model['amount'], 2) . " $</span>";
                    $row['vector'] = ($isOut ? 'Вывод' : 'Ввод');
                    $row['date'] = date('d.m.Y H:i:s', $model['updated_at']);
                    $row['pay_sys'] = $rowData['pay_system'];
                    $row['state'] = Transaction::getStatusName($model['status']);

                    $data[] = $row;
                }


                break;
            case 'partners':
                $columns = [
                    'id' => '#',
                    'amount' => 'Начисления',
                    'partner_name' => 'Имя партнёра',
                    'lvl' => 'Уровень',
                    'parent_amount' => 'Инвестиция на сумму',
                    'date' => 'Дата',
                ];

                foreach ($models as $model) {
                    /** @var $model [] */
                    $row = [];

                    $isOut = false;
                    $rowData = json_decode($model['data'], true);

                    /**
                     * @var $parentTransaction ReferralFromInvestTransaction
                     */
                    $parentTransaction = Transaction::getInstance($rowData['parent_transaction']);
                    $row['id'] = $model['id'];
                    $row['amount'] = "<span class='" . ($isOut ? 'text-danger' : 'text-success') . "'>" . ($isOut ? '-' : '') . number_format($model['amount'], 2) . " $</span>";
                    $row['partner_name'] = $parentTransaction->user->username;
                    $row['lvl'] = isset($rowData['lvl'])?$rowData['lvl']:"неизвестен";
                    $row['parent_amount'] = number_format($parentTransaction->amount, 2)." $";
                    $row['date'] = date('d.m.Y H:i:s', $model['updated_at']);

                    $data[] = $row;
                }


                break;
            case 'invest':
                $columns = [
                    'id' => '#',
                    'amount' => 'Начисления',
                    'tariff' => 'Тариф',
                    'date' => 'Дата',
                ];

                foreach ($models as $model) {
                    /** @var $model [] */
                    $row = [];

                    $isOut = false;
                    $rowData = json_decode($model['data'], true);

                    /**
                     * @var $parentTransaction TariffProfitTransaction
                     */
                    try {
                        $tariff = TariffToUser::findOne(['id' => $rowData['tarToUserId']])->tariff;
                    } catch(\Exception $ex) {
                        $tariff = Tariff::adminNewObj();
                        $tariff->name = "tariff was deleted";
                    }
                    $row['id'] = $model['id'];
                    $row['amount'] = "<span class='" . ($isOut ? 'text-danger' : 'text-success') . "'>" . ($isOut ? '-' : '') . number_format($model['amount'], 2) . " $</span>";
                    $row['tariff'] = $tariff->name;
                    $row['date'] = date('d.m.Y H:i:s', $model['updated_at']);

                    $data[] = $row;
                }
                break;
        }

        return $this->render('history', [
            'models' => $models,
            'pagination' => $pagination,
            'count' => $count,
            'activeNav' => $type,
            'columns' => $columns,
            'data' => $data
        ]);
    }

    public function actionSettings()
    {
        $picModel = new SettingsPictureForm();
        if ($picModel->load($_POST) && $picModel->validate()) {
            $picModel->save();

            NotifyMessages::addPushState('success', "Сохранено");

            return $this->refresh();
        }

        $myDataModel = new SettingsMyDataForm();
        $myDataModel->loadAttributes();
        if ($myDataModel->load($_POST) && $myDataModel->validate()) {
            $myDataModel->save();

            NotifyMessages::addPushState('success', "Сохранено");

            return $this->refresh();
        }


        $passWordModel = new SettingsPasswordForm();
        $passWordModel->loadAttributes();
        if ($passWordModel->load($_POST) && $passWordModel->validate()) {
            $passWordModel->save();

            NotifyMessages::addPushState('success', "Сохранено");

            return $this->refresh();
        }


        $paySystemModel = new SettingsPaySysForm();
        $paySystemModel->loadAttributes();
        if ($paySystemModel->load($_POST) && $paySystemModel->validate()) {
            $paySystemModel->save();

            NotifyMessages::addPushState('success', "Сохранено");

            return $this->refresh();
        }



        $socsModel = new SettingsSocsForm();
        $socsModel->loadAttributes();
        if ($socsModel->load($_POST) && $socsModel->validate()) {
            $socsModel->save();

            NotifyMessages::addPushState('success', "Сохранено");

            return $this->refresh();
        }

        return $this->render('settings', [
            'picModel' => $picModel,
            'myDataModel' => $myDataModel,
            'passWordModel' => $passWordModel,
            'paySystemModel' => $paySystemModel,
            'socsModel' => $socsModel
        ]);
    }

    public function actionProgramsLevel($level) {

        $lvl = 0;
        $lvlU = [User::getCurrentUser()];
        $nxtU = [];

        while ($lvl != $level) {

            foreach ($lvlU as $curU) {
                /**
                 * @var $curU User
                 */
                $nxtUs = User::find()->where(['refer'=>$curU->id])->all();
                foreach ($nxtUs as $nxt) {
                    /**
                     * @var $nxt User
                     */
                    if ($nxt->id != $curU->id) {
                        $nxtU [] = $nxt;
                    }
                }
            }


            $lvl++;
            $lvlU = $nxtU;
            $nxtU = [];
        }


        $profSum = [];
        foreach (ReferralFromInvestTransaction::query()
                     ->andWhere(['status'=>Transaction::STATUS_OK])
                     ->andWhere(['user_id'=>User::getCurrentUser()->id])
                     ->all() as $transaction) {

            /**
             * @var $transaction ReferralFromInvestTransaction
             */
            $data = $transaction->getDataAsArray();
            $parent = InvestTransaction::findOne(['id'=>$data['parent_transaction']]);
            if (!isset($profSum[$parent->user_id])) {
                $profSum[$parent->user_id] = 0;
            }
            $profSum[$parent->user_id] += floatval($transaction->amount);

        }


        $data = [];
        foreach ($lvlU as $user) {
            /**
             * @var $user User
             */

            $row = [];

            $row['login'] = $user->username;
            $row['email'] = $user->email;

            $socs = [];
            foreach ($user->userSocs as $soc) {
                $socs[$soc->source_id] = $soc->source;
            }

            $row['soc'] = $socs;

            $row['invest'] = number_format(InvestTransaction::query()
                ->andWhere(['status'=>Transaction::STATUS_OK])
                ->andWhere(['user_id'=>$user->id])
                ->sum('amount'), 2);

            if (!isset($profSum[$user->id])) {
                $profSum[$user->id] = 0;
            }

            $row['profit'] = number_format($profSum[$user->id], 2);


            $row['first_lvl'] = User::find()
                                ->andWhere(['refer'=>$user->id])
                                ->count('*');

            $data []= $row;
        }

        return $this->render('programs_level', [
            'data'=>$data,
            'level'=>$level
        ]);
    }



    public function actionVerify($code=null) {
        $model = new VerifyForm();
        $user = User::getCurrentUser();


        $send = false;
        if ($model->load($_POST)) {
            if ($model->validate() && $model->reset()) {
                NotifyMessages::addPushState('success', "Почта подтверждена");

                return $this->redirect(Url::to(['profile/settings']));
            }
        } else {
            if ($code) {
                $model->code = $code;
            } else {
                $user->email_expired_at = 0;
                $user->save();
                if ($user->email_expired_at < time()) {
                    NotifyMessages::addPushState('success', "Письмо отправлено на почту");
                    $send = true;
                    $model->sendLetter();
                }
            }
        }

        return $this->render('verify', ['model'=>$model, 'sendBlock'=>$send]);
    }

}
