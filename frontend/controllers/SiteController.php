<?php
namespace frontend\controllers;

use common\helpers\SystemHelper;
use common\models\Article;
use common\models\Tariff;
use common\models\TariffToUser;
use common\models\transactions\TariffProfitTransaction;
use common\models\User;
use common\models\UserLikeArticle;
use Yii;
use yii\data\Pagination;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public $layout = "info";

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout='landing';

        if ($uname = Yii::$app->request->get('from', null)) {
            SystemHelper::setRefer($uname);
            return $this->redirect(Url::to(['auth/sign-up']));
        }

        return $this->render('index');
    }


    public function actionInvest() {
        return $this->render('invest');
    }


    public function actionPrograms() {
        return $this->render('programs');
    }

    public function actionFinances() {
        return $this->render('finances');
    }

    public function actionFaq() {
        $this->layout='info';
        return $this->render('faq');
    }



    public function actionMarketing() {
        $this->layout='info';
        return $this->render('marketing');
    }

    public function actionSupport() {
        return $this->render('support');
    }

    public function actionAbout() {
        $this->layout='info';
        return $this->render('about');
    }


    public function actionLicense() {
        $this->layout='info';
        return $this->render('license');
    }


    private static function  getPopularNews($query = null, $limit = 3) {
        if (!$query) {
            $query = Article::find();
        }


        return $query
            ->orderBy('likes desc, views desc, id')
            ->limit($limit)
            ->all();
    }

    public function actionDebug() {
        $this->layout = 'debug';

        return $this->render('index');
    }


    public function actionNews($q="", $category="") {
        $this->layout='info';

        $newsSearcher = Article::find();
        if (strlen($q)) {
            $newsSearcher->where(['like', 'title', $q]);
        }

        if (strlen($category)) {
            $newsSearcher->andWhere(['category_id'=>$category]);
        }

        $newsSearcher->orderBy('updated_at desc');

        // get the total number of articles (but do not fetch the article data yet)
        $count = $newsSearcher->count();

        // create a pagination object with the total count
        $pagination = new Pagination(['totalCount' => $count]);

        // limit the query using the pagination and retrieve the articles
        $articles = $newsSearcher->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('news', [
            'query'=>$q,
            'category' => $category,
            'popular' => $this->getPopularNews($newsSearcher),
            'articles' => $articles,
            'pagination' => $pagination,
            'count'=>$count
        ]);
    }

    public function actionArticle($id) {
        $model = Article::findOne(['id'=>$id]);
        $model->views += 1;
        $model->save();
        return $this->render('article', [
            'model'=>$model,
            'popular' => $this->getPopularNews(),
            'query'=>''
        ]);
    }

    public function actionCronCheckTariff() {


        //active inday tariffs
        foreach (TariffToUser::search(null, TariffToUser::STATUS_ACTIVE. null) as $tar) {
            /**
             * @var $tariff Tariff
             */
            $tariff = $tar->tariff;

            if (time() - $tar->cron_profit_at >= $tariff->frequency*60*60) {
                $amount = $tar->cost * $tariff->percent;
                $transaction = new TariffProfitTransaction($tar->user_id, $amount, $tar->id);
                $transaction->executeTransaction();
                $tar->profit += $amount;
                $tar->cron_profit_at += $tariff->frequency*60*60;
            }
            if ($tar->expired_at <= time()) {
                $tar->status = TariffToUser::STATUS_CLOSED;
            }
            $tar->save();
        }

    }


    public function actionGame() {
        $this->layout='game';
        return $this->render('game');
    }

    public function actionBoxes() {
        $this->layout='game';
        return $this->render('game-boxes');
    }


    public function actionLottery() {
        $this->layout='game';
        return $this->render('game-lottery');
    }


    public function actionLike($id) {
        $article = Article::findOne(['id'=>$id]);
        if (Yii::$app->user->isGuest) {
            return json_encode([
                "content" => $this->renderPartial('parts/_like_block', [
                    "article" => $article
                ])
            ]);
        }
        $user = User::getCurrentUser();

        $isLike = UserLikeArticle::find()
            ->where([
                'article_id'=>$article->id,
                'user_id'=>$user->id
            ])
            ->one();

        if ($isLike) {
            $isLike->delete();

            $article->likes--;
        } else {
            $nw = new UserLikeArticle();
            $nw->user_id = $user->id;
            $nw->article_id = $article->id;
            $nw->save();

            $article->likes++;
        }

        $article->save();


        return json_encode([
            "content"=>$this->renderPartial('parts/_like_block', [
                "article"=>$article
            ])
        ]);
    }
}
