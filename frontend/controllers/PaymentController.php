<?php
namespace frontend\controllers;

use common\models\payment\AdvCash;
use common\models\payment\Payeer;
use common\models\payment\PerfectMoney;
use common\models\Transaction;
use common\models\transactions\FinanceInvestMoneyTransaction;
use frontend\helpers\NotifyMessages;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;

/**
 * Cabinet controller
 */
class PaymentController extends Controller
{
    public $layout = 'clearLayout';
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionCallback1448() {
        $class = Yii::$app->request->get('className', 'payeer');
        if ($class == 'payeer') {
            $class = Payeer::className();
        }
        if ($class == 'adv') {
            $class = AdvCash::className();
        }
        call_user_func([$class, 'parseCallback']);
        return 'success';
    }

    public function actionPaymentSuccess() {
        NotifyMessages::addPushState('success', "Транзакция создана. Ожидание подтверждения.");

        return $this->redirect(['profile/finances']);
    }

    public function actionPaymentDecline() {
        NotifyMessages::addPushState('warn', "Запрос отменён");

        $t_id = Yii::$app->request->get('t_id', 0);
        if ($t_id) {
            $op = FinanceInvestMoneyTransaction::findOne(['id'=>$t_id]);
            $op->status = Transaction::STATUS_CANCELED;
            $op->save();
        }
        return $this->redirect(['profile/finances']);
    }


    public function actionRedirectForm($amount, $pay_system ) {

        $className = Transaction::getPaymentClassName($pay_system);
        $form = call_user_func(
            [$className, 'sendQuery'],
            $amount,
            FinanceInvestMoneyTransaction::typeFromTypeName($pay_system),
            ['pay_sys'=>$pay_system]);
        return \Yii::$app->controller->render('redirect_form',[
            'className'=>$className,
            'form'=>$form]);
    }

};