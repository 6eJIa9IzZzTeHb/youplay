<?php

namespace frontend\controllers;

use Codeception\Lib\Interfaces\ActiveRecord;
use common\helpers\SearchEditModel;
use common\helpers\SearchModel;
use common\helpers\SearchRecord;
use common\models\Article;
use common\models\ArticleCategory;
use common\models\Faq;
use common\models\Option;
use common\models\OptionCategory;
use common\models\ReferralProfit;
use common\models\Tariff;
use common\models\TariffToUser;
use common\models\Transaction;
use common\models\transactions\FinanceWithdrawalMoneyTransaction;
use common\models\User;
use frontend\helpers\NotifyMessages;
use frontend\models\admin\HistoryModel;
use frontend\models\admin\WithdrawalModel;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\web\NotAcceptableHttpException;

class AdminController extends \yii\web\Controller
{

    public $layout = "admin";

    /**
     **
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            if (\Yii::$app->user->isGuest) return false;

                            $user = User::getCurrentUser();

                            return $user->isAdmin() && $user->hasAdminKey();
                        }
                    ]
                ],
                'denyCallback' => function ($rule, $action) {
                    if (!\Yii::$app->user->isGuest) {
                        return $this->redirect(['auth/admin-login']);
                    } else {
                        return $this->goHome();
                    }
                }
            ],

        ];
    }

    public function actionIndex()
    {

        return $this->render('index');
    }

    public function actionEdit($slug = 'user', $type = 'view', $id = null)
    {


        $class = self::modelSlugs()[$slug];
        if (!$class)
            throw new NotAcceptableHttpException('Действие невозможно');

        $params = [];
        switch ($type) {
            case 'view':
                $model = new $class();
                $searchModel = new SearchModel($class, $model);
                $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

                $params = [
                    'model' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'slug' => $slug
                ];
                break;
            case 'create':
            case 'update':

                $title = self::slugLabels()[$slug];
                /**
                 * @var $obj SearchRecord
                 */
                if ($type == 'create') {
                    $obj = $class::adminNewObj();
                    $type = 'update';
                    $title .= ": Добавление";
                } else {
                    $obj = $class::findOne(['id' => $id]);
                    $title .= " $id : Редактирование";
                }
                $model = new SearchEditModel($class, $obj);
                $model->loadAttributes();

                if ($model->load(\Yii::$app->request->post())) {
                    if ($model->validate()) {
                        $model->save();

                        NotifyMessages::addPushState('success', 'сохранено');
                        return $this->redirect(Url::to(['admin/edit', 'slug' => $slug, 'type' => $type, 'id' => $model->id]));
                    }
                }


                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $field => $errors) {
                        foreach ($errors as $error) {
                            NotifyMessages::addPushState('warn', $error);
                        }

                    }
                }

                $params = [
                    'model' => $model,
                    'fields' => $obj->adminEditTypes(),
                    'title' => $title

                ];

                break;

            case 'delete':

                /**
                 * @var $model ActiveRecord
                 */
                $model = $class::findOne(['id' => $id]);
                $model->delete();


                NotifyMessages::addPushState('success', "Удалено");
                return $this->redirect(Url::to(['admin/edit', 'slug' => $slug]));

                break;
        }

        return $this->render($type, array_merge(['slug' => $slug], $params));
    }

    public function actionHistory() {
        $searchModel = new HistoryModel(Transaction::className());
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('history', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionWithdrawal() {
        $searchModel = new WithdrawalModel(FinanceWithdrawalMoneyTransaction::className());
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('withdrawal', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionChangeWithdrawalTransactionStatus($id, $status) {
        $transaction = Transaction::getInstance($id);
        $transaction->executeFirstStep($status);

        NotifyMessages::addPushState('success', "Успешно");

        return $this->redirect(['admin/withdrawal']);
    }



    public function actionRecalcUser() {
        /**
         * @var $users User[]
         */
        $users = User::find()->all();
        $length = sizeof($users);

        for ($i=$length-1; $i>=0; $i--) {
            $users[$i]->structure_size = 0;
        }

        for ($i=$length-1; $i>=0; $i--) {
            $refer = $users[$i]->refer - 1;


            if ($users[$i]->refer != $users[$i]->id) {
                $users[$refer]->structure_size +=
                    1 + $users[$i]->structure_size;
            }
        }


        foreach ($users as $user) {
//            echo $user->username." ".$user->structure_size."<br/>";
            $user->save();
        }
    }

    public static function modelSlugs()
    {
        return [
            'user' => User::className(),
            'faq' => Faq::className(),
            'tariff' => Tariff::className(),
            'refer' => ReferralProfit::className(),
            'option' => Option::className(),
            'option_category' => OptionCategory::className(),
            'news' => Article::className(),
            'news_category' => ArticleCategory::className(),
        ];
    }

    public static function slugLabels()
    {
        return [
            'user' => 'Пользователи',
            'faq' => 'FAQ',
            'refer' => 'Реферальная система',
            'tariff' => 'Тарифы',
            'option' => 'Опции',
            'option_category' => 'Категории опций',
            'news' => 'Новости',
            'news_category' => 'Категории новостей',
        ];
    }
}
