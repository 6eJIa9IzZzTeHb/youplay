<?php

namespace frontend\controllers;

use Codeception\Module\MessageHelper;
use common\helpers\EmailHelper;
use common\models\Auth;
use common\models\User;
use frontend\models\auth\AdminLoginForm;
use frontend\models\auth\LoginForm;
use frontend\models\auth\ResetPasswordConfirmForm;
use frontend\models\auth\ResetPasswordForm;
use frontend\models\auth\SignUpForm;
use yii\authclient\ClientInterface;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Response;

class AuthController extends \yii\web\Controller
{
    public $layout='auth';


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['sign-up', 'log-in', 'reset-password', 'reset-password-confirm', 'auth', 'o-auth-success'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions'=> ['admin-login'],
                        'allow'=>true,
                        'matchCallback' => function ($rule, $action) {
                            if (\Yii::$app->user->isGuest) return false;

                            /**
                             * @var $user User
                             */
                            $user = \Yii::$app->user->getIdentity();

                            return $user->isAdmin();
                        }
                    ]
                ],
                'denyCallback'=>function($rule, $action) {
                    if (!\Yii::$app->user->isGuest) {
                        /**
                         * @var $user User
                         */
                        $user = \Yii::$app->user->getIdentity();

                        return $this->redirect($user->getUrl());
                    } else {
                        return $this->redirect(Url::to(['site/index']));
                    }
                }
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'oAuthSuccess'],
            ],
        ];
    }


    public function actionLogIn() {


        $model = new LoginForm();

        if ($model->load($_POST) && $model->validate()) {
            if ($model->login())
                return $this->refresh();
        }


        return $this->render('login', ['model'=>$model]);
    }

    public function actionSignUp() {

        $model = new SignUpForm();

        if ($model->load($_POST) && $model->validate()) {
            if ($model->signUp()) {
                return $this->refresh();
            }
        }


        return $this->render('signup', ['model'=>$model]);
    }


    public function actionAdminLogin($code=null) {
        $model = new AdminLoginForm();
        $user = User::getCurrentUser();


        $send = false;
        if ($model->load($_POST)) {
            if ($model->validate() && $model->reset()) {
                return $this->redirect(Url::to(['admin/index']));
            }
        } else {
            if ($code) {
                $model->code = $code;
            } else {
                $user->email_expired_at = 0;
                $user->save();
                if ($user->email_expired_at < time()) {

                    $send = true;
                    $model->sendLetter();
                }
            }
        }

        return $this->render('admin-login', ['model'=>$model, 'sendBlock'=>$send]);
    }

    public function actionResetPassword() {

        $model = new ResetPasswordForm();

        if ($model->load($_POST) && $model->validate()) {
            if ($model->reset()) {
                return $this->redirect(Url::to(['auth/reset-password-confirm']));
            }
        }


        return $this->render('reset-password', ['model'=>$model]);
    }

    public function actionResetPasswordConfirm() {
        $model = new ResetPasswordConfirmForm();

        if ($model->load($_POST)) {
            if ($model->validate() && $model->reset()) {
                return $this->redirect(Url::to(['auth/reset-password-confirm']));
            }
        } else {
            $model->code = \Yii::$app->request->get('code', null);
        }

        return $this->render('reset-password-confirm', ['model'=>$model]);
    }

    public function actionLogout() {
        \Yii::$app->user->logout(true);
        return $this->refresh();
    }


    /**
     * This function will be triggered when user is successfuly authenticated using some oAuth client.
     *
     * @param ClientInterface $client
     * @return boolean|Response
     */
    public function oAuthSuccess($client) {
        // get user data from client

        $userAttributes = $client->getUserAttributes();

        $source = $client->getName();
        $auth_params = $client->getAccessToken()->getParams();
        $email = ArrayHelper::getValue($auth_params,'email',null);
        if (!$email) {
            $email = $source == "google" ? $userAttributes['emails'][0]["value"] : $userAttributes["email"];
        }
        $user = User::findByEmail($email);

        if (!$user) {
            $username = preg_split("/[^\w]/", $email)[0];
            while (User::findByUsername($username)) {
                $username = $username.rand(0,9);
            }
            $user = User::createNewUser($username, $email, \Yii::$app->security->generateRandomString(8));
        }
        $user->login();



        $sourceId = $userAttributes["id"];
        $uid = $user->id;
        $auth = Auth::find()->where([
            "user_id"=>$uid,
            "source"=>$source
        ])->one();

        if (!$auth) {
            $auth = new Auth();
        }

        $auth->user_id = $uid;
        $auth->source = $source;
        $auth->source_id = $sourceId;
        $auth->save();
        return $this->refresh();
    }

}
