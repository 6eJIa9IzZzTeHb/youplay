<?php

namespace frontend\helpers;


use yii\base\Widget;
use yii\web\View;

class NotifyMessages extends \yii\bootstrap\Widget{

    public static $name = 'notify-falsh';

    public static function addPushState($type, $message) {
        $messages = \Yii::$app->session->getFlash(static::$name);
        if (!$messages) {
            $messages = '[]';
        }

        $messages = json_decode($messages);

        array_push($messages, ['type'=>$type, 'message'=>$message]);

        \Yii::$app->session->setFlash(static::$name, json_encode($messages));
    }

    public static function executeMessages() {
        $messages = \Yii::$app->session->getFlash(static::$name);
        \Yii::$app->session->removeFlash(static::$name);

        if (!$messages) {
            $messages = '[]';
        }

        return json_decode($messages);
    }


    public function run() {
        foreach (static::executeMessages() as $message) {
            \Yii::$app->view->registerJs("$.notify('".$message->message."', '".$message->type."');", View::POS_END);
        }
        return "";
    }

}