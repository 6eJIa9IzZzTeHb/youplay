<?php

namespace frontend\helpers;


use common\helpers\HtmlSettingsHelper;
use yii\authclient\widgets\AuthChoice;
use yii\base\Model;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

class HtmlThemeHelper {

    /**
     * Renders input
     *
     * @param $form ActiveForm
     * @param $model Model
     * @param $fieldName
     * @param $inputType
     * @param array $inputOptions
     * @param bool|array $labelOptions
     * @return string
     */
    public static function field($form, $model, $fieldName, $inputType = 'text', $inputOptions = [], $labelOptions = false) {
        return $form->field($model, $fieldName, [
            'template' => '{label}<div class="youplay-input">{input}</div>{error}{hint}',
            'inputOptions' => array_merge(
                    $inputOptions,
                    ['placeholder'=>$model->getAttributeLabel($fieldName)]
                ),
            ])
            ->input($inputType)
            ->label($labelOptions);
    }

    /**
     * Renders input
     *
     * @param $form ActiveForm
     * @param $model Model
     * @param $fieldName
     * @param $inputType
     * @param array $inputOptions
     * @param bool|array $labelOptions
     * @return string
     */
    public static function selectField($form, $model, $fieldName, $data=[], $inputOptions = [], $labelOptions = false) {
        return $form->field($model, $fieldName, [
//            'template' => '{label}<div class="youplay-input">{input}{error}{hint}</div>',
            'inputOptions' => array_merge(
                $inputOptions,
                ['placeholder'=>$model->getAttributeLabel($fieldName)]
            ),
        ])
        ->dropDownList($data, $inputOptions)
        ->label($labelOptions);
    }

    /**
     * Renders input
     *
     * @param $form ActiveForm
     * @param $model Model
     * @param $fieldName
     * @param array $inputOptions
     * @param bool|array $labelOptions
     * @return string
     */
    public static function checkboxField($form, $model, $fieldName,  $inputOptions = [], $labelOptions = true) {
        $id = $model->formName() . '_' . $fieldName;

        $label = $model->attributeLabels()[$fieldName];

        return $form->field($model, $fieldName, [
            'template' => '<div class="mb-20 hand">
                            {input}{label}'.$label.'<div class="btn btn-success active" data-value="'.$model->$fieldName.'" data-toogle="checkbox" data-target="#'.$id.'"></div>
                            {error}{hint}
                        </div>',

        ])
        ->checkbox(array_merge(
            $inputOptions,
            [
                'id'=>$id,
                'class'=>'hidden',
                'labelOptions'=>['class'=>'hidden']
            ]
        ));
    }


    /**
     * @param $data
     * @param $columns
     * @return string
     */
    public function  table($data, $columns) {
        return "";
    }

    public static function getLiForLandingMenu($slag, $href, $subName, $name) {
        $currentSlag = HtmlSettingsHelper::getViewName();
        return '
        <li class="'.($slag == $currentSlag ? 'active' : '').'">
            <a href="'.$href.'">
                '.$name.'<br/> <span class="label">'.$subName.'</span>
            </a>
        </li>
    ';
    }


    public static function getLiForProfileSubMenu($slag, $href, $name) {
        $currentSlag = HtmlSettingsHelper::getViewName();
        return '
            <li class="menu-item '.($slag == $currentSlag ? 'active' : '').'">
                <a href="'.$href.'"  role="button"
                   aria-expanded="false">
                    '.$name.'
                </a>
            </li>
        ';
    }


    public static function getLiForProfileMenu($slag, $href, $name) {
        $currentSlag = HtmlSettingsHelper::getViewName();
        return '
        <li class="'.((strpos($currentSlag, $slag) === 0) ? 'active' : '').'">
            <a href="'.$href.'">
                '.$name.'
            </a>
        </li>
    ';
    }


    public static function getAuthButtons() {

        $html = '<div class="btn-group social-list dib">';


        $authAuthChoice = AuthChoice::begin(['baseAuthUrl' => ['site/auth'], 'autoRender' => false]);

        $icons = [
            'facebook'=>'facebook',
            'vkontakte'=>'vk',
            'google'=>'google-plus',
        ];

        foreach ($authAuthChoice->getClients() as $client):
            $html .= '<a class="btn btn-default" title="Share on Facebook" href="'.Url::to(['auth/auth', 'authclient'=> $client->getName() ]).'"><i class="fa fa-'.$icons[$client->getName()].'"></i></a>';
        endforeach;

        AuthChoice::end();
        $html.='</div>';


        return $html;
    }
}