<?php
/**
 * @var $this \yii\web\View
 * @var $content string
 */
use yii\helpers\Url;
use yii\helpers\Html;
use common\helpers\HtmlSettingsHelper;


$lang = HtmlSettingsHelper::getLang();
$charset = HtmlSettingsHelper::getCharset();
$pingback = HtmlSettingsHelper::getPingBackUrl();
$favicon = HtmlSettingsHelper::getFavicon();
\frontend\assets\AppAsset::register($this);


$isGuest = Yii::$app->user->isGuest;

$newWithdrawalRequests = \common\models\transactions\FinanceWithdrawalMoneyTransaction::query()->andWhere(['status' => \common\models\Transaction::STATUS_IN_PROCESS])->count();

?>
<?php $this->beginPage() ?>

    <!DOCTYPE html>
    <!--[if lt IE 7]>
    <html class="lt-ie7" lang="<?=$lang?>"> <![endif]-->
    <!--[if IE 7]>
    <html class="lt-ie8" lang="<?=$lang?>"> <![endif]-->
    <!--[if IE 8]>
    <html class="lt-ie9" lang="<?=$lang?>"> <![endif]-->
    <!--[if gt IE 8]><!-->
    <html lang="<?= $lang ?>">


    <!-- Added by HTTrack -->
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <!-- /Added by HTTrack -->
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta charset="<?= $charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?= $pingback ?>">


        <link rel="shortcut icon" href="<?= $favicon ?>"/>

        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>

    </head>


    <body class="<?= HtmlSettingsHelper::getPageClass() ?>">

    <?php $this->beginBody() ?>

    <div class="perfectScroll">
        <!-- Preloader -->
        <div class="page-preloader preloader-wrapp">
            <img src="/img/logo.png" alt="">

            <div class="preloader"></div>
        </div>
        <!-- /Preloader -->

        <!-- Navbar -->
        <nav class="navbar-youplay navbar navbar-default navbar-fixed-top ">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="off-canvas" data-target="#navbar"
                            aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?= Yii::$app->homeUrl ?>">
                        <img src="/img/logo.png" alt="">
                    </a>
                </div>


                <div id="navbar" class="navbar-collapse collapse">

                    <?= $this->render('parts/_main_menu') ?>
                </div>
            </div>
        </nav>
        <!-- /Navbar -->


        <!-- Main Content -->
        <section class="content-wrap">

            <!-- Banner -->
            <div class="youplay-banner youplay-banner-parallax banner-top youplay-banner-parallax small">
                <div class="image" style="background-image: url('/img/game-diablo-iii-1400x656.jpg')">
                </div>


                <div class="info">
                    <div class="container">
                        <div class="square purpure admin-column">
                            <h4>Страницы</h4>

                            <div class="line red  mb-10"></div>
                            <div class="inline-block"><a href="<?= Url::to(['site/index']) ?>">Лендинг</a></div>
                            <div class="inline-block"><a href="<?= \common\models\User::getCurrentUser()->getUrl() ?>">Кабинет</a>
                            </div>
                            <div class="inline-block"><a href="<?= Url::to(['game/index']) ?>">Игры</a></div>

                        </div>

                        <div class="square purpure admin-column">
                            <h4>Управление</h4>

                            <div class="line red  mb-10"></div>
                            <div class="inline-block"><a href="<?= Url::to(['admin/history']) ?>">История транзакций</a>
                            </div>
                            <div class="inline-block"><a href="<?= Url::to(['admin/withdrawal']) ?>">Запросы на вывод
                                    <?php if ($newWithdrawalRequests): ?>
                                        <span class="badge pull-right bg-default"><?= $newWithdrawalRequests ?></span>
                                    <?php endif; ?>
                                </a>
                            </div>
                            <div class="inline-block"><a href="<?= Url::to(['admin/index']) ?>">Статистика</a></div>
                        </div>

                        <div class="square purpure admin-column two-columns">
                            <h4>Редактирование</h4>

                            <div class="line red mb-10"></div>
                            <?php foreach (\frontend\controllers\AdminController::slugLabels() as $slug => $label): ?>
                                <div class="inline-block"><a
                                        href="<?= Url::to(['admin/edit', 'slug' => $slug]) ?>"><?= $label ?></a></div>
                            <?php endforeach; ?>
                        </div>

                    </div>
                </div>

            </div>
            <!-- /Banner -->
            <section class="pt-20">
                <div class=" pb-40">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <?=
                            \frontend\helpers\NotifyMessages::widget();
                            ?>
                        </div>
                    </div>

                    <h5 class="text-center">Серверное время</h5>
                    <h3 class="text-center mb-40" data-toggle="time" data-timestamp="<?= time()?>"><span class="fa fa-calendar"></span> <span class="date-cnt"><?= date('d.m.Y')?></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-clock-o"></span> <span class="time-cnt"><?= date('H.i.s')?></span> </h3>

                    <?= $content ?>
                </div>

                <!-- Footer -->
                <footer class="mt-60">


                    <?= $this->render('parts/_copyright')?>

                </footer>
            </section>
            <!-- /Footer -->

        </section>
        <!-- /Main Content -->

        <!-- Search Block -->
        <div class="search-block">
            <a href="#!" class="search-toggle glyphicon glyphicon-remove"></a>

            <form action="http://html.nkdev.info/youplay/dark/search.html">
                <div class="youplay-input">
                    <input type="text" name="search" placeholder="Search...">
                </div>
            </form>
        </div>
        <!-- /Search Block -->


        <div>
            <a href="/game" class="left-fixed square green hover transparent-2">
                <img src="/img/joistik.png" alt="" height="20"/>
            </a>


        </div>
    </div>
    <?php $this->endBody() ?>


    </body>
    </html>

<?php $this->endPage() ?>