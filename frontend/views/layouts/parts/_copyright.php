<!-- Copyright -->
<div class="copyright" xmlns="http://www.w3.org/1999/html">
    <div class="container">
        <strong>You Play</strong> &copy; <?= date('Y') ?>. Все права <a href="<?= \yii\helpers\Url::to(['site/license'])?>"> защищены </a>
    </div>
</div>
<!-- /Copyright -->