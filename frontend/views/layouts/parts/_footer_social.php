<?php

use common\models\Option;
?>

<div class="footer-social text-center">
    <a class="btn" href="<?= Option::getOption('social_facebook')?>"><span class="fa fa-facebook"></span></a>
    <a class="btn" href="<?= Option::getOption('social_instagram')?>"><span class="fa fa-instagram"></span></a>
    <a class="btn" href="<?= Option::getOption('social_skype')?>"><span class="fa fa-skype"></span></a>
    <a class="btn" href="<?= Option::getOption('social_telegram')?>"><span class="fa fa-telegram"></span></a>
    <a class="btn" href="<?= Option::getOption('social_vk')?>"><span class="fa fa-vk"></span></a>
</div>