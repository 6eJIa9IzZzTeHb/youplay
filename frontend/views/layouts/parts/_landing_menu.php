<?php
use yii\helpers\Url;
use frontend\helpers\HtmlThemeHelper;

/**
 * @var $user \common\models\User|null
 */
$user = Yii::$app->user->getIdentity();

?>

<ul class="nav navbar-nav" role="menu">
    <?= HtmlThemeHelper::getLiForLandingMenu("site-index", Url::to(['site/index']), 'страница',  'Главная')?>
    <?= HtmlThemeHelper::getLiForLandingMenu("site-about", Url::to(['site/about']), 'узнать', 'О нас')?>
    <?= HtmlThemeHelper::getLiForLandingMenu("site-marketing", Url::to(['site/marketing']), 'проекта', 'Маркетинг')?>
    <?= HtmlThemeHelper::getLiForLandingMenu("site-news", Url::to(['site/news']), 'новое', 'Новости')?>
    <?= HtmlThemeHelper::getLiForLandingMenu("site-faq", Url::to(['site/faq']), 'ответы', 'FAQ')?>

    <?php if(Yii::$app->user->isGuest): ?>
        <?= HtmlThemeHelper::getLiForLandingMenu("auth-login", Url::to(['auth/log-in']), 'на сайте', 'Авторизация')?>
    <?php else: ?>
    <?php endif; ?>

</ul>
<?php if (!Yii::$app->user->isGuest): ?>
<ul role="menu" class="nav navbar-nav navbar-right">

    <?= $this->render('__profile_submenu')?>

<!--    <li>-->
<!--        <a class="search-toggle" href="search.html">-->
<!--            <i class="fa fa-search"></i>-->
<!--        </a>-->
<!--    </li>-->

</ul>
<?php endif ?>