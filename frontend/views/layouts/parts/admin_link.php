<?php

if ($user = \common\models\User::getCurrentUser()):
    if ($user->isAdmin()):


        ?>

        <a href="/game" class="left-fixed square green hover transparent-2">
            <img src="/img/joistik.png" alt="" height="20"/>
        </a>


        <a href="<?= \yii\helpers\Url::to(['admin/index']) ?>"
           class="no-style left-fixed square red hover transparent-2 text-center"
           style="transform: rotateZ(-90deg); right:-7px;">
            <div class="no-style text-center fa fa-adn"></div>
            <div class="small">админка</div>
        </a>

    <?php
    endif;
endif;
?>