<?php
use frontend\helpers\HtmlThemeHelper;
?>

<li class="menu-item current-menu-ancestor dropdown dropdown-hover">
    <a class="dropdown-toggle" data-toggle="dropdown" href="<?= \common\helpers\HtmlSettingsHelper::getUserProfileUrl()?>" role="button" aria-expanded="false">

        <span class="glyphicon glyphicon-user"></span><?= \common\models\User::getCurrentUser()->username?>
        <span class="fa fa-caret-down"></span>
        <span class="label">В кабинет </span>
    </a>

    <div class="dropdown-menu">
        <ul role="menu">

            <?= HtmlThemeHelper::getLiForProfileSubMenu('profile-index', \common\models\User::getCurrentUser()->getUrl(), 'Профиль')?>
            <?= HtmlThemeHelper::getLiForProfileSubMenu('site-news', \yii\helpers\Url::to(['site/news']), 'Новости')?>
            <?= HtmlThemeHelper::getLiForProfileSubMenu('site-support', \yii\helpers\Url::to(['site/support']), 'Техподдержка')?>
            <li>
                <a>
                    <?php \yii\widgets\ActiveForm::begin(['action'=>\yii\helpers\Url::to(['auth/logout'])]);?>

                    <button class="no-style no-padding no-margin">Выход</button>
                    <?php \yii\widgets\ActiveForm::end(); ?>
                </a>
            </li>

        </ul>
    </div>
</li>

<!--<li class="menu-item current-menu-ancestor">-->
<!--    <a href="#">-->
<!--        <span class="fa fa-arrows-alt" data-fullscreen data-toggle-class="noevents" data-toggle-general-events="fullscreenchange MSFullscreenChange mozfullscreenchange webkitfullscreenchange" data-toggle-from="fa-arrows-alt" data-toggle-to="fa-compress" ></span>-->
<!--    </a>-->
<!--</li>-->