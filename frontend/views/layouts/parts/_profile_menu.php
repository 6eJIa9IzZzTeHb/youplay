<?php

use frontend\helpers\HtmlThemeHelper;
use yii\helpers\Url;


/**
 * @var $user \common\models\User
 */
$user = Yii::$app->user->getIdentity();
?>
<div class="youplay-user-navigation">
    <div class="container">
        <ul>

            <?= HtmlThemeHelper::getLiForProfileMenu('profile-index', \common\models\User::getCurrentUser()->getUrl(), 'Профиль')?>
            <?= HtmlThemeHelper::getLiForProfileMenu('profile-invest', Url::to(['profile/invest']), 'Инвестировать')?>
            <?= HtmlThemeHelper::getLiForProfileMenu('profile-finances', Url::to(['profile/finances']), 'Финансы')?>
            <?= HtmlThemeHelper::getLiForProfileMenu('profile-programs', Url::to(['profile/programs']), 'Реф. программа')?>
            <?= HtmlThemeHelper::getLiForProfileMenu('profile-history', Url::to(['profile/history']), 'История')?>
            <?= HtmlThemeHelper::getLiForProfileMenu('profile-settings', Url::to(['profile/settings']), 'Настройки')?>

        </ul>
    </div>
</div>