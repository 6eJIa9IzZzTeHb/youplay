<?php
/**
 * @var $this \yii\web\View
 * @var $content string
 */
use yii\helpers\Url;
use yii\helpers\Html;
use common\helpers\HtmlSettingsHelper;


$lang = HtmlSettingsHelper::getLang();
$charset = HtmlSettingsHelper::getCharset();
$pingback = HtmlSettingsHelper::getPingBackUrl();
$favicon = HtmlSettingsHelper::getFavicon();
\frontend\assets\LandingAsset::register($this);

?>
<?php $this->beginPage() ?>

    <!DOCTYPE html>
    <!--[if lt IE 7]>
    <html class="lt-ie7" lang="<?=$lang?>"> <![endif]-->
    <!--[if IE 7]>
    <html class="lt-ie8" lang="<?=$lang?>"> <![endif]-->
    <!--[if IE 8]>
    <html class="lt-ie9" lang="<?=$lang?>"> <![endif]-->
    <!--[if gt IE 8]><!-->
    <html lang="<?= $lang ?>">


    <!-- Added by HTTrack -->
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <!-- /Added by HTTrack -->
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta charset="<?= $charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?= $pingback ?>">


        <link rel="shortcut icon" href="<?= $favicon ?>"/>

        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>

    </head>


    <body class="<?= HtmlSettingsHelper::getPageClass() ?>">

    <?php $this->beginBody() ?>

    <div class="perfectScroll">

        <!-- Preloader -->
        <div class="page-preloader preloader-wrapp">
            <img src="/img/logo.png" alt="">

            <div class="preloader"></div>
        </div>
        <!-- /Preloader -->

        <!-- Navbar -->
        <nav class="navbar-youplay navbar navbar-default navbar-fixed-top ">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="off-canvas" data-target="#navbar"
                            aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?= Yii::$app->homeUrl ?>">
                        <img src="img/logo.png" alt="">
                    </a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <?= $this->render('parts/_landing_menu') ?>
                </div>
            </div>
        </nav>
        <!-- /Navbar -->


        <!-- Main Content -->
        <section class="content-wrap">

            <?= $content ?>

            <!-- Footer -->
            <footer class="">


                <?= $this->render('parts/_footer_social') ?>

                <div class="footer-menu text-center">
                    <a class="footer-link" href="#">Главная</a>
                    <a class="footer-link" href="#">О нас</a>
                    <a class="footer-link" href="#">Маркетинг</a>
                    <a class="footer-link" href="#">Новости</a>
                    <a class="footer-link" href="#">FAQ</a>
                </div>

                <div class="line grey"></div>


                <?= $this->render('parts/_copyright')?>

            </footer>
            <!-- /Footer -->

        </section>
        <!-- /Main Content -->

        <!-- Search Block -->
        <div class="search-block">
            <a href="#!" class="search-toggle glyphicon glyphicon-remove"></a>

            <form action="http://html.nkdev.info/youplay/dark/search.html">
                <div class="youplay-input">
                    <input type="text" name="search" placeholder="Search...">
                </div>
            </form>
        </div>
        <!-- /Search Block -->


    </div>
    <?php $this->endBody() ?>


    </body>
    </html>

<?php $this->endPage() ?>