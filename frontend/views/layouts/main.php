<?php
/**
 * @var $this \yii\web\View
 * @var $content string
 */
use yii\helpers\Url;
use yii\helpers\Html;
use common\helpers\HtmlSettingsHelper;


$lang = HtmlSettingsHelper::getLang();
$charset = HtmlSettingsHelper::getCharset();
$pingback = HtmlSettingsHelper::getPingBackUrl();
$favicon = HtmlSettingsHelper::getFavicon();
\frontend\assets\AppAsset::register($this);


$user = \common\models\User::getCurrentUser();
$isGuest = Yii::$app->user->isGuest;
?>
<?php $this->beginPage() ?>

    <!DOCTYPE html>
    <!--[if lt IE 7]>
    <html class="lt-ie7" lang="<?=$lang?>"> <![endif]-->
    <!--[if IE 7]>
    <html class="lt-ie8" lang="<?=$lang?>"> <![endif]-->
    <!--[if IE 8]>
    <html class="lt-ie9" lang="<?=$lang?>"> <![endif]-->
    <!--[if gt IE 8]><!-->
    <html lang="<?= $lang ?>">


    <!-- Added by HTTrack -->
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <!-- /Added by HTTrack -->
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta charset="<?= $charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?= $pingback ?>">


        <link rel="shortcut icon" href="<?= $favicon ?>"/>

        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>

    </head>


    <body class="<?= HtmlSettingsHelper::getPageClass() ?>">

    <?php $this->beginBody() ?>


        <!-- Preloader -->
        <div class="page-preloader preloader-wrapp">
            <img src="/img/logo.png" alt="">

            <div class="preloader"></div>
        </div>
        <!-- /Preloader -->

        <!-- Navbar -->
        <nav class="navbar-youplay navbar navbar-default navbar-fixed-top ">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="off-canvas" data-target="#navbar"
                            aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?= Yii::$app->homeUrl ?>">
                        <img src="/img/logo.png" alt="">
                    </a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <?= $this->render('parts/_main_menu') ?>
                </div>
            </div>
        </nav>
        <!-- /Navbar -->


        <!-- Main Content -->
        <section class="content-wrap">

            <!-- Banner -->
            <div class="youplay-banner banner-top youplay-banner-parallax small">
                <div class="image" style="background-image: url('/img/game-diablo-iii-1400x656.jpg')">
                </div>

                <?= $this->render('parts/_profile_menu'); ?>

                <div class="info">
                    <div>
                        <div class="container youplay-user">
                            <a class="angled-img">
                                <div class="img">

                                    <img src="<?= $user->pic?>" alt="avatar">
                                </div>
                            </a>
                            <!--
                                -->
                            <div class="user-data  mb-20" style="vertical-align: top;">
                                <h2 class="text-center"><?= $user->username ?></h2>
                                <!--                            <div class="location"><i class="fa fa-map-marker"></i> Los Angeles</div>-->
                                <div class="">
                                    <div>
                                        <!--                                    <div class="num">69</div>-->
                                        <!--                                    <div class="title">Posts</div>-->
                                    </div>
                                    <div class="text-center">
                                        <h1 class="mt-15 mb-0">$ <?= number_format($user->balance, 2) ?></h1>

                                        <div class="help-block">баланс</div>
                                    </div>
                                    <div>
                                        <!--                                    <div class="num">689</div>-->
                                        <!--                                    <div class="title">Followers</div>-->
                                    </div>
                                </div>
                            </div>

                            <!--                        <a href="/game" class="user-data hover ml-60 no-style" style="vertical-align: top;">-->
                            <!--                            <h2><img src="/img/joistik.png" alt="" height="85"/></h2>-->
                            <!--                            <!--                            <div class="location"><i class="fa fa-map-marker"></i> Los Angeles</div>-->
                            <!--                            <div class="">-->
                            <!--                                <div>-->
                            <!--                                    <!--                                    <div class="num">69</div>-->
                            <!--                                    <!--                                    <div class="title">Posts</div>-->
                            <!--                                </div>-->
                            <!--                                <div class="text-center">-->
                            <!--                                    <p>Play Game</p>-->
                            <!--                                </div>-->
                            <!--                                <div>-->
                            <!--                                    <!--                                    <div class="num">689</div>-->
                            <!--                                    <!--                                    <div class="title">Followers</div>-->
                            <!--                                </div>-->
                            <!--                            </div>-->
                            <!--                        </a>-->
                        </div>


                        <!--                    <div class="container mt-20">-->
                        <!--                        <a href="#!" class="btn btn-sm btn-default ml-0">Add Friend</a>-->
                        <!--                        <a href="#!" class="btn btn-sm btn-default">Private Message</a>-->
                        <!--                    </div>-->
                    </div>
                </div>


            </div>
            <!-- /Banner -->
            <section class="bg-pictures <?= $isGuest ? "pt-70" : "pt-20" ?>">
                <div class=" pb-40">
                    <?= $content ?>
                </div>

                <!-- Footer -->
                <footer class="mt-60">


                    <div class="mb-40">
                        <?= $this->render('parts/_footer_social') ?>
                    </div>



                    <!-- Partners -->
                    <section class="youplay-banner youplay-banner-parallax small text-center">

                        <div class="image" style="background-image: url('/img/landing-block-5-bg.jpg');">
                        </div>


                        <img height="70" src="/img/logo.png" alt="pay system"/>

                    </section>



                    <?= $this->render('parts/_copyright')?>


                </footer>
            </section>
            <!-- /Footer -->

        </section>
        <!-- /Main Content -->

        <!-- Search Block -->
        <div class="search-block">
            <a href="#!" class="search-toggle glyphicon glyphicon-remove"></a>

            <form action="http://html.nkdev.info/youplay/dark/search.html">
                <div class="youplay-input">
                    <input type="text" name="search" placeholder="Search...">
                </div>
            </form>
        </div>
        <!-- /Search Block -->


        <div>


            <?= $this->render('parts/admin_link') ?>
        </div>
        <?php $this->endBody() ?>
    </body>
    </html>
<?=
\frontend\helpers\NotifyMessages::widget();
?>
<?php $this->endPage() ?>