<?php
/**
 * @var $this \yii\web\View

 *
 * @var $models Transaction[]
 * @var $data []
 * @var $pagination \yii\data\Pagination
 * @var $count int
 * @var $columns []
 */
?>
<div class="container youplay-content">

    <div class="row">


        <?php $navs = [
            'invest' => 'Начисления из инвестиций',
            'partners' => 'Начисления от партнёров',
            'in-out' => 'Ввод/вывод'
        ];

        ?>
        <div class="col-md-12 text-center">
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <?php foreach($navs as $id=>$label): ?>
                <li class="nav-item">
                    <button data-href="<?= \yii\helpers\Url::to(['profile/history', 'type'=>$id])?>" class="btn mb-10 <?= $id!=$activeNav?'':'active'?>" id="pills-<?= $id?>-tab" data-toggle="pill" data-target="#pills-<?= $id?>" role="tab" aria-controls="pills-home" aria-expanded="true"><?= $label?></button>
                </li>

                <?php endforeach; ?>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <?php foreach($navs as $id=>$label): ?>
                <div class="tab-pane fade" id="pills-<?= $id?>" role="tabpanel">
                    <ul class="pagination">
                        <?= \yii\widgets\LinkPager::widget([
                            'pagination' => $pagination,
                        ]);
                        ?>

                    </ul>
                    <table class="table table-bordered table-without-head-border table-striped">
                        <thead>
                        <tr>
                            <?php foreach ($columns as $name): ?>
                            <th><?=$name?></th>
                            <?php endforeach; ?>
                        </tr>
                        </thead>
                        <tbody>

                        <?php if (!sizeof($data)):?>
                            <td colspan="<?= sizeof($columns)?>">Нет записей</td>
                        <?php else: ?>

                            <?php foreach ($data as $row): ?>
                                <tr>
                                    <?php foreach ($columns as $id=>$name): ?>
                                        <th><?=$row[$id]?></th>
                                    <?php endforeach; ?>
                                </tr>
                            <?php endforeach ?>

                        <?php endif;?>



                        </tbody>
                    </table>
                    <ul class="pagination">
                        <?= \yii\widgets\LinkPager::widget([
                            'pagination' => $pagination,
                        ]);
                        ?>

                    </ul>
                </div>
                <?php endforeach; ?>
            </div>
        </div>



    </div>

</div>
