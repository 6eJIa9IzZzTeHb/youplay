<?php
/**
 * @var $this \yii\web\View
 * @var $model \frontend\models\profile\FinanceInvestForm
 */

$type = $model->pay_system;

?>

<?php $form = \yii\widgets\ActiveForm::begin() ?>

    <div class="full-width square purpure text-center">
        <h2>Ввод</h2>
        <?php $idValue = $form->id.'_type'; ?>
        <section  data-toggle="typePickContainer"  data-target="<?= $idValue?>"  data-value="<?= $type?>"  class="little-grey-block grey-block mt-10 text-center">
            <button data-target="<?= $idValue?>" data-value="adv" type="button" data-toggle="typePick" class="parallelepiped light-purpure hover">
                <img src="/img/landing-info-block-4-adv.png" height="16"/>
            </button>
            <button data-target="<?= $idValue?>" data-value="perfect_money" data-toggle="typePick" type="button" class="parallelepiped light-purpure hover">
                <img src="/img/landing-info-block-4-pf.png" height="17"/>
            </button>
            <button data-target="<?= $idValue?>" data-value="payeer" data-toggle="typePick" type="button" class="parallelepiped light-purpure hover">
                <img src="/img/landing-info-block-4-pay.png" height="16"/>
            </button>

            <?= $form->field($model, 'pay_system')->hiddenInput(['id'=>$idValue])->label(false) ?>
        </section>
        <?= $form->field($model, 'amount', [
            'template' => '
                    <div class="mt-10"><strong>Cумма:</strong> &nbsp;&nbsp;
            <div class="parallelepiped dark-purpure">
            {input}

            </div>
            &nbsp;&nbsp;&nbsp;
            <div class="parallelepiped light-purpure">
                <div class="fa fa-usd"></div>
            </div>
        </div> {label}{error}{hint}
            '
        ])->textInput(['class' => 'no-style'])->label(false) ?>


        <div class="mt-20 mb-20">
            <button class="btn active hover pr-50 pl-50">Пополнить</button>
        </div>
    </div>
<?php \yii\widgets\ActiveForm::end() ?>


