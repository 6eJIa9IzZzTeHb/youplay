<?php
/**
 * @var $this \yii\web\View
 * @var $pay_form \frontend\models\profile\FinanceInvestForm
 * @var $withdrawal_form \frontend\models\profile\FinanceWithdrawalForm
 *
 * @var $models Transaction[]
 * @var $pagination \yii\data\Pagination
 * @var $count int
 */

?>

<div class="container youplay-content">

    <div class="row">

        <div class="col-md-12 text-center mb-40">
            <h2 class="mt-0">Финансы</h2>

        </div>
        <div class="col-md-6 pl-50 pr-50 mb-20">
            <?= $this->render('parts/_pay_form', ['model'=>$pay_form]);?>
        </div>

        <div class="col-md-6 pl-50 pr-50 mb-20">

            <?= $this->render('parts/_withdrawal_form', ['model'=>$withdrawal_form]);?>

        </div>



        <div class="col-md-12 mt-50 mt-50">

            <h3 class="mt-70 mb-30 text-center">Последнии транзакции</h3>

            <table class="table table-bordered table-without-head-border table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Начисление</th>
                    <th>Дата/Время</th>
                    <th>Тип</th>
                    <th>Cтатус</th>
                </tr>
                </thead>
                <tbody>

                <?php foreach($models as $model):?>
                <?php $t = \common\models\Transaction::getInstance($model['id']);?>
                <tr>
                    <td><p><?= $model['id']?></p></td>
                    <td><p><?= number_format($model['amount'], 2)?> $</p></td>
                    <td><p><?= date('Y.m.d H:i:s', $model['created_at'])?></p></td>
                    <td><p><?= $t->description()?></p></td>
                    <td><p><?= \common\models\Transaction::getStatusName($t->status)?></p></td>
                </tr>
                <?php endforeach;?>

                </tbody>
            </table>
            <!-- Pagination -->
            <ul class="pagination">
                <?= \yii\widgets\LinkPager::widget([
                    'pagination' => $pagination,
                ]);
                ?>

            </ul>
            <!-- /Pagination -->
        </div>
    </div>

</div>
