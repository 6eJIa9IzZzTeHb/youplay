<?php
$this->title = "Инвестиции";
?>
<div class="container youplay-content">

    <div class="row">

        <div class="col-md-12 text-center">
            <h3 class="text-center mt-70 mb-20">Тарифы</h3>



            <div class="row">
                <?php /** @var $tars \common\models\Tariff[] */ ?>
                <?php $tars = \common\models\Tariff::find()->orderBy('start_price')->all()?>

                <?php for($i=0;$i<sizeof($tars); $i++): ?>
                    <?php
                    $can = Yii::$app->user->isGuest ? $tars[$i]->limit : max(0, $tars[$i]->limit -
                        (Yii::$app->user->isGuest ? true : \common\models\TariffToUser::find()
                            ->where(['status'=>\common\models\TariffToUser::STATUS_ACTIVE])
                            ->andWhere(['>', 'created_at', time()-24*60*60])
                            ->andWhere(['tariff_id'=>$tars[$i]->id])
                            ->count()));

                    $canBuy = (Yii::$app->user->isGuest ? true : \common\models\TariffToUser::find()
                        ->where(['status'=>\common\models\TariffToUser::STATUS_ACTIVE])
                        ->andWhere(['user_id'=>\common\models\User::getCurrentUser()->id])
                        ->andWhere(['>', 'created_at', time()-24*60*60])
                        ->andWhere(['tariff_id'=>$tars[$i]->id])
                        ->count()==0) && $can;


                    ?>

                    <div class="col-md-3 plan-cnt">

                        <div class="square purpure  p-0 square-cnt mt-30" style="width:270px;">
                            <div class="square light-purpure  m-0 p-15 square-content full-width">
                                <strong><?= $tars[$i]->name?></strong>
                            </div>

                            <div class="plan-img relative" style="background-image: url('<?= $tars[$i]->pic ?>')">
                                <div class="plan-limit-text">
                                    <div class=" pull-right youplay-hexagon-rating youplay-hexagon-rating-small inline-block"
                                        data-max="<?= $tars[$i]->limit ?>" data-size="50"><span><?= $can?></span>
                                    </div>

                                    <div class=" inline-block pull-right mt-15 mr-5">лимит</div>
                                </div>
                            </div>

                            <div class="line line-right ping"></div>
                            <div class="square purpure">


                                <h3 class="square-content mt-20">от <?= $tars[$i]->start_price?>$ до <?= number_format($tars[$i]->end_price)?>$</h3>
                                <div class="tip-block">сумма</div>


                                <div class="line shadow-line grey"></div>

                                <h3 class="square-content"><?= $tars[$i]->percent*100?>% <?= $tars[$i]->frequency != 24 ? 'каждые '.$tars[$i]->frequency.'ч.' : 'в день'?> </h3>
                                <div class="tip-block">процент</div>
                                <div class="line shadow-line grey"></div>
                                <h3 class="square-content"><?= $tars[$i]->time?> дней</h3>


                                <div class="tip-block">срок</div>
                                <div class="line shadow-line grey"></div>
                                <h3 class="square-content"><?= $tars[$i]->back_dep ? 'с возвращением депозита' : 'без возвращения депозита' ?>
                                </h3>

                                <div class="square-content mt-10 mb-10" style="white-space: nowrap;">
                                    <a style="vertical-align: top; margin-top: 3px;" href="<?= $canBuy ? \yii\helpers\Url::to(['profile/invest-buy', 'type'=> $tars[$i]->id]) : '#' ?>" class="btn <?= $canBuy ? '' : 'active'?>"><?= $canBuy ? 'Инвестировать' : 'Исчерпано'?>

                                    </a>

                                    <span class="parallelepiped light-purpure" data-toggle="tooltip" data-title="<?= htmlspecialchars($tars[$i]->desc)?>">
                                        <span class="fa fa-usd"></span>
                                    </span>
                                </div>
                            </div>
                        </div>

                    </div>
                <?endfor; ?>
            </div>

        </div>


    </div>

</div>
