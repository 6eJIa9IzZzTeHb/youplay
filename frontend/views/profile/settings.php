<?php

/**
 * @var $this \yii\web\View
 * @var $picModel \yii\base\Model
 * @var $myDataModel \yii\base\Model
 * @var $passWordModel \yii\base\Model
 * @var $paySystemModel \yii\base\Model
 * @var $socsModel \yii\base\Model
 */


$user = \common\models\User::getCurrentUser();

?>
<div class="container youplay-content">

    <div class="row">

        <div class="col-md-12">
            <div class="info">
                <div>
                    <div class="angled-change container youplay-user  text-center">

                        <div class="parallelepiped purpure transparent">

                            <a class="angled-img">
                                <div class="img">
                                    <div style="">
                                        <?= \common\helpers\SystemHelper::getImageInput($picModel, 'pic') ?>
                                    </div>
                                </div>
                                <?php $picForm = \yii\widgets\ActiveForm::begin() ?>


                                <?= $picForm->field($picModel, 'pic')->hiddenInput()->label(false); ?>
                                <button class="btn btn-full" style="margin-left:-10px;">Сохранить</button>

                                <?php \yii\widgets\ActiveForm::end() ?>

                            </a>
                            <!--
                                -->
                            <div class="user-data">
                                <h2 class="text-uppercase mt-0"><?= $user->username ?></h2>
                                <!--                            <div class="location"><i class="fa fa-map-marker"></i> Los Angeles</div>-->
                                <div class="">
                                    <div>
                                        <!--                                    <div class="num">69</div>-->
                                        <!--                                    <div class="title">Posts</div>-->
                                    </div>
                                    <div class="text-center">
                                        <h1 class="mt-15 mb-0">$ <?= number_format($user->balance, 2) ?></h1>

                                        <div class="help-block" style="margin-top:-3px;">баланс</div>
                                        <!--                                    <button class="btn btn-full" style="margin-left:-23px; margin-top:-8px;">Сохранить</button>-->
                                    </div>
                                    <div>
                                        <!--                                    <div class="num">689</div>-->
                                        <!--                                    <div class="title">Followers</div>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--                    <div class="container mt-20">-->
                    <!--                        <a href="#!" class="btn btn-sm btn-default ml-0">Add Friend</a>-->
                    <!--                        <a href="#!" class="btn btn-sm btn-default">Private Message</a>-->
                    <!--                    </div>-->
                </div>
            </div>

        </div>

        <!--        <div class="col-md-12 text-center">-->
        <!--            <div class="square purpure">-->
        <!--                <img height="200" width="200" src="img/avatar.png"/>-->
        <!--                <br/>-->
        <!--            </div>-->
        <!--        </div>-->

        <?php
        $withSpace = false;
        $row = true;
        foreach ([
                     "Мои данные" => $myDataModel ,
                     "Смена пароля"=> $passWordModel,
                     "Платёжные системы"=> $paySystemModel,
                     "Социальные сети"=> $socsModel

                 ] as $frameName => $model):
            ?>
            <?php /** @var $model \yii\base\Model */?>
            <?php $form = \yii\widgets\ActiveForm::begin() ?>

            <?= $row ? "</div><div class='row'>" : ""?>

            <div class="col-md-5 mt-50 <?= $withSpace ? "col-md-offset-2" : "" ?>">
                <div class="square full-width purpure transparent p-20">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1 text-center"><h3><?= $frameName ?></h3></div>


                        <?php foreach ($model->attributeLabels() as $key=>$label): ?>
                            <div class="col-md-12 mt-25">
                                <div class="row">
                                    <div class="col-md-4 text-center text-right-md mt-5 mb-3"><?= $label ?>:</div>
                                    <div class="col-md-7">
                                        <?= \frontend\helpers\HtmlThemeHelper::field($form, $model, $key, $model->getInputType($key), [
                                            'disabled'=>$key=='username'||$key=='email'
                                        ])?>
                                        <?php if ($key == 'email'): ?>
                                                <?php if ($user->is_verified): ?>
                                                    <p class="text-success">Подтверждено</p>
                                                <?php else: ?>
                                                    <a href="<?= \yii\helpers\Url::to(['profile/verify'])?>">Подтвердить</a>
                                                <?php endif; ?>
                                        <?php endif; ?>

                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>

                        <div class="text-center col-md-10 col-md-offset-2 mt-30 mb-10">
                            <button class="btn">Сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php \yii\widgets\ActiveForm::end() ?>

            <?php
            $withSpace = !$withSpace;
            $row = !$row;
        endforeach;

        ?>


    </div>

</div>
