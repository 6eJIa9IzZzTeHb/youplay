<?php

/**
 * @var $sendBlock bool
 */

$this->title = "Подтвердить почту";

?>

<div class="info">
    <div>
        <div class="container align-center">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="square light-purpure transparent-2 p-80 pt-20 pb-20">
                        <h2 class="mb-60">Подтверждение почты</h2>


                        <?php $form = \yii\widgets\ActiveForm::begin(['options'=>['class'=>'mb-20']]) ?>


                        <?php if ($sendBlock): ?>
                            <p>
                                <span class="text-center hint-block">Запрос был отправлен на почту</span>
                            </p>
                        <?php endif; ?>

                        <?= \frontend\helpers\HtmlThemeHelper::field($form, $model, 'code')?>


                        <button class="btn btn-default db mt-20">Подтверлить</button>
                        <?php  \yii\widgets\ActiveForm::end(); ?>

                        <div>
                            <a href="<?= \common\models\User::getCurrentUser()->getUrl()?>">вернутьься в профиль?</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>