<?php

$user = \common\models\User::getCurrentUser();
$this->title= $user->username;

/**
 * @var $pagination \yii\data\Pagination
 * @var $data \common\models\TariffToUser[]
 */
?>


<div class="container youplay-content">

    <div class="row">

        <div class="col-md-5">

            <div class="text-center">
            <div class="parallelepiped grey text-dark mb-15" style="min-width:60%"><?= $user->getReferralLink()?></div>
                <button class="btn mt-minus-5" data-toggle="clipboard" data-clipboard-action="copy" data-clipboard-text="<?= $user->getReferralLink()?>">Копировать</button>

            </div>


            <div style="padding:20px;" class="mt-20">
                <div class="parallelepiped purpure full-width text-center mt-30">
                    <img src="/img/logo-light.png" style="margin:40px 0;">

                    <div class="line ping"></div>


                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <h2><?= number_format($user->balance,2)?> $</h2>
                            <h3>баланс</h3>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <h2><?= $user->structure_size?> <span class="glyphicon glyphicon-user"></span></h2>
                            <h3>структура</h3>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <div class="col-md-7 text-center">

            <h2 class="mt-0">Моя статистика</h2>

            <section class="grey-block mt-50">
                <div class="parallelepiped grey text-purpure">
                    <div class="circle circle-top purpure"><div class="fa fa-credit-card-alt"></div></div>
                    <h1><?= number_format($user->structure_invest, 2)?>$</h1><br/> инвестировано</div>
                <div class="parallelepiped grey text-purpure">

                    <div class="circle circle-top ping"><div class="fa fa-money"></div></div>
                    <h1><?= number_format($user->total_profit, 2)?>$</h1><br/> начислено</div>
                <div class="parallelepiped grey text-purpure">

                    <div class="circle circle-top light-blue"><div class="fa fa-users"></div></div>
                    <h1><?= number_format($user->profit_invest, 2)?>$</h1><br/> доход от партнёров</div>
                <div class="parallelepiped grey text-purpure">

                    <div class="circle circle-top green"><div class="fa fa-credit-card"></div></div>
                    <h1><?= number_format($user->withdraw, 2)?>$</h1><br/> выведено</div>
            </section>

        </div>

        <div class="col-md-12 mt-50 mt-50">


            <h3 class="mt-0 mb-20">Мои инвестиции</h3>
            <table class="table table-bordered table-without-head-border table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Тариф</th>
                        <th>Сумма</th>
                        <th>Дней в работе</th>
                        <th>Начислено</th>
                        <th>Дата окончания</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($data as $tar): ?>
                <tr>
                    <td><p><?= $tar->id?></p></td>
                    <td><p><?= $tar->tariff->name?></p></td>
                    <td><p><?= number_format($tar->cost, 2)?>$</p></td>
                    <td><p><?= floor((time()-$tar->created_at)/24/60/60)?></p></td>
                    <td><p><?= number_format($tar->profit, 2)?>$</p></td>
                    <td><p><?= date('d.m.Y', $tar->expired_at)?></p></td>
                </tr>

                <?php endforeach;?>

                </tbody>
            </table>
            <?php if ($count == 0):?>
                <blockquote class="alert alert-danger text-center">По вашему запросу записей не нашлось :-(</blockquote>
            <?php endif;?>
            <!-- Pagination -->
            <ul class="pagination">
                <?= \yii\widgets\LinkPager::widget([
                    'pagination' => $pagination,
                ]);
                ?>

            </ul>
        </div>


    </div>

</div>
