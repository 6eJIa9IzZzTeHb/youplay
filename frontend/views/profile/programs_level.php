<?php

/**
 * @var $user \common\models\User
 * @var $data []
 * @var $level int
 */

$user = \common\models\User::getCurrentUser();

?>
<div class="container youplay-content">

    <div class="row">
        <div class="col-md-12 mt-50 mt-20">

            <?php foreach ([1,2,3,4,5] as $lvl): ?>
                <a class="<?= $lvl == $level ? 'square ping' : ''?> mr-20" href="<?= \yii\helpers\Url::to(['profile/programs-level', 'level'=>$lvl])?>">Уровень <?= $lvl?></a>
            <?php endforeach;?>
        </div>

        <div class="col-md-12 mt-50 mt-70">


            <h3 class="mt-0 mb-20">Мои рефералы</h3>
            <table class="table table-bordered table-without-head-border table-striped">
                <thead>
                    <tr>
                        <th>Логин</th>
                        <th>Почта</th>
                        <th>Социальная сеть</th>
                        <th>Инвестировано</th>
                        <th>Прибыль от реферала</th>
                        <th>Людей в первой линии</th>
                    </tr>
                </thead>
                <tbody>


                <?php if (!sizeof($data)): ?>

                    <tr><td colspan="6 text-сenter">пока нет ни одного реферала</td></tr>
                <?php endif; ?>

                <?php foreach ($data as $row): ?>
                <tr>
                    <td><p><?= $row['login']?></p></td>
                    <td><p><?= $row['email']?></p></td>
                    <td><p>
                            <?php if (!sizeof($row['soc'])): ?>
                                нет

                            <?php endif; ?>
                            <?php foreach ($row['soc'] as $source=>$source_id): ?>
                                <?= \yii\helpers\Html::a($source, $source_id)?>
                            <?php endforeach; ?>
                        </p></td>
                    <td><p><?= $row['invest']?> $</p></td>
                    <td><p><?= $row['profit']?> $</p></td>
                    <td><p><?= $row['first_lvl']?></p></td>
               </tr>

                <?php endforeach; ?>




                </tbody>
            </table>

        </div>


    </div>

</div>
