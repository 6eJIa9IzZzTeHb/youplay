<?php
/**
 * @var $Model \frontend\models\profile\InvestBuyForm
 * @var $tar \common\models\Tariff
 */

$this->title = "Покупка тарифа";

$tar = \common\models\Tariff::findOne($model->type);

?>
<div class="container youplay-content">

    <div class="row">

        <div class="col-md-12 text-center">
            <div class=" mb-20 parallelepiped purpure p-20" style="width:600px;">
                <h3 class="text-center">Покупка тарифа "<?= $tar->name ?>"</h3>
            </div>
            <section class="little-grey-block grey-block mt-30 mb-60 text-center">
                <div class="parallelepiped grey text-dark" style="width:160px">

                    <h4 class="text-dark">Сумма</h4><br> от <?= $tar->start_price ?>$ до <?= number_format($tar->end_price)?>$
                </div>
                <div class="parallelepiped grey text-dark" style="width:160px">

                    <h4 class="text-dark">Процент</h4><br> <?= $tar->percent * 100 ?>% каждые <?= $tar->frequency?>ч.
                </div>
                <div class="parallelepiped grey text-dark" style="width:160px">

                    <h4 class="text-dark">Срок</h4><br> <?= $tar->time?>
                </div>
                <div class="parallelepiped grey text-dark" style="width:160px">

                    <h4 class="text-dark">Возврат</h4><br> <?= $tar->back_dep ? 'Да' : 'Нет' ?>
                </div>

            </section>



            <?php $form = \yii\widgets\ActiveForm::begin(); ?>

            <div class="square purpure p-40 pl-80 pr-80">


                <h3 class="mb-30">Сколько вы хотите инвестиорвать?</h3>

                <?= \frontend\helpers\HtmlThemeHelper::field($form, $model, 'amount') ?>

                <button class="btn">Заказать</button>
            </div>

            <?php \yii\widgets\ActiveForm::end() ?>


        </div>


    </div>

</div>
