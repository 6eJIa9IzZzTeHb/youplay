<?php

/**
 * @var $user \common\models\User
 */

$user = \common\models\User::getCurrentUser();

?>
<div class="container youplay-content">

    <div class="row">

        <div class="col-md-12 text-center mb-40">
            <h2 class="mt-0">Реферальная программа</h2>

        </div>
        <div class="col-md-7">

            <div class="text-center">
                <div class="parallelepiped grey text-dark mb-15" style="min-width:60%"><?= $user->getReferralLink()?></div>
                <button class="btn mt-minus-5" data-toggle="clipboard" data-clipboard-action="copy" data-clipboard-text="<?= $user->getReferralLink()?>">Копировать</button>

            </div>


        </div>
        <div class="col-md-5 text-center">
            <div class="parallelepiped light-blue hover cursor" data-toggle="tooltip" title="скоро" style="padding:10px 60px">Рекламные материалы</div>

        </div>


        </div>
        <div class="row">
            <div class="col-md-5 text-center">
                <div class="parallelepiped purpure youplay-user mt-60">
                    <?php $sponsor = $user->referObj;?>

                    <a class="angled-img">
                        <div class="img">

                            <img src="<?= $sponsor->pic?>" alt="avatar">
                        </div>
                    </a>


                    <div class="inline-block text-center ml-40 mr-40">
                        <h3>Ваш спонсор</h3>
                        <h2><?= $sponsor->username?></h2>
                        <div style="margin-top:-20px"><?= $sponsor->email?></div>
                    </div>
                </div>
            </div>
        <div class="col-md-7">
            <div class="mt-60">

                <h3 class="mt-0 text-center">Ваши реферальные уровни </h3>

                <section class="little-grey-block grey-block mt-10 text-center">

                    <?php if (!$user->isVip()):?>

                        <?php foreach(\common\models\ReferralProfit::find()->where(['type'=>0])->orderBy('lvl')->all() as $ref): ?>
                            <?php /** @var $ref \common\models\ReferralProfit */?>

                            <div class="parallelepiped purpure fs-10 m-2">

                                <h3 class="text-white m-0"><?= $ref->percent*100?>%</h3><br> с <?= $ref->lvl?>го уровня</div>
                        <?php endforeach ?>

                    <?php else: ?>
                        <?php foreach(\common\models\ReferralProfit::find()->where(['type'=>1])->orderBy('lvl')->all() as $ref): ?>
                            <?php /** @var $ref \common\models\ReferralProfit */?>

                            <div class="parallelepiped dark-grey fs-10 m-2">


                                <h3 class="text-white m-0"><?= $ref->percent*100?>%</h3><br> с <?= $ref->lvl?>го уровня</div>
                        <?php endforeach ?>
                    <?php endif; ?>

                </section>
            </div>
        </div>

        <div class="col-md-12 mt-50 mt-70">


            <h3 class="mt-0 mb-20">Доход от рефералов <a style="font-size: 16px;" href="<?= \yii\helpers\Url::to(['profile/programs-level', 'level'=>1])?>">перейти к структуре</a></h3>
            <table class="table table-bordered table-without-head-border table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Логин</th>
                        <th>Дата регистрации</th>
                        <th>Инвестировано</th>
                        <th>Получено</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($models as $model): ?>
                <?php /** @var $model \common\models\transactions\ReferralFromInvestTransaction */?>
                <tr>
                    <td><p><?= $model->id?></p></td>
                    <td><p><?= $model->getParentTransaction()->user->username ?></p></td>
                    <td><p><?= date('d.m.Y', $model->created_at)?></p></td>
                    <td><p><?= number_format($model->getParentTransaction()->amount, 2)?>$</p></td>
                    <td><p><?= number_format($model->amount, 2)?>$</p></td>
                </tr>

                <?php endforeach; ?>




                </tbody>
            </table>

        </div>


    </div>

</div>
