<?php

/**
 * @var $form []
 * @var $className string
 * @var $this \yii\web\View
 */

?>

<div class="jumbotron">
    <h3 class="text-center">Перенаправление...</h3>
</div>

<form class="hidden" style="display: none;" id="send_form" action="<?= $form['action']?>" method="<?= $form['method']?>">
    <?php foreach($form['fields'] as $name=>$value):?>
        <label class="block">
            <?= $name?>
            <input type="text" name="<?=$name?>" value="<?= $value?>"/>
        </label>
    <?php endforeach;?>
</form>

<script type="text/javascript">
    document.getElementById('send_form').submit();
</script>