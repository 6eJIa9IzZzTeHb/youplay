<?php

use yii\helpers\Url;

/**
 * @var $user \common\models\User|null|false
 */
$user = Yii::$app->user->getIdentity();

?>


<?php if (Yii::$app->user->isGuest): ?>

    <a class="parallelepiped text-center purpure dark-shadow hover p-20 ml-10" style="width:125px" href="<?= Url::to(['auth/log-in'])?>">Вход</a>

    <a class="parallelepiped text-center ping dark-shadow hover p-20 ml-30" style="" href="<?= Url::to(['auth/sign-up'])?>">Регистрация</a>


<?php else: ?>

    <a class="parallelepiped text-center purpure dark-shadow hover p-20 ml-10" style="width:125px" href="<?= $user->getUrl()?>">Профиль</a>

    <a class="parallelepiped text-center ping dark-shadow hover p-20 ml-30" style="" href="<?= Url::to(['profile/invest'])?>">Инвестировать</a>

<?php endif; ?>