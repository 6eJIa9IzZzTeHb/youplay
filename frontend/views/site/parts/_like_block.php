<?php

/**
 * @var $article \common\models\Article
 */

$user = \common\models\User::getCurrentUser();

$id = uniqid('like');

$isLike = \common\models\UserLikeArticle::find()
    ->where(['article_id'=>$article->id,
    'user_id'=>$user->id])
    ->count();

$count = $article->likes;
?>


<div id="<?= $id?>">

    <button class="no-style" data-pjax="<?= \yii\helpers\Url::to(['site/like', 'id'=>$article->id])?>" data-pjax-container="#<?= $id?>"> <span class="no-decoration fa <?= $isLike?'fa-heart':'fa-heart-o'?>"></span> <?= $count?></button>

</div>
