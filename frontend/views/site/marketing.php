<?php
$this->title = "Маркетинг";
?>
<div class="container youplay-content">

    <div class="about">
        <div class="purpure transparent square p-15">
            <div class="row">

                <div class="col-md-12  mb-40">

                    <?= \common\models\Option::getOption('info_marketing_text') ?>

                </div>

                <div class="col-md-5 text-center">

                    <h3 class="parallelepiped ping p-15 mt-30">Реферальная программа</h3>
                    <section class="little-grey-block grey-block mt-40 text-center nowrap">

                        <?php foreach (\common\models\ReferralProfit::find()->where(['type' => 0])->orderBy('lvl')->all() as $ref): ?>
                            <?php /** @var $ref \common\models\ReferralProfit */ ?>

                            <div class="parallelepiped purpure fs-10 m-2">

                                <h3 class="text-white m-0"><?= $ref->percent * 100 ?>%</h3><br> с <?= $ref->lvl ?>го
                                уровня
                            </div>
                        <?php endforeach ?>
                    </section>
                </div>
                <div class="col-md-7 text-center">
                    <div class=" square closed dark-grey transparent-2" data-toggle="tooltip" data-placement="bottom"
                         title="Скоро">
                        <h3 class="parallelepiped light-blue p-15">VIP реферальная программа</h3>
                        <section class="little-grey-block grey-block mt-40 text-center">
                            <?php foreach (\common\models\ReferralProfit::find()->where(['type' => 1])->orderBy('lvl')->all() as $ref): ?>
                                <?php /** @var $ref \common\models\ReferralProfit */ ?>

                                <div class="parallelepiped dark-grey fs-10 m-2">


                                    <h3 class="text-white m-0"><?= $ref->percent * 100 ?>%</h3><br> с <?= $ref->lvl ?>го
                                    уровня
                                </div>
                            <?php endforeach ?>


                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">


        <div class="col-md-12 text-center">
            <h3 class="text-center mt-70 mb-20">Тарифы</h3>


            <div class="owl-carousel" data-nav="true">
                <?php /** @var $tars \common\models\Tariff[] */ ?>

                <?php $tars = \common\models\Tariff::find()->orderBy('start_price')->all() ?>

                <?php for ($i = 0; $i < sizeof($tars); $i++): ?>
                    <?php
                    $can = Yii::$app->user->isGuest ? $tars[$i]->limit : max(0, $tars[$i]->limit -
                        (Yii::$app->user->isGuest ? true : \common\models\TariffToUser::find()
                            ->where(['status' => \common\models\TariffToUser::STATUS_ACTIVE])
                            ->andWhere(['>', 'created_at', time() - 24 * 60 * 60])
                            ->andWhere(['tariff_id' => $tars[$i]->id])
                            ->count()));

                    $canBuy = (Yii::$app->user->isGuest ? true : \common\models\TariffToUser::find()
                                ->where(['status' => \common\models\TariffToUser::STATUS_ACTIVE])
                                ->andWhere(['user_id' => \common\models\User::getCurrentUser()->id])
                                ->andWhere(['>', 'created_at', time() - 24 * 60 * 60])
                                ->andWhere(['tariff_id' => $tars[$i]->id])
                                ->count() == 0) && $can;


                    ?>

                    <div class="col-md-3 plan-cnt">

                        <div class="square purpure  p-0 square-cnt mt-30" style="width:270px;">
                            <div class="square light-purpure  m-0 p-15 square-content full-width">
                                <strong><?= $tars[$i]->name ?></strong>
                            </div>

                            <div class="plan-img relative" style="background-image: url('<?= $tars[$i]->pic ?>')">
                                <div class="plan-limit-text">
                                    <div
                                        class=" pull-right youplay-hexagon-rating youplay-hexagon-rating-small inline-block"
                                        data-max="<?= $tars[$i]->limit ?>" data-size="50"><span><?= $can ?></span>
                                    </div>

                                    <div class=" inline-block pull-right mt-15 mr-5">лимит</div>
                                </div>
                            </div>

                            <div class="line line-right ping"></div>
                            <div class="square purpure">


                                <h3 class="square-content mt-20">от <?= $tars[$i]->start_price ?>$
                                    до <?= number_format($tars[$i]->end_price) ?>$</h3>

                                <div class="tip-block">сумма</div>


                                <div class="line shadow-line grey"></div>

                                <h3 class="square-content"><?= $tars[$i]->percent * 100 ?>
                                    % <?= $tars[$i]->frequency != 24 ? 'каждые ' . $tars[$i]->frequency . 'ч.' : 'в день' ?> </h3>

                                <div class="tip-block">процент</div>
                                <div class="line shadow-line grey"></div>
                                <h3 class="square-content"><?= $tars[$i]->time ?> дней</h3>


                                <div class="tip-block">срок</div>
                                <div class="line shadow-line grey"></div>
                                <h3 class="square-content"><?= $tars[$i]->back_dep ? 'с возвращением депозита' : 'без возвращения депозита' ?>
                                </h3>

                                <div class="square-content mt-10 mb-10" style="white-space: nowrap;">
                                    <a style="vertical-align: top; margin-top: 3px;"
                                       href="<?= $canBuy ? \yii\helpers\Url::to(['profile/invest-buy', 'type' => $tars[$i]->id]) : '#' ?>"
                                       class="btn <?= $canBuy ? '' : 'active' ?>"><?= $canBuy ? 'Инвестировать' : 'Исчерпано' ?>

                                    </a>

                                    <span class="parallelepiped light-purpure" data-toggle="tooltip"
                                          data-title="<?= htmlspecialchars($tars[$i]->desc) ?>">
                                        <span class="fa fa-usd"></span>
                                    </span>
                                </div>
                            </div>
                        </div>

                    </div>
                <? endfor; ?>
            </div>

        </div>


    </div>

</div>
