


<?php $navs = [
    'all' => 'Все игры',
    'my' => 'Мои игры'
];
$activeNav = 'all';
?>

<h3 class="text-center mt-20">YP Лотерея</h3>


<div  class="container mt-30">
    <section class="square purpure transparent full-width">
        <div class="row">
            <div class="col-md-12 mt-20 mb-20 text-center">
                <?php for($i=0; $i<3; $i++): ?>
                <div class="mt-20">

                    <?php for($j=0; $j<3; $j++): ?>

                    <div class="parallelepiped cursor hover purpure p-15 ml-10 mr-10">
                        <img src="/img/logo-light.png" width="150" alt=""/>
                    </div>

                    <?php endfor; ?>

                </div>
                <?php endfor; ?>
            </div>
            <div class="col-md-12 text-center"></div>
            <div class="col-md-6 text-center">
                <div class="mt-20">
                    <div class="row">
                        <div class="col-md-8 text-right pt-10 text-center-mob p-10-mob">Кейс за:</div>

                        <div class="col-md-4 text-left text-center-mob p-10-mob">
                            <div class="btn-group" data-toggle="btn-select">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    1$ <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <?php foreach([1,2,3,5,10,15] as $var): ?>
                                        <li data-val="<?= $var?>$"><a class="cursor text-dark text-center"><?= $var?>$</a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-20">

                    <div class="row">
                        <div class="col-md-8 text-right pt-10 text-center-mob p-10-mob">
                            <input name="demo" class="hidden" type="checkbox"/>Режим демо версии:
                        </div>

                        <div class="col-md-4 text-left text-center-mob p-10-mob">
                            <div class="btn btn-success active m-0" data-toogle="checkbox" data-target="[name=demo]"></div>
                        </div>
                    </div>
                </div>
                <div class="mt-20">

                    <div class="row">
                        <div class="col-md-8 text-right pt-10 text-center-mob p-10-mob">
                            <input name="rules"  class="hidden" type="checkbox"/>Я ознакомлен с <a href="#rules">правилами</a>:
                        </div>

                        <div class="col-md-4 text-left text-center-mob p-10-mob">
                            <div class="btn btn-danger active" data-toogle="checkbox" data-target="[name=rules]"></div>
                        </div>
                    </div>
                </div>
                <div class="mt-20">

                    <div class="row">
                        <div class="col-md-8 text-right pt-10 f-20 text-center-mob p-10-mob">
                            Гарантированный приз:

                        </div>

                        <div class="col-md-4 text-left text-center-mob p-10-mob">
                            <div class="parallelepiped ml-20 ping fs-10 m-2 p-5 pl-10 pr-10">

                                <h4 class="text-white m-0">0.1$</h4></div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 text-center">

                <div class="mt-30 text-center">

                    <div class="row">
                        <div class="col-md-10 text-right text-center-mob p-10-mob">
                            <button class="parallelepiped light-blue hover" style="padding:10px 60px">Рассчитать приз</button>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
                <div class="mt-30 mb-30">
                    <section class="little-grey-block grey-block mt-40 text-center">
                        <?php foreach([1.5, 2, 2.5] as $var): ?>
                            <div class="parallelepiped ping fs-10 m-2 p-0 pt-15 pb-15">

                                <h3 class="text-white m-0"><?= $var?>$</h3></div>

                        <?php endforeach; ?>
                    </section>
                    <div class="mt-10">Возможные призы</div>
                </div>
            </div>


        </div>
    </section>


    <section class="text-center">

        <h3 class="mt-50">Последние игры</h3>

        <ul class="nav nav-pills mb-3 mt-20 text-center" id="pills-tab" role="tablist">
            <?php foreach($navs as $id=>$label): ?>
                <li class="nav-item no-pull inline-block-important">
                    <button class="btn mb-10 <?= $id!=$activeNav?'':'active'?>" id="pills-<?= $id?>-tab" data-toggle="pill" data-target="#pills-<?= $id?>" role="tab" aria-controls="pills-home" aria-expanded="true"><?= $label?></button>
                </li>

            <?php endforeach; ?>
        </ul>
        <div class="tab-content square purpure transparent mt-10" id="pills-tabContent">
            <?php foreach($navs as $id=>$label): ?>
                <div class="tab-pane fade" id="pills-<?= $id?>" role="tabpanel">
                    <?php for($i=0; $i<6; $i++): ?>

                        <div class="parallelepiped purpure mt-20 ml-10 mr-10 transparent p-0 text-left">
                            <a class="angled-img inline-block-important" style="vertical-align: top">
                                <div class="img">
                                    <img src="img/user.png" alt="" style="width:75px"/>
                                </div>
                            </a>

                            <div class="inline-block"  style="vertical-align: top">
                                <h4 class="mt-5 mb-0 ml-25 mr-25">John Doe</h4>
                                <div class="line red mb-10"></div>
                                <table class="text-center">
                                    <tr>
                                        <td class="pl-15">Yp Кейсы</td>
                                        <td class="text-yellow pl-15 pr-15">5$</td>
                                        <td class="pr-15 text-green">+5$</td>
                                    </tr>
                                    <tr>
                                        <td class="pl-15">игра</td>
                                        <td class="pl-15 pr-15">депозит</td>
                                        <td class="pr-15">профит</td>

                                    </tr>
                                </table>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>
            <?php endforeach; ?>
        </div>

    </section>

    <section id="rules" class="square purpure pt-30 pb-50 mt-90 transparent full-width pl-20 pr-20">
        <div class="row">

            <div class="col-md-6 text-justify pl-20 pr-20">
                <h3>Правила игры:</h3>
                <p>AAAAAAAAAA AAAAAAA AAAAAAAA AAAAAA AAAAAAA AAAAAAAA AAAAAA AAAAAAA AAAAAAAA AAAAAA AAAAAAA AAAAAAAA AAAAAA AAAAAAA AAAAAAAA </p>
            </div>
            <div class="col-md-6 text-center">
                <?php for($i=0; $i<3; $i++): ?>
                    <div class="mt-20">

                        <?php for($j=0; $j<3; $j++): ?>

                            <div class="parallelepiped cursor purpure p-15 ml-5 mr-5">
                                <img src="/img/logo-light.png" width="75" alt=""/>
                            </div>

                        <?php endfor; ?>

                    </div>
                <?php endfor; ?>
            </div>


        </div>
    </section>

</div>
