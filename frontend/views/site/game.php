
<?php $navs = [
    'all' => 'Все игры',
    'my' => 'Мои игры'
];
$activeNav = 'all';
?>
<div class="container youplay-content">

    <div class="row">

        <div class="col-md-5 text-center">

            <h4 class="mt-0">Игровая статистика</h4>

            <ul class="nav nav-pills mb-3 mt-20 text-center" id="pills-tab" role="tablist">
                <?php foreach($navs as $id=>$label): ?>
                    <li class="nav-item no-pull inline-block-important">
                        <button class="btn mb-10 <?= $id!=$activeNav?'':'active'?>" id="pills-<?= $id?>-tab" data-toggle="pill" data-target="#pills-<?= $id?>" role="tab" aria-controls="pills-home" aria-expanded="true"><?= $label?></button>
                    </li>

                <?php endforeach; ?>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <?php foreach($navs as $id=>$label): ?>
                    <div class="tab-pane fade" id="pills-<?= $id?>" role="tabpanel">
                        <?php for($i=0; $i<5; $i++): ?>
                            <div class="mb-20">
                                <div class="parallelepiped purpure transparent p-0 text-left">
                                    <a class="angled-img inline-block-important" style="vertical-align: top">
                                        <div class="img">
                                            <img src="img/user.png" alt="" style="width:75px"/>
                                        </div>
                                    </a>

                                    <div class="inline-block"  style="vertical-align: top">
                                        <h4 class="mt-5 mb-0 ml-25 mr-25">John Doe</h4>
                                        <div class="line red mb-10"></div>
                                        <table class="text-center">
                                            <tr>
                                                <td class="pl-15">Yp Кейсы</td>
                                                <td class="text-yellow pl-15 pr-15">5$</td>
                                                <td class="pr-15 text-green">+5$</td>
                                            </tr>
                                            <tr>
                                                <td class="pl-15">игра</td>
                                                <td class="pl-15 pr-15">депозит</td>
                                                <td class="pr-15">профит</td>

                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        <?php endfor; ?>
                    </div>
                <?php endforeach; ?>
            </div>

        </div>
        <div class="col-md-7 text-center">

            <h4 class="mt-0">Моя общая статистика</h4>

            <section class="grey-block mt-45">
                <div class="parallelepiped grey text-purpure">
                    <div class="circle circle-top purpure"><div class="fa fa-star"></div></div>
                    <h1>100</h1><br/> игр сыграно</div>
                <div class="parallelepiped grey text-purpure">

                    <div class="circle circle-top ping"><div class="fa fa-money"></div></div>
                    <h1>235$</h1><br/> начислено</div>

            </section>
            
            <section class="parallelepiped purpure p-40 mt-20" style="width: 60%">
                <img src="img/logo.png" style="width:100%" alt=""/>
            </section>

        </div>

        <div class="col-md-12 mt-10 mt-10 text-center">

            <h4>Топ победителей</h4>

            <div class="square purpure transparent mt-20">
                <?php for($i=0; $i<5; $i++): ?>
                    <div class="inline-block">
                        <div class="p-0 m-10 text-left square purpure p-20">
                            <a class="angled-img text-center inline-block-important" style="vertical-align: top">
                                <div class="img">
                                    <img src="img/user.png" alt="" style="width:100px"/>
                                </div>
                                <h3>John Doe</h3>
                                <div class="line red"></div>
                                <h4 class="text-green">3720$</h4>
                                <div>профит</div>
                                <div class="line red"></div>
                                <h4 class="text-yellow">372</h4>
                                <div>сыграно игр</div>
                            </a>
                        </div>
                    </div>
                <?php endfor; ?>
            </div>


        </div>


    </div>

</div>
