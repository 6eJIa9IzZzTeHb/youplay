<?php

use kartik\form\ActiveForm;

$this->title="Новости";

/**
 * @var $query string
 * @var $category int
 * @var $pagination \yii\data\Pagination
 * @var $articles \common\models\Article[]
 */


?>
<div class="container youplay-news">

    <div class="col-md-9">

        <?php if ($count == 0):?>
            <blockquote class="alert alert-danger text-center">По вашему запросу записей не нашлось :-(</blockquote>
        <?php endif;?>
    <?php foreach ($articles as $article): ?>
        <div class="news-one">
            <div class="row vertical-gutter">
                <div class="col-md-4">
                    <a href="<?= $article->getUrl()?>" class="angled-img">
                        <div class="img">
                            <div style="background-image:url('<?= $article->pic?>'); width:240px; height:240px; display: block; background-size: cover; background-position: center;"></div>
                        </div>
<!--                        <div class="youplay-hexagon-rating youplay-hexagon-rating-small" data-max="10" data-size="50" title="9.1 out of 10" style="width: 50px; height: 50px;"><canvas width="50" height="50"></canvas><span>9.1</span>-->
<!--                        </div>-->
                    </a>
                </div>
                <div class="col-md-8">
                    <div class="clearfix">
                        <h3 class="h2 pull-left m-0"><a href="<?= $article->getUrl()?>"><?= $article->title?></a></h3>
                        <span class="date pull-right"><i class="fa fa-calendar"></i> <?= date('d.m.Y', $article->updated_at)?></span>
                    </div>
<!--                    <div class="tags">-->
<!--                        <i class="fa fa-tags"></i>  <a href="#">Bloodborne</a>, <a href="#">first try</a>, <a href="#">first boss problem</a>, <a href="#">newbie game</a>-->
<!--                    </div>-->
                    <div class="description  article-tables">
                        <?= $article->short?>
                    </div>
                    <a href="<?= $article->getUrl()?>" class="btn read-more pull-left">Подробнее</a>
                    <div class="pull-right mt-10">
                        <?= $this->render("parts/_like_block", ["article"=>$article]); ?>
                    </div>
                </div>
            </div>

            <div class="line grey mt-20"></div>
        </div>
        <!-- /Single News Block -->


    <?php endforeach; ?>

        <!-- Pagination -->
        <ul class="pagination">
            <?= \yii\widgets\LinkPager::widget([
                'pagination' => $pagination,
            ]);
            ?>

        </ul>
        <!-- /Pagination -->
    </div>
    <!-- /News List -->

    <!-- Right Side -->
    <div class="col-md-3">

        <!-- Side Search -->
        <div class="side-block">
            <p>Поиск:</p>
            <?php $form = ActiveForm::begin(['method'=>'GET'])?>
                <div class="youplay-input">
                    <input type="text" name="q" placeholder="Введите текст" value="<?= $query?>">
                </div>
            <?php ActiveForm::end()?>
        </div>
        <!-- /Side Search -->

        <!-- Side Categories -->
        <div class="side-block">
            <h4 class="block-title">Категории</h4>
            <ul class="block-content">
                <?php foreach (\common\models\ArticleCategory::find()->all() as $category): ?>
                <li><a href="<?= \yii\helpers\Url::to(['site/news', 'q'=>$query, 'category'=>$category->id ])?>"><?= $category->name?></a>
                </li>
                <?php endforeach ?>
            </ul>
        </div>
        <!-- /Side Categories -->

        <!-- Side Popular News -->
        <div class="side-block">
            <h4 class="block-title">Популярные новости</h4>
            <div class="block-content p-0">

                <?php foreach($popular as $article): ?>
                <?php /** @var $article \common\models\Article */?>
                <!-- Single News Block -->
                <div class="row youplay-side-news">
                    <div class="col-xs-3 col-md-4">
                        <a href="<?= $article->getUrl()?>" class="angled-img">
                            <div class="img">
                                <img src="<?= $article->pic?>"" alt="">
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-9 col-md-8">
                        <h4 class="ellipsis"><a href="<?= $article->getUrl()?>" title="<?= $article->title?>"><?= $article->title?>I</a></h4>
                        <span class="date"><i class="fa fa-calendar"></i> <?= date('d.m.Y', $article->updated_at)?></span>
                    </div>
                </div>
                <!-- /Single News Block -->

                <?php endforeach; ?>
            </div>
        </div>
        <!-- /Side Popular News -->


    </div>
    <!-- /Right Side -->

</div>