<?php
$this->title = "FAQ";
?>


<div class="container youplay-content">

    <?php
    $id = 0;
    foreach (\common\models\Faq::find()->all() as $faq):

        /**
         * @var $faq \common\models\Faq
         */

        $id++;
    ?>

    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" data-target="#collapse<?= $id?>" aria-expanded="false" aria-controls="collapseOne" class="collapsed cursor">
                        <?= $id?>. <?= $faq->name?> <span class="icon-plus"></span>
                    </a>
                </h4>
            </div>
            <div id="collapse<?= $id?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                <div class="panel-body square purpure transparent full-width">
                    <?=  $faq->value?>
                </div>
            </div>
        </div>
    </div>

    <?php
        endforeach;
    ?>
</div>