<?php

use common\models\Option;

/* @var $this yii\web\View */

$this->title = "YouPlay";
?>

<!-- Slider Revolution -->
<!--
  Use classes:
  rs-fullscreen
-->
<div class="tp-banner-container rs-youplay rs-fullscreen">
    <div class="tp-banner">
        <ul>
            <!-- SLIDE NR. 1  -->
            <li data-thumb="<?= Option::getOption('main_slider_1_img') ?>" data-saveperformance="on"
                data-transition="random-static" data-slotamount="7" data-masterspeed="700">
                <!-- MAIN IMAGE -->
                <img src="<?= Option::getOption('main_slider_1_img') ?>" data-bgposition="center top" data-bgfit="cover"
                     data-bgrepeat="no-repeat">
                <!-- LAYERS -->

                <!-- LAYER NR. 1 -->
                <div class="tp-caption customin customout p-50 parallelepiped transparent-2 purpure" data-x="left"
                     data-hoffset="60" data-y="center" data-voffset="-60" data-customin="x:200;scaleX:0.5;scaleY:0.5;"
                     data-customout="x:0;scaleX:1;scaleY:1;" data-start="500" data-speed="700"
                     data-easing="Sine.easeInOut"
                     data-endspeed="600" data-endeasing="Linear.easeNone">

                    <?= Option::getOption('main_slider_1_text') ?>

                </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption customin customout mt-20" data-x="left" data-hoffset="60" data-y="center"
                     data-voffset="60" data-customin="x:200;scaleX:0.5;scaleY:0.5;"
                     data-customout="x:0;scaleX:1;scaleY:1;" data-start="1000" data-speed="700"
                     data-easing="Sine.easeInOut"
                     data-endspeed="600" data-endeasing="Linear.easeNone">
                    <div class="mt-90 visible-xs"></div>

                    <?= $this->render('parts/slider_auth') ?>

                </div>
            </li>

            <!-- SLIDE NR. 2  -->
            <li data-thumb="<?= Option::getOption('main_slider_2_img') ?>" data-saveperformance="on"
                data-transition="random-static" data-slotamount="7" data-masterspeed="700">
                <!-- MAIN IMAGE -->
                <img src="<?= Option::getOption('main_slider_2_img') ?>" alt="" data-bgposition="center top"
                     data-bgfit="cover" data-bgrepeat="no-repeat">
                <!-- LAYERS -->

                <!-- LAYER NR. 1 -->
                <div class="tp-caption customin customout p-50 parallelepiped transparent-2 ping" data-x="left"
                     data-hoffset="60" data-y="center" data-voffset="-45" data-customin="x:200;scaleX:0.5;scaleY:0.5;"
                     data-customout="x:0;scaleX:1;scaleY:1;" data-start="500" data-speed="700"
                     data-easing="Sine.easeInOut"
                     data-endspeed="600" data-endeasing="Linear.easeNone">

                    <?= Option::getOption('main_slider_2_text') ?>
                </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption customin customout mt-20" data-x="left" data-hoffset="60" data-y="center"
                     data-voffset="45" data-customin="x:200;scaleX:0.5;scaleY:0.5;"
                     data-customout="x:0;scaleX:1;scaleY:1;" data-start="1000" data-speed="700"
                     data-easing="Sine.easeInOut"
                     data-endspeed="600" data-endeasing="Linear.easeNone">

                    <div class="mt-90 visible-xs"></div>

                    <?= $this->render('parts/slider_auth') ?>
                </div>
            </li>

            <!-- SLIDE NR. 3  -->
            <li data-thumb="<?= Option::getOption('main_slider_3_img') ?>" data-saveperformance="on"
                data-transition="random-static" data-slotamount="7" data-masterspeed="700">
                <!-- MAIN IMAGE -->
                <img src="<?= Option::getOption('main_slider_3_img') ?>" alt="" data-bgposition="center top"
                     data-bgfit="cover" data-bgrepeat="repeat">
                <!-- LAYERS -->

                <!-- LAYER NR. 1 -->
                <div class="tp-caption customin customout p-50 parallelepiped transparent-2 dark-purpure" data-x="left"
                     data-hoffset="60" data-y="center" data-voffset="-60" data-customin="x:200;scaleX:0.5;scaleY:0.5;"
                     data-customout="x:0;scaleX:1;scaleY:1;" data-start="500" data-speed="700"
                     data-easing="Sine.easeInOut"
                     data-endspeed="600" data-endeasing="Linear.easeNone">

                    <?= Option::getOption('main_slider_3_text') ?>
                </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption customin customout mt-20" data-x="left" data-hoffset="60" data-y="center"
                     data-voffset="60" data-customin="x:200;scaleX:0.5;scaleY:0.5;"
                     data-customout="x:0;scaleX:1;scaleY:1;" data-start="1000" data-speed="700"
                     data-easing="Sine.easeInOut"
                     data-endspeed="600" data-endeasing="Linear.easeNone">

                    <div class="mt-90 visible-xs"></div>

                    <?= $this->render('parts/slider_auth') ?>

                </div>
            </li>
        </ul>
        <div class="tp-bannertimer"></div>
    </div>
</div>
<!-- /Slider Revolution-->


<section class="block info-block-landing-1">
    <div class="parallelepiped grey text-purpure mt-15">
        <div class="circle circle-top purpure">
            <div class="fa fa-user"></div>
        </div>
        <h1><?= sizeof(\common\models\User::find()->all()) ?></h1><br/> участников
    </div>
    <div class="parallelepiped grey text-purpure mt-15">

        <div class="circle circle-top light-blue">
            <div class="fa fa-clock-o"></div>
        </div>
        <h1><?= floor((time() - Option::getOption('main_settings_day_start')) / 24 / 60 / 60) ?></h1><br/> дней работы
    </div>
    <div class="parallelepiped grey text-purpure mt-15">

        <div class="circle circle-top orange">
            <div class="fa fa-usd"></div>
        </div>
        <h1><?= floor(\common\models\Transaction::getTotalInvested()) ?>$</h1><br/> инвестированно
    </div>
    <div class="parallelepiped grey text-purpure mt-15">

        <div class="circle circle-top green">
            <div class="fa fa-money"></div>
        </div>
        <h1><?= floor(\common\models\Transaction::getTotalWithdrawal()) ?>$</h1><br/> выплачено
    </div>
</section>


<section class="block info-block-landing-2 relative">
    <div class="row">
        <div class="col-md-2 text-right">
            <h1>О нас</h1>
        </div>
        <div class="col-md-9 text-center">
            <div class="parallelepiped dark-purpure transparent-strong about-cnt dark-shadow">
                <div class="about-text">

                    <?= Option::getOption('main_settings_about') ?>
                </div>
                <div class="text-center">
                    <a class="parallelepiped ping no-hover hover p-15" href="/about">Подробнее о нас</a>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- Preorder -->
<section class="youplay-banner youplay-banner-parallax   info-block-landing-3">
    <div class="image" style="background-image: url('img/banner-witcher-3.jpg');">
    </div>

    <div class="info container align-center">
        <div>
            <h2>FAQ</h2>

            <div class="owl-carousel">

                <?php
                $colors = ['red', 'orange', 'green', 'light-blue', 'light-white-purpure', 'ping'];

                $vars = [];
                $matches = [];
                preg_match_all('/<li>(.*?)<br>(.*?)<\/li>/', Option::getOption('main_settings_faq'), $matches);

                for ($id = 0; $id < sizeof($matches[1]); $id++) {
                    $vars[($id+1).". ".$matches[1][$id]] = $matches[2][$id];
                }


                foreach ($vars as $title => $answer):
                    $color = $colors[array_rand($colors)];?>
                    <div class="item">
                        <div>
                            <div class="parallelepiped purpure transparent-strong">


                                <div class="circle circle-top <?= $color ?>">
                                    <div class="fa fa-question"></div>
                                </div>
                                <br/>
                                <strong class="question"><?= $title ?></strong>

                                <div class="line <?= $color ?>"></div>
                                <br/>
                                <strong><?= $answer ?></strong>
                                <br/>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>
<!-- /Preorder -->


<section class="block info-block-landing-4 relative container-fluid">
    <div class="row">
        <div class="col-md-3 text-right">
            <h1>Маркетинг</h1>
        </div>
        <div class="col-md-8 text-center">
            <div class="parallelepiped dark-purpure transparent-strong about-cnt dark-shadow">
                <div class="about-text">
                    <?= Option::getOption('main_settings_marketing')?>
                </div>
                <div class="text-center">
                    <a class="parallelepiped ping no-hover hover p-15" href="/marketing">Подробнее о маркетинге</a>
                </div>
            </div>
        </div>
    </div>
</section>

<!--
<section class="block info-block-landing-4 relative container-fluid">
    <div class="row">
        <div class="col-md-3 col-md-offset-1 text-center">
            <h1>Маркетинг</h1>
            <div class="square purpure transparent square-cnt mt-30">
                <div class="square purpure square-content">
                    <strong>подробнее о маркетинге</strong>
                </div>
                <div class="square-content"> FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF
                    FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFFFFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFFFFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFFFFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFFFFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFFFFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFFFFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFFFFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF FFF
                </div>
            </div>
        </div>
        <div class="col-md-7 col-md-offset-1 text-center">
            <div class="row">
                <div class="col-md-4">

                    <div class="square purpure transparent square-cnt mt-30">
                        <div class="square purpure square-content">
                            <strong>Dragon</strong>
                        </div>
                        <div class="square-content">от 10$ до 15$

                        </div>
                        <div class="line grey transparent"></div>
                        <div class="square-content">1.5% в день

                        </div>
                        <div class="line grey transparent"></div>
                        <div class="square-content">40 дней
                        </div>
                        <div class="line grey transparent"></div>
                        <div class="square-content">с возвращением депозита
                        </div>
                        <div class="square-content">
                            <button class="btn">Узнать больше</button>
                        </div>

                    </div>

                    <div class="square purpure transparent square-cnt mt-30">
                        <div class="square purpure square-content">
                            <strong>Dragon</strong>
                        </div>
                        <div class="square-content">от 10$ до 15$

                        </div>
                        <div class="line grey transparent"></div>
                        <div class="square-content">1.5% в день

                        </div>
                        <div class="line grey transparent"></div>
                        <div class="square-content">40 дней
                        </div>
                        <div class="line grey transparent"></div>
                        <div class="square-content">с возвращением депозита
                        </div>
                        <div class="square-content">
                            <button class="btn">Узнать больше</button>
                        </div>

                    </div>
                </div>
                <div class="col-md-4">

                    <div class="square purpure transparent square-cnt mt-30">
                        <div class="square purpure square-content">
                            <strong>Dragon</strong>
                        </div>
                        <div class="square-content">от 10$ до 15$

                        </div>
                        <div class="line grey transparent"></div>
                        <div class="square-content">1.5% в день

                        </div>
                        <div class="line grey transparent"></div>
                        <div class="square-content">40 дней
                        </div>
                        <div class="line grey transparent"></div>
                        <div class="square-content">с возвращением депозита
                        </div>
                        <div class="square-content">
                            <button class="btn">Узнать больше</button>
                        </div>

                    </div>

                    <div class="square purpure transparent square-cnt mt-30">
                        <div class="square purpure square-content">
                            <strong>Dragon</strong>
                        </div>
                        <div class="square-content">от 10$ до 15$

                        </div>
                        <div class="line grey transparent"></div>
                        <div class="square-content">1.5% в день

                        </div>
                        <div class="line grey transparent"></div>
                        <div class="square-content">40 дней
                        </div>
                        <div class="line grey transparent"></div>
                        <div class="square-content">с возвращением депозита
                        </div>
                        <div class="square-content">
                            <button class="btn">Узнать больше</button>
                        </div>

                    </div>
                </div><div class="col-md-4">

                    <div class="square purpure transparent square-cnt mt-30">
                        <div class="square purpure square-content">
                            <strong>Dragon</strong>
                        </div>
                        <div class="square-content">от 10$ до 15$

                        </div>
                        <div class="line grey transparent"></div>
                        <div class="square-content">1.5% в день

                        </div>
                        <div class="line grey transparent"></div>
                        <div class="square-content">40 дней
                        </div>
                        <div class="line grey transparent"></div>
                        <div class="square-content">с возвращением депозита
                        </div>
                        <div class="square-content">
                            <button class="btn">Узнать больше</button>
                        </div>

                    </div>

                    <div class="square purpure transparent square-cnt mt-30">
                        <div class="square purpure square-content">
                            <strong>Dragon</strong>
                        </div>
                        <div class="square-content">от 10$ до 15$

                        </div>
                        <div class="line grey transparent"></div>
                        <div class="square-content">1.5% в день

                        </div>
                        <div class="line grey transparent"></div>
                        <div class="square-content">40 дней
                        </div>
                        <div class="line grey transparent"></div>
                        <div class="square-content">с возвращением депозита
                        </div>
                        <div class="square-content">
                            <button class="btn">Узнать больше</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>-->


<section class="youplay-banner youplay-banner-parallax small info-block-landing-5 text-center">

    <div class="image" style="background-image: url('img/landing-block-5-bg.jpg');">
    </div>


    <div class="parallelepiped purpure transparent-strong">
        <img width="114" height="23" src="img/landing-info-block-4-pf.png" alt="pay system"/>
    </div>

    <div class="parallelepiped purpure transparent-strong">
        <img width="92" height="23" src="img/landing-info-block-4-pay.png" alt="pay system"/>
    </div>

    <div class="parallelepiped purpure transparent-strong">
        <img width="91" height="23" src="img/landing-info-block-4-adv.png" alt="pay system"/>
    </div>

    <!--    <div class="parallelepiped purpure transparent-strong">-->
    <!--        <img width="114" height="23" src="img/landing-info-block-4-bit.png" alt="pay system"/>-->
    <!--    </div>-->

</section>