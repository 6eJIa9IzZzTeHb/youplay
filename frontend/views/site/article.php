<?php

use kartik\form\ActiveForm;

$this->title="Новости";

/**
 * @var $query string
 * @var $category int
 * @var $model \common\models\Article
 */


?>
<div class="container youplay-news  article-tables">

    <div class="col-md-9">


        <div class="square purpure transparent full-width">
            <div class="text-center">
                <div class="">
                    <strong style="font-size:4rem;"><?= $model->title?></strong>

                </div>
            </div>
            <div class="mt-40 p-20 ">
                <?= $model->data?>
                <div class="line red mt-40 mb-20"></div>
                <span class="label">    <span class="fa fa-calendar"></span> <?= date('d.m.Y', $model->updated_at)?></span>
                <span class="label pull-right">    <span class="fa fa-eye"></span> <?= $model->views?></span>
                <div class="pull-right mr-20 label">
                    <?= $this->render("parts/_like_block", ["article"=>$model]); ?>
                </div>
            </div>
        </div>
    </div>
    <!-- /News List -->

    <!-- Right Side -->
    <div class="col-md-3">

        <!-- Side Search -->
        <div class="side-block">
            <p>Поиск:</p>
            <?php $form = ActiveForm::begin(['method'=>'GET', 'action'=>\yii\helpers\Url::to(['site/news'])])?>
                <div class="youplay-input">
                    <input type="text" name="q" placeholder="Введите текст" value="<?= $query?>">
                </div>
            <?php ActiveForm::end()?>
        </div>
        <!-- /Side Search -->



        <!-- Side Popular News -->
        <div class="side-block">
            <h4 class="block-title">Популярные новости</h4>
            <div class="block-content p-0">

                <?php foreach($popular as $article): ?>
                <?php /** @var $article \common\models\Article */?>
                <!-- Single News Block -->
                <div class="row youplay-side-news">
                    <div class="col-xs-3 col-md-4">
                        <a href="<?= $article->getUrl()?>" class="angled-img">
                            <div class="img">
                                <img src="<?= $article->pic?>"" alt="">
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-9 col-md-8">
                        <h4 class="ellipsis"><a href="<?= $article->getUrl()?>" title="<?= $article->title?>"><?= $article->title?>I</a></h4>
                        <span class="date"><i class="fa fa-calendar"></i> <?= date('d.m.Y', $article->updated_at)?></span>
                    </div>
                </div>
                <!-- /Single News Block -->

                <?php endforeach; ?>
            </div>
        </div>
        <!-- /Side Popular News -->


    </div>
    <!-- /Right Side -->

</div>