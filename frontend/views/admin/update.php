<?php

/**
 * @var $this \yii\web\View
 * @var $model \common\helpers\SearchEditModel
 * @var 4slug string
 * @var $fields array
 */


?>



<div class="container-fluid">

    <div class="row">
        <div class="col-md-12 admin-grid">
            <div class="text-center mb-30">
                <h4><?= $title ?></h4>
                <a href="<?= \yii\helpers\Url::to(['admin/edit', 'slug' => $slug, 'type' => 'view']) ?>"
                   class="fa fa-step-backward"> вернуться назад</a> |
                <a href="<?= \yii\helpers\Url::to(['admin/edit', 'slug' => $slug, 'type' => 'create']) ?>"
                   class="fa fa-creative-commons"> создать ещё </a> |
                <a class="fa fa-eye" href="<?= $model->model->getUrl()?>"> Просмотреть</a>
            </div>

            <!--
            Image Part
            -->
            <div class="row">
                <?php $id = 1; ?>
                <?php foreach ($fields as $field => $type): ?>
                <?php if ($type == \common\helpers\SearchEditHelper::TYPE_PIC_BOX):?>
                <div class="col-md-4 col-md-offset-1">
                    <?= \common\helpers\SearchEditHelper::drawType(null, $model, $field, $type) ?>
                </div>
                <?php if (($id++) % 2 == 0): ?>
            </div>
            <div class="row">
                <?php endif; ?>
                <?php endif; ?>
                <?php endforeach ?>
            </div>

            <?php $form = \yii\widgets\ActiveForm::begin(); ?>
            <div class="row">
                <?php $id = 1; ?>
                <?php foreach ($fields as $field => $type): ?>
                <?php if ($type == \common\helpers\SearchEditHelper::TYPE_PIC_BOX) continue;?>
                <?php if ($type == \common\helpers\SearchEditHelper::TYPE_WSYG_EDITOR) continue;?>
                <div class="col-md-4 col-md-offset-1">
                    <?= \common\helpers\SearchEditHelper::drawType($form, $model, $field, $type) ?>
                </div>
                <?php if (($id++) % 2 == 0): ?>
            </div>
            <div class="row">
                <?php endif; ?>
                <?php endforeach ?>
            </div>

            <div class="row">
                <?php $id = 1; ?>
                <?php foreach ($fields as $field => $type): ?>
                <?php if ($type == \common\helpers\SearchEditHelper::TYPE_WSYG_EDITOR):?>
                <div class="col-md-10 col-md-offset-1">
                    <?= \common\helpers\SearchEditHelper::drawType($form, $model, $field, $type) ?>
                </div>
                <?php if (($id++) % 2 == 0): ?>
            </div>
            <div class="row">
                <?php endif; ?>
                <?php endif; ?>
                <?php endforeach ?>
            </div>


            <div class="row">
                <div class="line red mt-40 mb-20"></div>
                <div class="col-md-10 col-md-offset-1 text-center mt-20">
                    <button class="btn">Сохранить</button>
                </div>
            </div>



            <?php \yii\widgets\ActiveForm::end() ?>

        </div>
    </div>
</div>