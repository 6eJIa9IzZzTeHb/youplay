<?php

/**
 * @var $this \yii\web\View
 * @var $model \common\helpers\SearchModel
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $slug string
 */

use kartik\grid\GridView;

$this->params['modelClass'] = $model::$modelClass;
?>



<div class="container-fluid">

    <div class="row">

        <div class="col-md-12 admin-grid">
            <?= GridView::widget([
                'filterModel' => $model,

                'dataProvider' => $dataProvider,

                'columns' => $model->gridColumns(),
                'pjax' => true,

                'emptyText' => 'Ничего не нашлось',
                'summary' => 'Паказаны записи {begin}-{end} из {count}.',
            ]) ?>
        </div>
    </div>
</div>