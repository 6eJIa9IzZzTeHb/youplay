<?php

/**
 * @var $this \yii\web\View
 * @var $model \common\helpers\SearchModel
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $slug string
 */

use kartik\grid\GridView;

$this->params['modelClass'] = $model::$modelClass;
$this->params['slug'] = $slug;
?>



<div class="container-fluid">

    <div class="row">
        <div class="col-md-12 text-center mb-40 mt-20">
            <a class="btn btn-danger" href="<?= \yii\helpers\Url::to(['admin/edit', 'slug'=>$slug, 'type'=>'create'])?>">Создать</a>

            <div class="mt-30">или провести операцию над существующими:</div>
        </div>
        <div class="col-md-12 admin-grid">
        <?= GridView::widget([
            'filterModel' => $model,

            'dataProvider' => $dataProvider,

            'columns' => array_merge($model->gridColumns(),
                [
                    [
                        'class' => \kartik\grid\ActionColumn::className(),
                        'urlCreator' => function ($action, $model, $key, $index) {
                            $class = $this->params['modelClass'];
                            $slug = $this->params['slug'];

                            if ($action == 'view') {
                                return $class::findOne(['id'=>$model->id])->getUrl();
                            }
                            return \yii\helpers\Url::to(['admin/edit', 'slug'=>$slug, 'type'=>$action, 'id'=>$model->id]);
                        }
                    ]
                ]),

            'pjax' => true,

            'emptyText' => 'Ничего не нашлось',
            'summary' => 'Паказаны записи {begin}-{end} из {count}.',
        ]) ?>
        </div>
    </div>
</div>