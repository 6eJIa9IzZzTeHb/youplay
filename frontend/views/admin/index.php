<?php

/**
 * @var $this \yii\web\View
 * @var $model \common\helpers\SearchModel
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $slug string
 */

use kartik\grid\GridView;


?>



<div class="container">

    <div class="row">
        <div class="col-md-12 text-center">

            <h2 class="mt-0">Статистика</h2>

            <section class="grey-block mt-50">
                <?php foreach(\common\models\Transaction::getPaySystems() as $pay_system=>$params): ?>
                    <?php $type = \common\models\transactions\FinanceInvestMoneyTransaction::typeFromTypeName($pay_system)?>
                    <div class="parallelepiped grey text-purpure">
                    <div class="circle circle-top <?= $params['color']?>"><div class="fa fa-credit-card-alt"></div></div>

                    <h1><?= number_format(
                            \common\models\transactions\FinanceInvestMoneyTransaction::query()
                                            ->andWhere([
                                                'type'=>$type,
                                                'status'=>\common\models\Transaction::STATUS_OK
                                            ])
                                            ->sum('amount')
                            ,2)?> $</h1><br> введено через <?= $pay_system?></div>
                <?php endforeach; ?>
            </section>

            <section class="grey-block mt-50">
                <?php foreach(\common\models\Transaction::getPaySystems() as $pay_system=>$params): ?>
                    <?php $type = \common\models\transactions\FinanceWithdrawalMoneyTransaction::typeFromTypeName($pay_system)?>
                    <div class="parallelepiped grey text-purpure">
                        <div class="circle circle-top  <?= $params['color']?>"><div class="fa fa-credit-card-alt"></div></div>

                        <h1><?= number_format(
                                \common\models\transactions\FinanceWithdrawalMoneyTransaction::query()
                                    ->andWhere([
                                        'type'=>$type,
                                        'status'=>\common\models\Transaction::STATUS_OK
                                    ])
                                    ->sum('amount')
                                ,2)?> $</h1><br> выведено через <?= $pay_system?></div>
                <?php endforeach; ?>
            </section>

            <section class="grey-block mt-50">
                    <div class="parallelepiped grey text-purpure">
                        <div class="circle circle-top orange"><div class="fa fa-credit-card-alt"></div></div>

                        <h1><?= $svb = number_format(
                                \common\models\User::find()
                                    ->sum('balance')
                                ,2)?> $</h1><br> сумма по баланасам</div>


            </section>


            <section class="grey-block mt-50">
                <?php foreach(\common\models\Transaction::getPaySystems() as $pay_system=>$params): ?>
                    <?php $type = \common\models\transactions\FinanceWithdrawalMoneyTransaction::typeFromTypeName($pay_system)?>
                    <div class="parallelepiped grey text-purpure">
                        <div class="circle circle-top  <?= $params['color']?>"><div class="fa fa-credit-card-alt"></div></div>

                        <h1><?= number_format(
                                \common\models\transactions\FinanceWithdrawalMoneyTransaction::query()
                                    ->andWhere([
                                        'type'=>$type,
                                        'status'=>\common\models\Transaction::STATUS_IN_PROCESS
                                    ])
                                    ->sum('amount')
                                ,2)?> $</h1><br> сумма заявок на вывод <br/> через <?= $pay_system?></div>
                <?php endforeach; ?>
            </section>



            <section class="grey-block mt-50">
                <div class="parallelepiped grey text-purpure">
                    <div class="circle circle-top orange"><div class="fa fa-credit-card-alt"></div></div>

                    <h1><?= number_format(
                            \common\models\transactions\FinanceWithdrawalMoneyTransaction::query()
                                ->andWhere([
                                    'status'=>\common\models\Transaction::STATUS_OK,
                                ])
                                ->andWhere([
                                    '>=', 'updated_at', time()-24*60*60
                                ])
                                ->sum('amount')
                            ,2)?> $</h1><br> сумма вывода за 24 часа</div>
                <div class="parallelepiped grey text-purpure">
                    <div class="circle circle-top orange"><div class="fa fa-credit-card-alt"></div></div>

                    <h1><?= $szd = number_format(
                            \common\models\transactions\FinanceInvestMoneyTransaction::query()
                                ->andWhere([
                                    'status'=>\common\models\Transaction::STATUS_OK,
                                ])
                                ->sum('amount')
                            ,2)?> $</h1><br> сумма всех заведенных денег</div>

                <div class="parallelepiped grey text-purpure">
                    <div class="circle circle-top orange"><div class="fa fa-credit-card-alt"></div></div>

                    <h1><?= number_format(
                            \common\models\transactions\FinanceWithdrawalMoneyTransaction::query()
                                ->andWhere([
                                    'status'=>\common\models\Transaction::STATUS_OK,
                                ])
                                ->sum('amount')
                            ,2)?> $</h1><br> сумма всех выведенных денег</div>

            </section>

            <section class="grey-block mt-50">
                <div class="parallelepiped grey text-purpure">
                    <div class="circle circle-top orange"><div class="fa fa-credit-card-alt"></div></div>


                    <?php
                        $left = 0;
                        //user balances
                        $left += \common\models\User::find()
                            ->sum('balance');

                        //invest
                        $left += \common\models\TariffToUser::find()
                            ->where(['status'=>\common\models\TariffToUser::STATUS_ACTIVE])
                            ->sum('cost');

                        //+invest
                        $left -= \common\models\transactions\FinanceInvestMoneyTransaction::find()
                                ->where(['status'=>\common\models\Transaction::STATUS_OK])
                                ->sum('amount');

                        //-withdrawal
                        $left += \common\models\transactions\FinanceInvestMoneyTransaction::find()
                            ->where(['status'=>\common\models\Transaction::STATUS_OK])
                            ->sum('amount');
                    ?>
                    <h1><?=  $left?> $</h1><br> фантики</div>


            </section>
            <section class="grey-block mt-50">
                <div class="parallelepiped grey text-purpure">
                    <div class="circle circle-top purpure"><div class="fa fa-credit-card-alt"></div></div>

                    <h1><?= number_format(\common\models\TariffToUser::find()->sum('cost'), 2) ?> $</h1><br> сумма денег в обороте</div>


            </section>
        </div>
    </div>
</div>