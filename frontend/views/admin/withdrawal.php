<?php

/**
 * @var $this \yii\web\View
 * @var $model \common\helpers\SearchModel
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $slug string
 */

use kartik\grid\GridView;

$this->params['modelClass'] = $model::$modelClass;
?>



<div class="container-fluid">

    <div class="row">

        <div class="col-md-12 admin-grid">
            <?= GridView::widget([
                'filterModel' => $model,

                'dataProvider' => $dataProvider,

                'columns' => array_merge($model->gridColumns(), [
                    [
                        'class' => \kartik\grid\DataColumn::className(),
                        'format' => 'html',
                        'value' => function ($model, $key, $index, $column) {
                            if ($model->status != \common\models\Transaction::STATUS_IN_PROCESS)
                                return "";
                            return \yii\helpers\Html::a('отказать', ['admin/change-withdrawal-transaction-status', 'status'=>\common\models\Transaction::STATUS_CANCELED, 'id'=>$model->id], ['class'=>'badge pull-right bg-default']).
                            \yii\helpers\Html::a('ок', ['admin/change-withdrawal-transaction-status', 'status'=>\common\models\Transaction::STATUS_OK, 'id'=>$model->id], ['class'=>'badge pull-right bg-success']);
                        }
                    ]
                ]),
                'pjax' => true,

                'emptyText' => 'Ничего не нашлось',
                'summary' => 'Паказаны записи {begin}-{end} из {count}.',
            ]) ?>
        </div>
    </div>
</div>