<?php

$this->title = "Регистрация";

?>

<div class="info">
    <div>
        <div class="container align-center">
            <div class="youplay-form">
                <h1>Регистрация</h1>


                <?= \frontend\helpers\HtmlThemeHelper::getAuthButtons();?>

                <?php $form = \yii\widgets\ActiveForm::begin(['options'=>['class'=>'mb-20']]) ?>


                    <?php if ($refer = \common\helpers\SystemHelper::getRefer()): ?>
                        <div class="youplay-input">
                            <input type="text" disabled value="Реферал: <?= $refer->username?>"/>
                        </div>
                    <?php endif;?>

                    <?= \frontend\helpers\HtmlThemeHelper::field($form, $model, 'username')?>

                    <?= \frontend\helpers\HtmlThemeHelper::field($form, $model, 'email')?>

                    <?= \frontend\helpers\HtmlThemeHelper::field($form, $model, 'password', 'password')?>

                    <?= \frontend\helpers\HtmlThemeHelper::field($form, $model, 'retry_password', 'password')?>

                    <?= \frontend\helpers\HtmlThemeHelper::checkboxField($form, $model, 'accept')?>

                    <button class="btn btn-default db">Подтверлить</button>

                <?php  \yii\widgets\ActiveForm::end(); ?>

                <div>
                    <a href="<?= \yii\helpers\Url::to(['auth/log-in'])?>">вернуться ко входу?</a>
                </div>
            </div>
        </div>
    </div>
</div>

