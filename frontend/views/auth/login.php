<?php

/**
 * @var $this \yii\web\View
 * @var $model \frontend\models\auth\LoginForm
 */

$this->title = "Вход";

?>

<div class="info">
    <div>
        <div class="container align-center">
            <div class="youplay-form">
                <h1>Вход</h1>

                <?= \frontend\helpers\HtmlThemeHelper::getAuthButtons();?>



                <?php $form = \yii\widgets\ActiveForm::begin(['options'=>['class'=>'mb-20']]) ?>



                    <?= \frontend\helpers\HtmlThemeHelper::field($form, $model, 'username')?>

                    <?= \frontend\helpers\HtmlThemeHelper::field($form, $model, 'password', 'password')?>


                    <button class="btn btn-default">Войти</button>

                <?php  \yii\widgets\ActiveForm::end(); ?>
                <div>
                    <a href="<?= \yii\helpers\Url::to(['auth/reset-password'])?>">забыли пароль?</a> / <a href="<?= \yii\helpers\Url::to(['auth/sign-up'])?>">регистрация</a>
                </div>
            </div>
        </div>
    </div>
</div>
