<?php

$this->title = "Сброс пароля";

?>

<div class="info">
    <div>
        <div class="container align-center">
            <div class="youplay-form">
                <h2 class="mb-40">Сброс пароля</h2>


                <?php $form = \yii\widgets\ActiveForm::begin(['options'=>['class'=>'mb-20']]) ?>


                <?= \frontend\helpers\HtmlThemeHelper::field($form, $model, 'email')?>



                <button class="btn btn-default db">Подтверлить</button>
                <?php  \yii\widgets\ActiveForm::end(); ?>

                <div>
                    <a href="<?= \yii\helpers\Url::to(['auth/log-in'])?>">вернуться ко входу?</a>
                </div>
            </div>
        </div>
    </div>
</div>