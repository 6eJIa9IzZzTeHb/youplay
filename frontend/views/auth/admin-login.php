<?php

/**
 * @var $sendBlock bool
 */

$this->title = "Сброс пароля";

?>

<div class="info">
    <div>
        <div class="container align-center">
            <div class="youplay-form">
                <h2 class="mb-40">Админка</h2>


                <?php $form = \yii\widgets\ActiveForm::begin(['options'=>['class'=>'mb-20']]) ?>


                <?php if ($sendBlock): ?>
                    <p>
                     <span class="text-center hint-block">Запрос был отправлен на почту</span>
                    </p>
                <?php endif; ?>

                <?= \frontend\helpers\HtmlThemeHelper::field($form, $model, 'code')?>


                <button class="btn btn-default db">Подтверлить</button>
                <?php  \yii\widgets\ActiveForm::end(); ?>

                <div>
                    <a href="<?= \common\models\User::getCurrentUser()->getUrl()?>">вернуться в профиль?</a>
                </div>
            </div>
        </div>
    </div>
</div>