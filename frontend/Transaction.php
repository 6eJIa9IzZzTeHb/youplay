<?php
//namespace common\models;

use common\models\payment\Payeer;
use common\models\payment\PerfectMoney;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * User model
 *
 * @property $id integer
 * @property $type integer
 * @property $owner integer
 * @property $amount float
 * @property $data string
 * @property $status integer
 * @property $err_no integer
 * @property $created_at integer
 * @property $updated_at integer
 */
class Transaction extends ActiveRecord
{

    const STATUS_UNKNOWN = 0;
    const STATUS_CREATED = 1;
    const STATUS_IN_PROCESS = 2;
    const STATUS_CANCELED = 3;
    const STATUS_ERROR = 4;
    const STATUS_ACCEPTED = 5;

    const NO_ERROR = 0;
    const ERR_BAD_HASH = 1;
    const ERR_BAD_IP = 2;
    const ERR_BAD_STATUS = 4;
    const ERR_BAD_AMOUNT = 8;
    const ERR_MISSED_PARAMS = 16;
    const ERR_PAYMENT_SIDE = 32;
    const ERR_CANT_OPEN = 64;
    const ERR_UNKNOWN = 128;
    const ERR_ARGUMENTS = 256;


    const TYPE_WITHDRAWAL = 1;
    const TYPE_CONTRIBUTE = 2;
    const TYPE_USER_STRUCTURE_PROFIT = 3;
    const TYPE_OWNER_PROFIT = 5;
    const TYPE_USER_BUY_PLAN = 4;


    public static function getPaySystems() {
        return [
            'perfect_money' => ['label'=>'Perfect Money', 'icon'=>'', 'link'=>'https://perfectmoney.is/signup.html', 'class'=>PerfectMoney::className()],
            'payeer' => ['label'=>'Payeer', 'icon'=>'', 'link'=>'https://payeer.com/ru/account/?register=yes', 'class'=>Payeer::className()]
        ];
    }

    public static $statuses = ['незивестно', 'создан', 'в обработке', 'отменён', 'ошибка', 'успешно'];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transactions}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        ];
    }


    public function attributeLabels() {
        return [
        ];
    }

    /**
     * @param $user User
     * @param $days int
     * @return float
     */
    public static function getProfitUser($user, $days) {
        if (!$days) $days = 100000000;
        return 0.0;
    }

    /**
     * @param $user
     * @return Transaction[]
     */
    public function getTransactionsUser($user) {
        return Transaction::findALl(['owner'=>$user->id]);
    }

    public function getDescription() {
        $desc = "Неизвестная транзакция";
        try {
            $data = json_decode($this->data, true);
            if ($this->type == self::TYPE_CONTRIBUTE) {
                $desc = "Пополнение счёта";
                return "Пополнение счёта (" . self::getPaySystems()[$data['pay_system']]['label'] . ')';
            }
            if ($this->type == self::TYPE_WITHDRAWAL) {
                $desc = "Вывод средств";
                return "Вывод средств(" . self::getPaySystems()[$data['pay_system']]['label'] . ')';
            }
            if ($this->type == self::TYPE_USER_BUY_PLAN) {
                $desc = "Покупка инвестиций";
                $plan = ProjectPlans::findById($data['plan_id']);
                return "Покупка инвестиций (" . $plan->project->name . "-" . $plan->name . ")";
            }
            if ($this->type == self::TYPE_OWNER_PROFIT) {
                $desc = "Пользователь купил план";
                $plan = ProjectPlans::findById($data['plan_id']);
                $email = User::findIdentity($data['fromId'])->email;
                return "Пользователь купил план (" . $plan->project->name . "-" . $plan->name . ") - email: $email";
            }

            if ($this->type == self::TYPE_USER_STRUCTURE_PROFIT) {
                $desc = "Реферальный доход";
                $plan = ProjectPlans::findById($data['plan_id']);
                return "Реферальный доход (" . $plan->project->name . "-" . $plan->name . ")";
            }
        } catch (\Exception $ex) {

        }
        return $desc;
    }

    public function getStatus() {
        return self::$statuses[$this->status];
    }

    /**
     * @param $id
     * @return static
     */
    public static function findById($id) {
        return static::findOne(['id'=>$id]);
    }

    /**
     * @param $type
     * @return string
     */
    public static function getPaySystemWithType($type) {
        return self::getPaySystems()[$type]['class'];
    }


    /**
     * @param $owner User
     * @param $type int
     * @param $amount float
     * @param $options array
     * @return Transaction
     */
    public static function createNewTransaction($owner, $type, $amount, $options=[]) {

        $nw = new Transaction();
        $nw->type=$type;
        $nw->owner = $owner->id;
        $nw->amount = $amount;
        $nw->data = json_encode($options);
        $nw->status = self::STATUS_CREATED;
        $nw->save();

        return $nw;
    }


    /**
     * @param $owner User
     * @param $type int
     * @param $amount float
     * @param $options array
     * @return Transaction
     */
    public static function createAndAcceptNewTransaction($owner, $type, $amount, $options=[]) {

        $nw = new Transaction();
        $nw->type=$type;
        $nw->owner = $owner->id;
        $nw->amount = $amount;
        $nw->data = json_encode($options);
        $nw->status = self::STATUS_CREATED;
        $nw->save();

        $nw->changeStatus(Transaction::STATUS_ACCEPTED, 0);

        return $nw;
    }

    /**
     * @param $nw_status int
     * @param $err_no int
     * @param $options []
     */
    public function changeStatus($nw_status, $err_no, $options=[]) {
        if ($this->status == self::STATUS_ACCEPTED) {
            return;
        }

        $this->status=$nw_status;
        $this->err_no = $err_no;
        $this->data = json_encode(array_merge(json_decode($this->data,true), $options));
        $this->save();

        if ($err_no == 0 and $nw_status == self::STATUS_ACCEPTED) {
            $this->finishTransaction();
        }
    }

    /**
     * Finish transaction
     */
    protected function finishTransaction() {
        switch($this->type) {
            case self::TYPE_CONTRIBUTE:
                User::findIdentity($this->owner)->changeBuyBalance($this->amount)->save();
            break;
            case self::TYPE_USER_STRUCTURE_PROFIT:
            case self::TYPE_OWNER_PROFIT:
                User::findIdentity($this->owner)->changeMainBalance($this->amount)->save();
            break;

            case self::TYPE_USER_BUY_PLAN:
                User::findIdentity($this->owner)->changeBuyBalance(-$this->amount)->save();
            break;
            case self::TYPE_WITHDRAWAL:
                User::findIdentity($this->owner)->changeMainBalance(-$this->amount)->save();
            break;
        }
    }

    public static function getTotalInvested() {
        return floatval(Transaction::find()->where(['type'=>self::TYPE_USER_BUY_PLAN])->sum('amount'));
    }


    /**
     * @param $user User
     * @return static[]
     */
    public static function findAllForUser($user) {
        return static::findAll(['owner'=>$user->id]);
    }
}
