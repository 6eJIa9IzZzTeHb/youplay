<?php


$params = array_merge(
    requireIfExists(__DIR__ . '/../../common/config/params.php'),
    requireIfExists(__DIR__ . '/../../common/config/params-local.php'),
    requireIfExists(__DIR__ . '/params.php'),
    requireIfExists(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'baseUrl'=>'',
            'enableCookieValidation' => true,
            'enableCsrfValidation' => true,
            'cookieValidationKey' => 'eOvhhRWiYqfQdgnWEFOirzp58qDm-EyS',
            'csrfParam' => '_dsdisi_dsdo',

        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            // Hide index.php
            'showScriptName' => false,
            // Use pretty URLs
            'enablePrettyUrl' => true,
            'rules' => [
                'payment/<view:[\w\-]+>' => 'payment/<view>',
                'auth/<view:[\w\-]+>' => 'auth/<view>',
                'auth/reset-password-confirm/<code:[\w\d\_]+>' => 'auth/reset-password-confirm',
                'admin' => 'admin/index',
                'admin/edit/<slug:[\w\-]+>/<type:[\w\-]+>/<id:[\d]+>' => 'admin/edit',
                'admin/edit/<slug:[\w\-]+>/<type:[\w\-]+>' => 'admin/edit',
                'admin/edit/<slug:[\w\-]+>' => 'admin/edit',
                'admin/<view:[\w\-]+>' => 'admin/<view>',
                'game/<view:[\w\-]+>' => 'game/<view>',
                'user/<name:[\w\-]+>' => 'profile/index',
                'profile/<view:[\w\-]+>' => 'profile/<view>',
                'from/<from:[\w\d\-]+>' => 'site/index',
                '<view:[\w-]+>' => 'site/<view>',
                '/' => 'site/index',
            ],
        ],
    ],
    'modules' => [
        'redactor' => [
            'class' => 'yii\redactor\RedactorModule',
            'uploadDir' => '@frontend/web/img/editor-upload',
            'uploadUrl' => '/img/editor-upload',
            'imageAllowExtensions'=>['jpg','png','gif']
        ],
    ],
    'params' => $params,
];
