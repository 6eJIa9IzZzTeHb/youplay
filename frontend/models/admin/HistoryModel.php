<?php

namespace frontend\models\admin;


use common\helpers\SearchFormatHelper;
use common\helpers\SearchModel;
use common\models\Transaction;

use common\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Login form
 */
class HistoryModel extends ActiveRecord
{
    /** @var $modelClass string */
    public static $modelClass = null;

    /**
     * @param string $modelClass
     * @param array $config
     */
    public function __construct($modelClass = null, $config = [])
    {
        if (!is_string($modelClass)) {
            return null;

        }
        self::$modelClass = $modelClass;


        return parent::__construct($config);
    }


    public static function tableName()
    {
        return Transaction::tableName();
    }


    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'user_id' => 'Пользователь',
            'amount' => 'Сумма',
            'type' => 'Тип',
            'data' => 'Описание',
            'status' => 'Статус',
            'created_at' => 'Дата'
        ];
    }

    public function rules()
    {
        return [
            [['id', 'user_id', 'amount', 'type', 'data', 'status', 'created_at'], 'safe']

        ];
    }

    public function gridStrictSearch() {
        return [
            'id', 'user_id', 'type', 'status'
        ];
    }


    public function gridColumns()
    {
        return ['id',
            SearchFormatHelper::select2('user_id', ArrayHelper::map(User::find()->all(), 'id', 'username'), true),
            SearchFormatHelper::moneyFormat('amount'),
            SearchFormatHelper::pickFormat('type', Transaction::getTypesNames()),
            SearchFormatHelper::transactionDescription('data'),
            SearchFormatHelper::pickFormat('status', Transaction::getStatusNames()),
            SearchFormatHelper::dateFormat('updated_at'),
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'updated_at' => SORT_DESC,
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');

            return $dataProvider;
        }

        $strict = self::gridStrictSearch();

        foreach ($this->activeAttributes() as $field) {
            if (!isset($this->$field)) continue;
            $value = $this->$field;
            if (strlen($this->$field) && substr($field, -3) == '_at') {

                $parts = preg_split('/[^\d]+/', $this->$field);
                $day = date('d');
                $month = date('m');
                $year = date('Y');
                $step = 0;
                foreach ($parts as $part) {
                    if ($len = strlen($part)) {
                        if ($len>2) {
                            $year = $part;
                        } else {
                            if ($step == 0) $day = $part;
                            if ($step == 1) $month = $part;
                            if ($step == 2) $year= $part;

                            $step++;
                        }
                    }
                }
                $value = strtotime($year.'-'.$month.'-'.$day);
                $query->andFilterWhere(['>=', 'created_at', $value]);
                $query->andFilterWhere(['<', 'updated_at', $value+24*60*60]);
            } else if (!in_array($field, $strict)) {
                $query->andFilterWhere(['like', $field, $value]);
            } else {
                $query->andFilterWhere([$field => $value]);
            }
        }


        return $dataProvider;
    }


}
