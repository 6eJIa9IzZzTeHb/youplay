<?php

namespace frontend\models\profile;


use common\models\Tariff;
use common\models\TariffToUser;
use common\models\Transaction;
use common\models\transactions\InvestTransaction;
use common\models\User;
use phpnt\cropper\models\Photo;
use Yii;
use yii\base\Model;

/**
 * Login form
 */
class SettingsMyDataForm extends Model
{
    public $username;
    public $email;
    public $phone;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'email'], 'checkUnique'],
            ['phone', 'string', 'max'=>25 ],
            ['email', 'email'],
            ['username', 'string', 'min'=>4],
            [['username', 'email'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username'=>'Логин',
            'email'=>'Email',
            'phone'=>'Телефон',
        ];
    }



    public function getInputType($attr) {
        return 'text';
    }

    public function checkUnique($attr) {
        $obj = User::find()->where([$attr=>$this->$attr])->one();
        if ($obj && $obj->id != User::getCurrentUser()->id) {
            $this->addError($attr, 'Должно быть уникальным');
        }

    }


    public function loadAttributes() {
        $this->username = User::getCurrentUser()->username;
        $this->email = User::getCurrentUser()->email;
        $this->phone = User::getCurrentUser()->phone;
    }

    public function save() {

        $user = User::getCurrentUser();

//        $user->username = $this->username;
//        $user->email = $this->email;
        $user->phone = $this->phone;

        return $user->save();
    }

}
