<?php

namespace frontend\models\profile;


use common\models\Tariff;
use common\models\TariffToUser;
use common\models\Transaction;
use common\models\transactions\FinanceInvestMoneyTransaction;
use common\models\transactions\InvestTransaction;
use common\models\User;
use Yii;
use yii\base\Model;

/**
 * Login form
 */
class FinanceInvestForm extends Model
{
    public $pay_system;
    public $amount;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pay_system', 'amount'], 'required'],
            ['pay_system', 'in', 'range'=>array_keys(Transaction::getPaySystems())],
            ['amount', 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pay_system' => 'Тариф',
            'amount' => 'Цена',
        ];
    }




    /**
     * Last step of owning tariff
     */
    public function finish()
    {





        return true;
    }
}
