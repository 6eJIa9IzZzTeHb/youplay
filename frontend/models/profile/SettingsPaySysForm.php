<?php

namespace frontend\models\profile;


use common\models\Tariff;
use common\models\TariffToUser;
use common\models\Transaction;
use common\models\transactions\InvestTransaction;
use common\models\User;
use phpnt\cropper\models\Photo;
use Yii;
use yii\base\Model;

/**
 * Login form
 */
class SettingsPaySysForm extends Model
{
    public $adv;
    public $payeer;
    public $perfect_money;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['adv', 'payeer', 'perfect_money'], 'string', 'max'=>100 ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'adv'=>'ADV',
            'payeer'=>'PAYEER',
            'perfect_money'=>'PERFECT MONEY',
        ];
    }



    public function getInputType($attr) {
        return 'text';
    }



    public function loadAttributes() {
        foreach (['adv', 'payeer', 'perfect_money'] as $pay_system) {
            $this->$pay_system = User::getCurrentUser()->getAccount($pay_system);
        }
    }

    public function save() {

        $user = User::getCurrentUser();

        foreach (['adv', 'payeer', 'perfect_money'] as $pay_system) {
            User::getCurrentUser()->setAccount($pay_system, $this->$pay_system);
        }

        return $user->save();
    }

}
