<?php

namespace frontend\models\profile;


use common\models\Tariff;
use common\models\TariffToUser;
use common\models\Transaction;
use common\models\transactions\InvestTransaction;
use common\models\User;
use phpnt\cropper\models\Photo;
use Yii;
use yii\base\Model;

/**
 * Login form
 */
class SettingsPasswordForm extends Model
{
    public $oldPass;
    public $newPass;
    public $confirmPass;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['newPass', 'confirmPass'], 'required'],
            ['confirmPass', 'compare', 'compareAttribute' => 'newPass'],
            ['newPass', 'string', 'min' => 8],
            ['newPass', 'match', 'pattern' => '/([a-z]+\w*[A-Z]+|[A-Z]+\w*[a-z]+)\w*/', 'message'=>'Долна быть хотя бы одна строчная и одна заглавная буква'],

            ['oldPass', 'checkPass'],
            ['oldPass', 'required']

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'oldPass'=>'Старый пароль',
            'newPass'=>'Новый пароль',
            'confirmPass'=>'Повтор пароля',
        ];
    }




    public function checkPass($attr) {
        if (!User::getCurrentUser()->validatePassword($this->$attr)) {
            $this->addError($attr, 'Неверный пароль');
        }
    }

    public function getInputType($attr) {
        return 'password';
    }

    public function loadAttributes() {
    }

    public function save() {

        $user = User::getCurrentUser();

        $user->setPassword($this->newPass);

        return $user->save();
    }

}
