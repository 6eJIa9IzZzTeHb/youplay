<?php

namespace frontend\models\profile;


use common\models\Tariff;
use common\models\TariffToUser;
use common\models\Transaction;
use common\models\transactions\InvestTransaction;
use common\models\User;
use phpnt\cropper\models\Photo;
use Yii;
use yii\base\Model;

/**
 * Login form
 */
class SettingsPictureForm extends Model
{
    public $pic;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
        ];
    }



    public function getId($field) {
        return User::getCurrentUser()->id;
    }

    public function getLabel($field) {
        return User::tableName().':'.$this->getId($field).':'.$field;
    }

    public function getPictures($field) {
        $user = User::getCurrentUser();
        $where = [
            'object_id' => $this->getId($field),
            'deleted' => 0,
            'type' => $this->getLabel($field)
        ];

        Photo::updateAll([
            'deleted' => 1,
        ], $where);

        Photo::updateAll([
            'deleted' => 0,
        ], ['file'=>$user->pic]);


        return Photo::find()->where($where)->all();
    }


    public function save() {

        $user = User::getCurrentUser();
        $photo = Photo::find()->where([
            'object_id' => $this->getId('pic'),
            'deleted' => 0,
            'type' => $this->getLabel('pic')
        ])->one();
        $user->pic = $photo ? $photo->file : '/img/user.png';
        $user->save();

        return $user->save();
    }

}
