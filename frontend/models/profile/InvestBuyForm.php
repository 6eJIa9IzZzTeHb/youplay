<?php

namespace frontend\models\profile;


use common\models\Tariff;
use common\models\TariffToUser;
use common\models\Transaction;
use common\models\transactions\InvestTransaction;
use common\models\User;
use Yii;
use yii\base\Model;

/**
 * Login form
 */
class InvestBuyForm extends Model
{
    public $type;
    public $amount;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'amount'], 'required'],
            ['amount', 'number'],
            ['amount', 'checkType'],
            ['amount', 'checkAmount']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type' => 'Тариф',
            'amount' => 'Цена',
        ];
    }

    public function checkAmount()
    {
        if ($this->amount - User::getCurrentUser()->balance > 0) {
            $this->addError('amount', 'Недостаточно средств');
        }
    }

    public function checkType()
    {

        $count = TariffToUser::find()
            ->where(['tariff_id' => $this->type])
            ->andWhere(['>=', 'created_at', time()-24*60*60])
            ->count();

        $limit = Tariff::findOne(['id' => $this->type])->limit;

        if ($count >= $limit) {
            $this->addError('amount', 'Вы заказали максимальное количество таких тарифов');
            return;
        }

        $tar = Tariff::findOne(['id'=>$this->type]);
        if ($this->amount < $tar->start_price || $this->amount > $tar->end_price) {
            $this->addError('amount', 'Введенная сумма лежит за пределами тарифа');
        }
    }


    /**
     * Last step of owning tariff
     */
    public function finish()
    {


        $transaction = new InvestTransaction(
            User::getCurrentUser()->id,
            $this->amount,
            Yii::$app->request->get('type')
        );


        return $transaction->executeTransaction();
    }
}
