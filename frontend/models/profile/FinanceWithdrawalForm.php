<?php

namespace frontend\models\profile;


use common\models\Tariff;
use common\models\TariffToUser;
use common\models\Transaction;
use common\models\transactions\FinanceInvestMoneyTransaction;
use common\models\transactions\FinanceWithdrawalMoneyTransaction;
use common\models\transactions\InvestTransaction;
use common\models\User;
use Yii;
use yii\base\Model;

/**
 * Login form
 */
class FinanceWithdrawalForm extends Model
{
    public $pay_system;
    public $amount;
    public $account;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pay_system', 'amount', 'account'], 'required'],
            ['pay_system', 'in', 'range'=>array_keys(Transaction::getPaySystems())],
            ['amount', 'number'],
            ['amount', 'checkAmount']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pay_system' => 'Тариф',
            'amount' => 'Цена',
            'account' => 'Аккаунт'
        ];
    }

    public function checkAmount() {
        if (User::getCurrentUser()->balance < $this->amount) {
            $this->addError('amount', 'Недостаточно средств');
        }
    }


    /**
     * Last step of owning tariff
     */
    public function finish()
    {

        $user = User::getCurrentUser();
        $transaction = new FinanceWithdrawalMoneyTransaction($user->id, $this->amount, $this->pay_system, $this->account);

        $transaction->save();


        return true;
    }
}
