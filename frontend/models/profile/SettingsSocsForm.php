<?php

namespace frontend\models\profile;


use common\models\Tariff;
use common\models\TariffToUser;
use common\models\Transaction;
use common\models\transactions\InvestTransaction;
use common\models\User;
use phpnt\cropper\models\Photo;
use Yii;
use yii\base\Model;

/**
 * Login form
 */
class SettingsSocsForm extends Model
{
    public $vk;
    public $facebook;
    public $twitter;
    public $instagram;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vk', 'facebook', 'twitter', 'instagram'], 'string', 'max'=>100 ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vk'=>'VK',
            'facebook'=>'Facebook',
            'twitter'=>'Twitter',
            'instagram'=>'Instagram',
        ];
    }



    public function getInputType($attr) {
        return 'text';
    }



    public function loadAttributes() {
        foreach (array_keys($this->attributeLabels()) as $soc) {
            $this->$soc = User::getCurrentUser()->getSoc($soc);
        }
    }

    public function save() {

        $user = User::getCurrentUser();

        foreach (array_keys($this->attributeLabels()) as $soc) {
            User::getCurrentUser()->setSoc($soc, $this->$soc);
        }

        return $user->save();
    }

}
