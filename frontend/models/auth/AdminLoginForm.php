<?php

namespace frontend\models\auth;


use common\helpers\EmailHelper;
use common\models\User;
use Yii;
use yii\base\Model;
use yii\helpers\Url;

/**
 * Login form
 */
class AdminLoginForm extends Model
{
    public $code;


    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['code', 'required', 'message'=>'Не может быть пустым'],
            ['code', 'string', 'max' => 255],
            ['code', 'checkCode']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code' => 'Код подтверждения',
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function checkCode($attribute, $params)
    {
        if (!User::isPasswordResetTokenValid($this->code)) {
            $this->addError('code', 'Токен недействителен');
            return;
        }


        if (!self::getUser()) {
            $this->addError('code', 'Нет такого пользователя');

        }


    }


    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is signed in successfully
     */
    public function reset()
    {
        if ($this->validate()) {
            $user = self::getUser();

            $user->admin_expired_at = time() + (60 * 60 * 2);
            $user->email_expired_at = 0;

            $user->save();

            return true;
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByPasswordResetToken($this->code);
        }





        return $this->_user;
    }


    public function sendLetter() {
        $user = User::getCurrentUser();
        $user->generatePasswordResetToken();
        $user->email_expired_at = time() + 60*60*2;
        $user->save();

        $link = Url::to(['auth/admin-login', 'code'=>$user->password_reset_token], true);



        return EmailHelper::sendEmail('adminLoginToken', $user->email, 'Запрос в административную панель', [
            'user'=>$user,
            'code'=>$user->password_reset_token,
            'reset_link'=>$link,
        ]);
    }
}
