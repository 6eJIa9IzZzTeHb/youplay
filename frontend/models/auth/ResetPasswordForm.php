<?php

namespace frontend\models\auth;


use common\helpers\EmailHelper;
use common\helpers\SystemHelper;
use common\models\User;
use Yii;
use yii\base\Model;
use yii\helpers\Url;

/**
 * Login form
 */
class ResetPasswordForm extends Model
{
    public $email;


    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'required', 'message'=>'Не может быть пустым'],
            ['email', 'string', 'max' => 255],
            ['email', 'checkEmail']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => 'Логин или Email',
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function checkEmail($attribute, $params)
    {
        if (!($user = self::getUser())) {
            $this->addError('email', 'Нет такого пользователя');
            return;
        }

        if ($user->email_expired_at > time()) {

            $this->addError('email', 'Не чаще 1 раза в минуту');
        }

    }


    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is signed in successfully
     */
    public function reset()
    {
        if ($this->validate()) {
            $user = self::getUser();

            $user->generatePasswordResetToken();
            $user->email_expired_at = time() + 60;
            $user->save();

            $link = Url::to(['auth/reset-password-confirm', 'code'=>$user->password_reset_token], true);

            return EmailHelper::sendEmail('passwordResetToken', $user->email, 'Смена пароля', [
               'user'=>$user,
               'code'=>$user->password_reset_token,
               'reset_link'=>$link,

            ]);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByUsername($this->email);
        }


        if ($this->_user === null) {
            $this->_user = User::findByEmail($this->email);
        }



        return $this->_user;
    }
}
