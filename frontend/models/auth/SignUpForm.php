<?php

namespace frontend\models\auth;


use common\models\User;
use Yii;
use yii\base\Model;
use yii\helpers\Html;

/**
 * Login form
 */
class SignUpForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $retry_password;
    public $accept;


    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password', 'username', 'retry_password'], 'required', 'message'=>'Не может быть пустым'],
            [['email', 'username'], 'trim'],

            ['username', 'required', 'message'=>'Не может быть пустым'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Логин уже используется'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'email', 'message'=>'Не является правильным email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Почта уже зарегестрирована'],

            ['password', 'string', 'min' => 8],
            ['password', 'match', 'pattern' => '/([a-z]+\w*[A-Z]+|[A-Z]+\w*[a-z]+)\w*/', 'message'=>'Долна быть хотя бы одна строчная и одна заглавная буква'],
            ['retry_password', 'compare', 'compareAttribute' => 'password', 'message'=>'пароли не совпадают'],
            ['password', 'compare', 'compareAttribute' => 'username', 'operator' => '!=', 'message'=>'пароль не должен совпадать с логином'],

            ['accept', 'boolean'],
            ['accept', 'compare', 'compareValue' => 1, "type"=>"numeric", 'message'=>'Вы должны согласиться']

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'email' => 'Email',
            'password' => 'Пароль',
            'retry_password' => 'Повторите пароль',
            'accept' => 'Я согласен с '.Html::a('правилами', ['site/license'], ['target'=>'_blank'])
        ];
    }



    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is signed in successfully
     */
    public function signUp()
    {
        if ($this->validate()) {

            $user = User::createNewUser($this->username, $this->email, $this->password);

            $user->login();

            return !$user->hasErrors();
        } else {
            return false;
        }
    }

}
