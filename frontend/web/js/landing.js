/**
 * Created by Андрей on 05.08.2017.
 */

jQuery(function ($) {


    if (typeof youplay !== 'undefined') {
        youplay.init({
            // enable parallax
            parallax: true,

            // set small navbar on load
            navbarSmall: false,

            // enable fade effect between pages
            fadeBetweenPages: true,

            // twitter and instagram php paths
            php: {
                twitter: './php/twitter/tweet.php',
                instagram: './php/instagram/instagram.php'
            }
        });
    }


    $(".countdown").each(function () {
        $(this).countdown($(this).attr('data-end'), function (event) {
            $(this).text(
                event.strftime('%D days %H:%M:%S')
            );
        });
    });


    $('.owl-carousel').each(function() {
        var el = $(this);
        var nav = false;
        if (el.data('nav')) {
            nav = true;
        }
        el.owlCarousel({
            margin: 20,
            loop: true,
            nav: nav,
            dots:nav,
            navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
            autoWidth: true,
            //items: 10,
            autoplay: true,
            autoplayTimeout: 1500,
            autoplayHoverPause: true
        });
    });


//    $('.perfectScroll').perfectScrollbar();


    (function() {
        $('[data-fullscreen]').click(function () {

            if (!document.fullscreenElement && !document.msFullscreenElement && !document.mozFullScreenElement && !document.webkitFullscreenElement) {
                if (document.body.requestFullscreen) {
                    document.body.requestFullscreen();
                } else if (document.body.msRequestFullscreen) {
                    document.body.msRequestFullscreen();
                } else if (document.body.mozRequestFullScreen) {
                    document.body.mozRequestFullScreen();
                } else if (document.body.webkitRequestFullscreen) {
                    document.body.webkitRequestFullscreen();
                }
            } else {
                if (document.exitFullscreen) {
                    document.exitFullscreen();
                } else if (document.msExitFullscreen) {
                    document.msExitFullscreen();
                } else if (document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else if (document.webkitCancelFullScreen) {
                    document.webkitCancelFullScreen();
                }
            }
            var elem = $(this);
            elem.data('fullscreen', !(elem.data('fullsreen')||false));


        });
    })();




    $('[data-toggle-class]').each(function() {

        var elem = $(this);
        function change() {
            var to = elem;
            var selector, classFrom, classTo;
            if (selector = elem.data('toogle-selector')) {
                to = $(elem.data('toogle-selector'));
            }

            if (classFrom = elem.data('toggle-from')) {
                to.toggleClass(classFrom);
            }


            if (classTo = elem.data('toggle-to')) {
                to.toggleClass(classTo);
            }

            return false;
        }

        elem.on(
            elem.data('toggle-class')||'click',
            change
        );

        var events;
        if (events = elem.data('toggle-general-events')) {
            $('body html').on(events, change);
            $(document).on(events, change);
            $(window).on(events, change);

        }
    });


    $(document).on('click', '[data-pjax]', function() {
        var cnt = $($(this).data('pjax-container'));
        if (!cnt.length) return;

        $.ajax({
            url: $(this).data('pjax'),
            dataType:'json',

            success: function(data) {
                cnt.replaceWith(data.content)
            },

            error: function(a,b,c) {
                console.log(a,b,c);
            },

        });

    });
});