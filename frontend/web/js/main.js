
$(function () {
    $('button[data-toggle="pill"]').click(function() {
        $('button[data-toggle="pill"]').removeClass('active');
        $(this).addClass('active');
    });

    $('[data-toggle="pill"].active').click();


    function updateBoxStatus(button) {
        var settings = $.merge({
            okClass: "btn-success",
            failClass: "btn-danger",
            okText: "Да",
            failText: "Нет"
        }, {
            okClass: button.data('okclass'),
            failClass: button.data('failclass'),
            okText: button.data('oktext'),
            failText: button.data('failtext')
        });

        if (button.hasClass(settings.okClass)) {
            button.removeClass(settings.okClass).addClass(settings.failClass);
            button.text(settings.failText);
        } else {
            button.removeClass(settings.failClass).addClass(settings.okClass);
            button.text(settings.okText);
        }

        var chbox = $(button.data('target'));
        chbox.prop("checked", button.hasClass(settings.okClass));
        chbox.val(button.hasClass(settings.okClass)?1:0);

    }


    $('[data-toogle=checkbox]').each(function(e) {
        var button = $(this);

        updateBoxStatus(button);
        if ($(this).data('value') == 1) {
            updateBoxStatus(button);
        }

        button.parent().mousedown(function() {
            updateBoxStatus(button);
        });

        return false;
    });


    $('[data-toggle="toggleClass"]').click(function() {
       $(this).toggleClass($(this).data('class'));
    });

    $('[data-toggle="btn-select"]').each(function() {
       var btn = $(this);
       btn.find('li').click(function() {
          btn.find('button').html($(this).data('val') + ' <span class="caret"></span>');
       });
    });


    var clip = new Clipboard('[data-toggle=clipboard');
    clip.on('success', function(e) {
        $.notify('ссылка успешно скопированна', 'success');
    });



    //typepick
    $('[data-toggle=typePickContainer]').each(function() {
        var el = $(this);
        var val = el.data("value");
        var targetId = el.data('target');
        if (!val || !targetId) return;
        $('[data-target='+targetId+'][data-value='+val+']').toggleClass('light-purpure').toggleClass('ping');
    });

    $('[data-toggle=typePickContainer] [data-target][data-value]').click(function() {
        var el = $(this);
        var target = el.data('target');
        $('[data-target='+target+'].ping').removeClass('ping').addClass('light-purpure');

        el.removeClass('light-purpure').addClass('ping');

        $('#' + el.data('target')).val(el.data('value'));

        el.blur();
    });


    $('[data-hint]').each(function() {
        var el = $(this);
        var refresh = function(el) {
            $('#'+el.data('hintinput')).val(el.data('hint'));
        };
        if (el.hasClass('ping')) {
            refresh(el);
        }
        el.click(function() {
            refresh(el);
        });
    });

    $('[data-href]').click(function() {
        document.location = $(this).data('href');
    });

    $('[data-toggle=time]').each(function() {
        var el = $(this);

        setInterval(function() {
            el.data('timestamp', (el.data('timestamp')*1) + 1);

            var date = new Date(el.data('timestamp')*1000);

            var year = "0" + date.getFullYear();

            var day = "0" + date.getDate();

            var month = "0" + (date.getMonth()+1);

            var hours = "0" + date.getHours();

            var minutes = "0" + date.getMinutes();

            var seconds = "0" + date.getSeconds();

            var ftime = "", stime = "";

            ftime += day.substr(-2) + "." + month.substr(-2) +'.'+ year.substr(-4);
            stime += hours.substr(-2) + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);

            el.find('.date-cnt').html(ftime);
            el.find('.time-cnt').html(stime);
        }, 1000);
    });


    setTimeout(function() {
        $('body').append($('.angled-img .modal'));
    }, 1000);
});