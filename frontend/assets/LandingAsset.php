<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class LandingAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.min.css',
        'css/font-awesome.min.css',
        'css/owl.carousel.min.css',
        'css/youplay.min.css',
        'css/settings.css',
        'http://fonts.googleapis.com/css?family=Lato:300,400,700',
        'css/site.css',
        'css/landing.css',
        'css/perfect-scrollbar.min.css'
    ];
    public $js = [
        'js/bootstrap.min.js',
        'js/jquery.themepunch.revolution.min.js',
        'js/jquery.themepunch.tools.min.js',
        'js/jquery.hexagonprogress.min.js',
        'js/jarallax.min.js',
        'js/SmoothScroll.js',
        'js/owl.carousel.min.js',
        'js/jquery.countdown.min.js',
        'js/youplay.min.js',
        'js/perfect-scrollbar.jquery.min.js',
        'js/shortcut.js',
        'js/landing.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

}
