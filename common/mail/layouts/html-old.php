<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>

    <div style="padding:40px; background-color: #201c3a; text-align: center;"><img src="<?=Yii::$app->homeUrl?>/img/joistik.png" alt=""/></div>
    <div style="background: #fef; text-align: justify; padding:20px;">
        <?= $content ?>

    </div>
    <div style="padding:40px; background-color: #201c3a; text-align: center;">

        <?php

        use common\models\Option;
        ?>

        <div style="text-align: center; padding:10px; color:white;">
            <a class="btn" href="<?= Option::getOption('social_facebook')?>">facebook</a>
            <a class="btn" href="<?= Option::getOption('social_instagram')?>">insagram</a>
            <a class="btn" href="<?= Option::getOption('social_skype')?>">skype</a>
            <a class="btn" href="<?= Option::getOption('social_telegram')?>">telegram</a>
            <a class="btn" href="<?= Option::getOption('social_vk')?>">vk</a>
        </div>
        <div style="text-align: center; padding:10px;  color:white;">
            <a href="<?= Yii::$app->homeUrl?>">перейти на сайт</a>
        </div>
    </div>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
