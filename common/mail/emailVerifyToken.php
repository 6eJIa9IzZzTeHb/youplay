<?php

/**
 * @var $this \yii\web\View
 * @var $user \common\models\User
 * @var $reset_link string
 * @var $code string
 */

?>
<h3>Здравствуйте, <?= $user->username?></h3>

<br/>

<p>Вы запросили код к подверждению почты. Введите этот код: <?= $code?> в форму подтверждения пароля или перейдите по <?= \yii\helpers\Html::a('ccылке', $reset_link)?></p>

<br/><br/>
<p>С уважением,
    <br/>
    Команда <a href='<?= Yii::$app->homeUrl?>'>youplay.com</a>
</p>
<p>Письмо отправлено роботом, отвечать на него не нужно</p>