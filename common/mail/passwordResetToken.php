<?php

/**
 * @var $this \yii\web\View
 * @var $user \common\models\User
 * @var $reset_link string
 * @var $code string
 */

?>
<h3>Здравствуйте, <?= $user->username?></h3>

<br/>

<p>Вы запросили смену пароля. Введите этот код: <?= $code?> в форму подтверждения пароля или перейдите по <?= \yii\helpers\Html::a('ccылке', $reset_link)?></p>
