<?php
namespace common\models;

use common\helpers\HtmlSettingsHelper;
use common\helpers\IpHelper;
use common\helpers\SearchEditHelper;
use common\helpers\SearchFormatHelper;
use common\helpers\SearchRecord;
use common\helpers\SystemHelper;
use phpnt\cropper\models\Photo;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $phone
 * @property string $pic
 * @property integer $role
 * @property integer $status
 * @property integer $structure_size
 * @property integer $admin_expired_at
 * @property integer $email_expired_at
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $refer
 * @property float $balance
 * @property string $structure_invest
 * @property string $total_profit
 * @property string $profit_invest
 * @property string $withdraw
 * @property bool $is_verified
 *
 * @property Auth[] $auths
 * @property TariffToUser[] $tariffToUsers
 * @property Transaction[] $transactions
 * @property User $referObj
 * @property User[] $users
 * @property UserIp[] $userIps
 * @property UserPaySys[] $userPaySys
 * @property UserSocs[] $userSocs
 */
class User extends SearchRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;


    const ROLE_ADMIN = 1;
    const ROLE_VIP = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function beforeSave($insert) {

        $this->balance = number_format($this->balance, 4, '.', '');
       // file_put_contents(__DIR__.'/1.txt', $this->id.':'.$this->balance.'\n', FILE_APPEND);

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            HtmlSettingsHelper::decimalRule(['balance', 'structure_invest', 'total_profit', 'profit_invest', 'withdraw']),
            [['username', 'auth_key', 'password_hash', 'email', 'admin_expired_at', 'email_expired_at'], 'required'],
            [['role', 'status', 'email_expired_at', 'refer'], 'integer'],
            [['balance', 'structure_invest', 'total_profit', 'profit_invest', 'withdraw'], 'number'],
            [['username', 'password_hash', 'password_reset_token', 'email', 'phone', 'pic'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
            [['refer', 'structure_size', 'is_verified'], 'integer'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Имя',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'phone' => 'Phone',
            'pic' => 'Pic',
            'role' => 'Администратор',
            'status' => 'Status',
            'admin_expired_at' => 'Admin Expired At',
            'email_expired_at' => 'Email Expired At',
            'created_at' => 'Создан',
            'updated_at' => 'Последняя активность',
            'refer' => 'Refer',
            'balance' => 'Баланс (в формате Х.ХХХХ именно с четырьмя знаками)',
            'structure_size' => 'Structure Size',
            'structure_invest' => 'Structure Invest',
            'total_profit' => 'Total Profit',
            'profit_invest' => 'Profit Invest',
            'withdraw' => 'Withdraw',
        ];
    }


    public function adminEditTypes() {
        return [
            'pic'=>SearchEditHelper::TYPE_PIC_BOX,
            'username'=>SearchEditHelper::TYPE_TEXT_FIELD,
            'email'=>SearchEditHelper::TYPE_TEXT_FIELD,
            'phone'=>SearchEditHelper::TYPE_TEXT_FIELD,
            'refer'=>['type'=>SearchEditHelper::TYPE_FOREIGN, 'values'=>User::find()->all(), 'keyName'=>'id', 'valueName'=>'username'],
            'balance'=>SearchEditHelper::TYPE_TEXT_FIELD,
            'role'=>SearchEditHelper::TYPE_CHECK_BOX,
            'admin_expired_at'=>SearchEditHelper::TYPE_DATE,
        ];
    }

    public static function gridColumns() {
        return ['id',
            SearchFormatHelper::select2('username', ArrayHelper::map(User::find()->all(), 'username', 'username')),
            SearchFormatHelper::select2('refer', ArrayHelper::map(User::find()->all(), 'id', 'username')),
            'email',
            'phone',
            SearchFormatHelper::moneyFormat('balance'),
            SearchFormatHelper::dateFormat('created_at'),
            SearchFormatHelper::dateFormat('updated_at'),
            SearchFormatHelper::moneyFormat('balance')];
    }


    public static function adminNewObj() {
        $login = Yii::$app->security->generateRandomString(8);
        return User::createNewUser($login, $login.'@mail.com', $login, false);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuths()
    {
        return $this->hasMany(Auth::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffToUsers()
    {
        return $this->hasMany(TariffToUser::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions()
    {
        return $this->hasMany(Transaction::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReferObj()
    {
        return $this->hasOne(User::className(), ['id' => 'refer']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['refer' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserIps()
    {
        return $this->hasMany(UserIp::className(), ['user_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPaySys()
    {
        return $this->hasMany(UserPaySys::className(), ['user_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserSocs()
    {
        return $this->hasMany(UserSocs::className(), ['user_id' => 'id']);
    }


    public function getPhotos()
    {
        return $this->hasMany(Photo::className(),
            [
                'object_id' => 'id'
            ])->andWhere(['deleted' => 0]);
    }


    /**
     * @param int $id
     * @return static
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }


    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByEmail($username)
    {
        return static::findOne(['email' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->admin_expired_at = 0;
        $this->email_expired_at = 0;
        $this->password_reset_token = "";
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return Url::to(['profile/index', 'name' => $this->username]);
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return ($this->role & self::ROLE_ADMIN);
    }


    /**
     * @return bool
     */
    public function isVip()
    {
        return ($this->role & self::ROLE_VIP);
    }

    /**
     * Return true if user can be in admin panel right now
     * @return bool
     */
    public function hasAdminKey() {

    //    return true;

       return $this->admin_expired_at>time();
    }

    /**
     * @var $username
     * @var $email
     * @var $password
     * @var $save
     * @returns User
     */
    public static function createNewUser($username, $email, $password, $save = true) {
        $user = new User();
        $user->username = $username;
        $user->email = $email;
        $user->setPassword($password);
        $user->generateAuthKey();
        $user->password_reset_token = null;

        /**
         * Strict
         */
        $user->refer = SystemHelper::getId();
        $user->structure_size = 0;
        $user->balance = 0;
        $user->structure_invest = 0;
        $user->profit_invest = 0;
        $user->total_profit = 0;
        $user->withdraw = 0;
        $user->is_verified = 0;


        /**
         * Settings
         */
        $user->phone = "";
        $user->pic = "/img/user.png";
        $user->role = 0;
        $user->admin_expired_at = 0;
        $user->email_expired_at = 0;
        $user->status = User::STATUS_ACTIVE;


        if ($save) {
            $user->save();
        }

        $current = $user;
        while ($current->id != $current->refer) {
            $current = $current->referObj;
            $current->structure_size++;
            $current->save();
        }

        return $user;
    }

    /**
     * @return bool
     */
    public function login() {
        $ip = new UserIp();
        $ip->user_id = $this->id;
        $ip->ip = IpHelper::getIpAsInt();
        $ip->save();

        return Yii::$app->user->login($this, 0);
    }

    /**
     * @returns User|null
     */
    public static function getCurrentUser() {
        if (Yii::$app->user->isGuest) return User::adminNewObj();
        return Yii::$app->user->getIdentity();
    }

    /**
     * @returns string
     */
    public function getReferralLink() {
        return Url::to(['site/index', 'from'=>$this->username], true);
    }

    /**
     * @param $pay_system
     * @returns string
     */
    public function getAccount($pay_system) {
        foreach ($this->userPaySys as $uss) {
            if ($uss->source_id == $pay_system) {
                return $uss->source;
            }
        }

        return "";
    }

    /**
     * @param $pay_system string
     * @param $value string
     */
    public function setAccount($pay_system, $value) {
        $uss = null;
        foreach ($this->userPaySys as $ussC) {
            if ($ussC->source_id == $pay_system) {
                $uss = $ussC;
                break;
            }
        }

        if (!$uss) {
            $uss = new UserPaySys();
            $uss->source_id = $pay_system;
            $uss->user_id = User::getCurrentUser()->id;
        }
        $uss->source = $value;
        $uss->save();
    }

    /**
     * @param $pay_system
     * @returns string
     */
    public function getSoc($soc) {
        foreach ($this->userSocs as $userC) {
            if ($userC->source_id == $soc) {
                return $userC->source;
            }
        }

        return "";
    }

    /**
     * @param $soc string
     * @param $value string
     */
    public function setSoc($soc, $value) {
        $uss = null;
        foreach ($this->userSocs as $ussC) {
            if ($ussC->source_id == $soc) {
                $uss = $ussC;
                break;
            }
        }

        if (!$uss) {
            $uss = new UserSocs();
            $uss->source_id = $soc;
            $uss->user_id = User::getCurrentUser()->id;
        }
        $uss->source = $value;
        $uss->save();

        if (!strlen($value)) {
            $uss->delete();
        }
    }
}
