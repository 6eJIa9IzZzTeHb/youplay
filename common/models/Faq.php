<?php

namespace common\models;

use common\helpers\SearchEditHelper;
use common\helpers\SearchRecord;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "faq".
 *
 * @property integer $id
 * @property string $name
 * @property string $value
 * @property integer $created_at
 * @property integer $updated_at
 */
class Faq extends SearchRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faq';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'string', 'max' => 255],
            ['value', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'value' => 'Value',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }


    public function adminEditTypes() {
        return [
            'name' => SearchEditHelper::TYPE_TEXT_FIELD,
            'value' => SearchEditHelper::TYPE_WSYG_EDITOR,
        ];
    }

    public static function gridColumns() {
        return ['name', 'value'];
    }

    public function getUrl() {
        return Url::to(['site/faq']);
    }



}
