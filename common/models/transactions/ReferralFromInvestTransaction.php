<?php

namespace common\models\transactions;

use common\models\Transaction;
use common\models\User;
use Yii;

/**
 * This is the model class for table "transaction".
 */
class ReferralFromInvestTransaction extends Transaction
{

    public $parentTransaction = null;

    /**
     * @param int $to
     * @param $amount
     * @param $fromId int
     * @param $parentTransactionId
     * @param $tariffToUserId
     * @param $lvl
     * @param $params
     */
    public function __construct($to=null, $amount=null, $fromId=null, $parentTransactionId=null, $tariffToUserId=null, $lvl=null, $params = [])
    {
        if (is_array($to) || is_null($to)) {
            parent::__construct($to);
            return;
        }

        parent::__construct($to, $amount, $params);

        //additional params
        $this->data = json_decode($this->data, true);


        $data = [];
        $data['from'] = $fromId;
        $data['parent_transaction'] = $parentTransactionId;
        $data['tariff_user_id'] = $tariffToUserId;
        $data['lvl'] = $lvl;

        $this->data = json_encode(
            array_merge(
                $this->data,
                $data
            )
        );
    }

    /**
     * @returns InvestTransaction
     */
    public function getParentTransaction() {

        if (!$this->parentTransaction) {
            $this->parentTransaction = InvestTransaction::query()->where(['id'=>$this->getDataAsArray()['parent_transaction']])->one();
        }
        return $this->parentTransaction;
    }


    public function description() {
        $data = $this->getDataAsArray();

        $from = User::findOne(['id'=>$data['from']]);

        return "Реферальных доход, источник: ".$from->username;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    protected static function activeQuery()
    {
        return static::find()->where(['type' => static::typeId()]);
    }


    /**
     * @return int
     */
    public static function typeId()
    {
        return static::TYPE_STRUCTURE_INVEST_PROFIT;
    }


    /**
     * @return bool
     */
    public function executeTransaction()
    {
        //transaction logic
        $user = User::findIdentity($this->user_id);



        //change user balance
        $user->balance += floatval($this->amount);
        $user->profit_invest += floatval($this->amount);

        $user->save();


        //mark transaction as ok finished
        $this->status = static::STATUS_OK;
        $this->save();

        return parent::executeTransaction();
    }

}
