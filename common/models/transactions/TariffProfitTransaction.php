<?php

namespace common\models\transactions;

use common\models\TariffToUser;
use common\models\Transaction;
use common\models\User;
use Yii;

/**
 * This is the model class for table "transaction".
 */
class TariffProfitTransaction extends Transaction
{
    /**
     * @param int $to
     * @param $amount float
     * @param $tarToUserId
     * @param $params
     */
    public function __construct($to=null, $amount=null, $tarToUserId=null, $params = [])
    {
        if (is_array($to) || is_null($to)) {
            parent::__construct($to);
            return;
        }
        parent::__construct($to, $amount, $params);

        //additional params
        $this->data = json_decode($this->data, true);


        $data = [];
        $data['tarToUserId'] = $tarToUserId;

        $this->data = json_encode(
            array_merge(
                $this->data,
                $data
            )
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    protected static function activeQuery()
    {
        return static::find()->where(['type' => static::typeId()]);
    }

    /**
     * @return int
     */
    public static function typeId()
    {
        return static::TYPE_PROFIT_TARIFF;
    }

    public function description() {
        $data = $this->getDataAsArray();

        $ttu = TariffToUser::findOne(['id'=>$data['tarToUserId']]);

        return "Доход от тарифа ".$ttu->tariff->name;
    }

    /**
     * @return bool
     */
    public function executeTransaction()
    {
        //transaction logic
        $user = User::findIdentity($this->user_id);



        //change user balance
        $user->balance += floatval($this->amount);
        $user->total_profit += floatval($this->amount);
        $user->save();


        //mark transaction as ok finished
        $this->status = static::STATUS_OK;
        $this->save();

        return parent::executeTransaction();
    }

}
