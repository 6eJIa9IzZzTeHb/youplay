<?php

namespace common\models\transactions;

use common\models\Transaction;
use common\models\User;
use Yii;

/**
 * This is the model class for table "transaction".
 */
class FinanceInvestMoneyTransaction extends Transaction
{
    /**
     * @param int $to
     * @param $amount
     * @param string $pay_system
     * @param $params
     */
    public function __construct($to = null, $amount = null, $pay_system = null, $params = [])
    {
        if (is_array($to) || is_null($to)) {
            parent::__construct($to);
            return;
        }

        parent::__construct($to, $amount, $params);
        $this->type = static::typeFromTypeName($pay_system);

        //additional params
        $this->data = json_decode($this->data, true);


        $data = [];
        $data['pay_system'] = $pay_system;

        $this->data = json_encode(
            array_merge(
                $this->data,
                $data
            )
        );
    }



    public function description() {
        $data = $this->getDataAsArray();

        return "Ввод средств: ".number_format($this->amount, 2)." $";
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    protected static function activeQuery()
    {
        return static::find()->where(['in', 'type', static::typeId()]);
    }

    /**
     * @return []
     */
    public static function typeId()
    {
        return array_values(static::types());
    }

    public static function types() {
        return [
            'adv'=>static::TYPE_INVEST_ADV,
            'perfect_money'=>static::TYPE_INVEST_PERFECT,
            'payeer'=>static::TYPE_INVEST_PAYEER,
        ];
    }

    public static function typeFromTypeName($type) {
        return static::types()[$type];
    }



    /**
     * @param $status int
     * @param $params []
     * @return bool
     */
    public function execute($status, $params)
    {

        if ($status == Transaction::STATUS_OK && $this->status != Transaction::STATUS_OK) {
            //transaction logic
            $user = User::findIdentity($this->user_id);

            //balance update
            $user->balance += floatval($this->amount);
            $user->save();
        }

        //update
        $this->status = $status;
        $this->data = json_encode(
            array_merge(
                $this->getDataAsArray(),
                $params
            )
        );
        $this->save();


        return parent::executeTransaction();
    }

}
