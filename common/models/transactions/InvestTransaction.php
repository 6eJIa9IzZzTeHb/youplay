<?php

namespace common\models\transactions;

use common\models\ReferralProfit;
use common\models\Tariff;
use common\models\TariffToUser;
use common\models\Transaction;
use common\models\User;
use Yii;

/**
 * This is the model class for table "transaction".
 */
class InvestTransaction extends Transaction
{
    /**
     * @param int $to
     * @param $amount
     * @param $typeId
     * @param $params
     */
    public function __construct($to=null, $amount=null, $typeId=null, $params = [])
    {
        if (is_array($to) || is_null($to)) {
            parent::__construct($to);
            return;
        }
        parent::__construct($to, $amount, $params);

        //additional params
        $this->data = json_decode($this->data, true);

        $data = [];
        $data["tariffType"] = $typeId;

        $this->data = json_encode(
            array_merge(
                $this->data,
                $data
            )
        );
    }


    public function description() {
        $data = $this->getDataAsArray();
        $tar = Tariff::findOne($data["tariffType"]);

        return "Покупка тарифа ".$tar->name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    protected static function activeQuery()
    {
        return static::find()->where(['type' => static::typeId()]);
    }

    /**
     * @return int
     */
    public static function typeId()
    {
        return static::TYPE_INVEST;
    }


    /**
     * @return bool
     */
    public function executeTransaction()
    {
        //transaction logic
        $data = $this->getDataAsArray();
        $tar = Tariff::findOne(['id' => $data['tariffType']]);
        $user = User::findIdentity($this->user_id);



        // increase number when tariff used
        $tar->times++;
        $tar->save();


        //create new tarToUser object
        $tarUser = new TariffToUser();
        $tarUser->user_id = $user->id;
        $tarUser->tariff_id = $tar->id;
        $tarUser->cost = $this->amount;
        $tarUser->expired_at = time() + $tar->time * 24 * 60 * 60;
        $tarUser->save();


        //change user balance
        $user->balance -= $this->amount;
        $user->structure_invest += floatval($this->amount);

        $user->save();

        //mark transaction as ok finished
        $this->status = static::STATUS_OK;

        $this->save();


        //referal profit
        /** @var ReferralProfit[] $refs */
        $refs = ReferralProfit::find()
            ->where(['type' => $user->isVip()])
            ->orderBy('lvl')->all();

        if ($refCount = sizeof($refs)) {
            $current = $user;
            $current_lvl = 0;
            for ($current_ref_id = 0; $current_ref_id < $refCount;) {
                $ref = $refs[$current_ref_id];
                if ($current_lvl == $ref->lvl) {

                    $transaction = new ReferralFromInvestTransaction(
                        $current->id,
                        $this->amount * $ref->percent,
                        $user->id,
                        $this->id,
                        $tarUser->id,
                        $current_lvl
                    );
                    $transaction->executeTransaction();

                    $current_ref_id++;
                }

                if ($current->refer != null) {
                    $current = $current->referObj;
                }
                $current_lvl++;
            }


        }


        return parent::executeTransaction();
    }

}
