<?php

namespace common\models\transactions;

use common\models\Transaction;
use common\models\User;
use Yii;

/**
 * This is the model class for table "transaction".
 */
class FinanceWithdrawalMoneyTransaction extends Transaction
{
    /**
     * @param int $to
     * @param $amount
     * @param string $pay_system
     * @param $account
     * @param $params
     */
    public function __construct($to = null, $amount = null, $pay_system = null, $account = null, $params = [])
    {
        if (is_array($to) || is_null($to)) {
            parent::__construct($to);
            return;
        }

        parent::__construct($to, $amount, $params);
        $this->type = static::typeFromTypeName($pay_system);

        //additional params
        $this->data = json_decode($this->data, true);


        $data = [];
        $data['pay_system'] = $pay_system;
        $data['account'] = $account;

        $this->data = json_encode(
            array_merge(
                $this->data,
                $data
            )
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    protected static function activeQuery()
    {
        return static::find()->where(['in', 'type', static::typeId()]);
    }


    public function description() {
        $data = $this->getDataAsArray();

        return "Вывод средств: ".number_format($this->amount, 2)." $";
    }


    /**
     * @return []
     */
    public static function typeId()
    {
        return array_values(static::types());
    }

    public static function types() {
        return [
            'adv'=>static::TYPE_WITHDRAWAL_ADV,
            'perfect_money'=>static::TYPE_WITHDRAWAL_PERFECT,
            'payeer'=>static::TYPE_WITHDRAWAL_PAYEER,
        ];
    }

    public static function typeFromTypeName($type) {
        return static::types()[$type];
    }


    public function executeFirstStep($status, $params=[]) {


        //transaction logic
        $user = User::findIdentity($this->user_id);

        if ($user->balance < $this->amount) {
            $status = Transaction::STATUS_ERROR;
        }

        //update
   //     $this->status = $status;
        $this->data = json_encode(
            array_merge(
                $this->getDataAsArray(),
                $params
            )
        );
        $this->save();


        if ($status == Transaction::STATUS_OK && $this->status != Transaction::STATUS_OK) {
            $className = Transaction::getPaySysFromType($this->type);
            call_user_func([$className, 'makeWithdrawal'], $this->id);
        }
    }

    /**
     * @param $status int
     * @param $params []
     * @return bool
     */
    public function execute($status, $params=[])
    {

        //transaction logic
        $user = User::findIdentity($this->user_id);


        $this->data = json_encode(
            array_merge(
                $this->getDataAsArray(),
                $params
            )
        );
        $this->save();


        if ($this->status == Transaction::STATUS_OK) {
            return parent::executeTransaction();
        }
        if ($status == Transaction::STATUS_OK && $user->balance > $this->amount) {
            //balance update
            $user->balance -= floatval($this->amount);
            $user->withdraw += floatval($this->amount);
       //     file_put_contents(__DIR__.'/1.txt', $this->id.':'.floatval($this->amount).':'.$user->balance.':'.($user->balance-floatval($this->amount)).'\n', FILE_APPEND);
            $user->save();
        }



        //update
        $this->status = $status;

        $this->save();


        return parent::executeTransaction();
    }

}
