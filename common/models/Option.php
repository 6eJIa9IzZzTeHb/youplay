<?php

namespace common\models;

use common\helpers\SearchEditHelper;
use common\helpers\SearchFormatHelper;
use common\helpers\SearchRecord;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Exception;
use yii\helpers\Url;

/**
 * This is the model class for table "option".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $name
 * @property string $value
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $type
 * @property string $slug
 *
 * @property OptionCategory $category
 */
class Option extends SearchRecord
{
    public static $options = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'option';
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'slug'], 'required'],
            [['category_id', 'type'], 'integer'],
            [['name', 'value'], 'safe'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => OptionCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Категория',
            'slug' => 'Системное название',
            'name' => 'Описание',
            'value' => 'Value',
            'type' => 'Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function adminEditTypes() {
        return [
            'category_id' => ['type'=>SearchEditHelper::TYPE_FOREIGN, 'values'=>OptionCategory::find()->all()],
            'name' => SearchEditHelper::TYPE_TEXT_FIELD,
            'slug' => SearchEditHelper::TYPE_TEXT_FIELD,
            'type' => ['type'=>SearchEditHelper::TYPE_SELECT, 'data'=>SearchEditHelper::typeLabels()],
            'value' => $this->type
        ];
    }

    public static function gridColumns() {
        return ['name', SearchFormatHelper::foreignFormat('category_id', OptionCategory::className()),  'slug', 'value', SearchFormatHelper::dateFormat('created_at')];
    }

    public function getUrl() {
        return Url::to(['admin/article', 'slug'=>'option', 'type'=>'update', 'id'=>$this->id]);
    }


    public static function adminNewObj() {
        $obj = parent::adminNewObj();
        $obj->type = SearchEditHelper::TYPE_TEXT_FIELD;
        return $obj;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(OptionCategory::className(), ['id' => 'category_id']);
    }


    /**
     * @param $slug
     * @param $asText
     * @return string|Option
     * @throws Exception
     */
    public static function getOption($slug, $asText=true) {
        if (!array_key_exists($slug, static::$options)) {

            /**
             * @var $option Option
             */
            $option = static::findOne(['slug'=>$slug]);

            if (!$option) {
                throw new Exception('no option with this slug');
            }

            /**
             * @var $options Option[]
             */
            $options = static::findAll(['category_id'=>$option->id]);
            foreach ($options as $obj) {
                static::$options[$obj->slug] = $obj;
            }
        } else {
            $option = static::$options[$slug];
        }

        return $asText ? $option->value : $option;
    }
}
