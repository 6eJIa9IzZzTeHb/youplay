<?php

namespace common\models;

use common\helpers\SearchEditHelper;
use common\helpers\SearchFormatHelper;
use common\helpers\SearchRecord;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "article_category".
 *
 * @property integer $id
 * @property string $name
 * @property integer $count
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Article[] $articles
 */
class ArticleCategory extends SearchRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_category';
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'count' => 'Count',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }


    public function adminEditTypes() {
        return [
            'name' => SearchEditHelper::TYPE_TEXT_FIELD,
        ];
    }

    public static function gridColumns() {
        return ['name', SearchFormatHelper::dateFormat('created_at')];
    }

    public function getUrl() {
        return Url::to(['site/news']);
    }




    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['category_id' => 'id']);
    }
}
