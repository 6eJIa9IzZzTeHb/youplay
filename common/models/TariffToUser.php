<?php

namespace common\models;

use common\helpers\HtmlSettingsHelper;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "tariff_to_user".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $tariff_id
 * @property integer $cost
 * @property integer $profit
 * @property integer $status
 * @property integer $expired_at
 * @property integer $cron_profit_at
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Tariff $tariff
 * @property User $user
 */
class TariffToUser extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_CLOSED = 2;


    /**
     * @param array $params
     */
    public function __construct($params = []) {
        $this->status = self::STATUS_ACTIVE;
        $this->profit = 0;
        $this->cron_profit_at = null;
        return parent::__construct($params);
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert) {
        if ($this->isNewRecord && !$this->cron_profit_at) {
            $this->cron_profit_at = time();
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tariff_to_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            HtmlSettingsHelper::decimalRule('profit'),
            [['user_id', 'tariff_id', 'expired_at', 'cost', 'profit'], 'required'],
            [['user_id', 'tariff_id', 'expired_at', 'status', 'cron_profit_at'], 'integer'],
            [['tariff_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tariff::className(), 'targetAttribute' => ['tariff_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'tariff_id' => 'Tariff ID',
            'expired_at' => 'Expired At',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'cost' => 'Cost',
            'profit' => "Profit"
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(Tariff::className(), ['id' => 'tariff_id']);
    }

    /**
     * @param null|User|int $user
     * @param null|int $status
     * @param bool $outTime
     * @param bool $count
     * @param [] $params
     * @return static[]
     */
    public static function search($user = null, $status = null, $outTime = false, $count = false, $params = []) {
        $q = static::find();
        if ($user) {
            $q->where(['user_id'=>is_object($user)?$user->id:$user]);
        }

        if ($status) {
            $q->andWhere(['in', 'status', $status]);
        }

        if ($outTime) {
            $q->andWhere([($outTime?'>':'<='), 'expired_at', time()]);
        }

        if (sizeof($params)) {
            $q->andWhere($params);
        }

        return $count ? $q->count() : $q->all();
    }
}
