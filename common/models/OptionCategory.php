<?php

namespace common\models;

use common\helpers\SearchEditHelper;
use common\helpers\SearchFormatHelper;
use common\helpers\SearchRecord;
use yii\helpers\Url;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "option_category".
 *
 * @property integer $id
 * @property string $name
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Option[] $options
 */
class OptionCategory extends SearchRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'option_category';
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function adminEditTypes() {
        return [
            'name' => SearchEditHelper::TYPE_TEXT_FIELD,
        ];
    }

    public static function gridColumns() {
        return ['id', 'name', SearchFormatHelper::dateFormat('created_at')];
    }

    public function getUrl() {
        return Url::to(['profile/invest']);
    }


    public static function adminNewObj() {
        return new static();
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptions()
    {
        return $this->hasMany(Option::className(), ['category_id' => 'id']);
    }
}
