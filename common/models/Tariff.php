<?php

namespace common\models;

use common\helpers\SearchEditHelper;
use common\helpers\SearchFormatHelper;
use common\helpers\SearchRecord;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "tariff".
 *
 * @property integer $id
 * @property string $name
 * @property string $desc
 * @property double $percent
 * @property double start_price
 * @property double end_price
 * @property integer frequency
 * @property integer $time
 * @property integer $limit
 * @property string $pic
 * @property integer $times
 * @property integer $back_dep
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property TariffToUser[] $tariffToUsers
 */
class Tariff extends SearchRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tariff';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['percent', 'time'], 'required'],
            [['percent', 'limit', 'frequency'], 'number'],
            [['time', 'times', 'back_dep'], 'integer'],
            [['name', 'desc'], 'string', 'max' => 255],
            [['start_price', 'end_price', 'pic'], 'safe']
        ];
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'pic' => 'Изображение',
            'desc' => 'Описание',
            'percent' => 'Процент',
            'limit' => 'Лимит кол-ва',
            'start_price' => 'Начальная цена',
            'end_price' => 'Конечная цена',
            'frequency' => 'Частота начисления (в часах)',
            'time' => 'Дней',
            'times' => 'Раз купили',
            'back_dep' => 'Возвращение',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }


    public function adminEditTypes() {
        return [
            'name' => SearchEditHelper::TYPE_TEXT_FIELD,
            'pic' => SearchEditHelper::TYPE_PIC_BOX,
            'limit' => SearchEditHelper::TYPE_TEXT_FIELD,
            'desc' => SearchEditHelper::TYPE_TEXT_FIELD,
            'percent' => SearchEditHelper::TYPE_TEXT_FIELD,
            'start_price' =>  SearchEditHelper::TYPE_TEXT_FIELD,
            'end_price' =>  SearchEditHelper::TYPE_TEXT_FIELD,
            'frequency' =>  SearchEditHelper::TYPE_TEXT_FIELD,
            'time' =>  SearchEditHelper::TYPE_TEXT_FIELD,
            'back_dep' =>  SearchEditHelper::TYPE_CHECK_BOX,
        ];
    }

    public static function gridColumns() {
        return ['id',
            'name',
            'frequency',
            'desc',
            'percent',
            'time',
            'times',
            'back_dep:boolean',
            SearchFormatHelper::moneyFormat('start_price'),
            SearchFormatHelper::moneyFormat('end_price'),
            SearchFormatHelper::dateFormat('created_at')];
    }

    public function getUrl() {
        return Url::to(['profile/invest']);
    }


    public static function adminNewObj() {
        $nw = new Tariff();
        $nw->pic = '/img/user.png';
        $nw->times = 0;
        return $nw;
    }




    protected static $mem = [];

    /**
     * @param $id
     * @return static
     */
    public static function getIdentity($id) {
        if (!array_key_exists($id, static::$mem)) {
            static::$mem[$id] = static::findOne(['id'=>$id]);
        }

        return static::$mem[$id];
    }

}
