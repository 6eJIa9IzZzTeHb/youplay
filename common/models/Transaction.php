<?php

namespace common\models;

use common\helpers\HtmlSettingsHelper;
use common\models\payment\AdvCash;
use common\models\payment\Payeer;
use common\models\payment\PerfectMoney;
use common\models\transactions\FinanceInvestMoneyTransaction;
use common\models\transactions\FinanceWithdrawalMoneyTransaction;
use common\models\transactions\InvestTransaction;
use common\models\transactions\ReferralFromInvestTransaction;
use common\models\transactions\TariffProfitTransaction;
use frontend\models\profile\FinanceWithdrawalForm;
use Prophecy\Exception\Doubler\ClassCreatorException;
use Symfony\Component\Console\Exception\CommandNotFoundException;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * This is the model class for table "transaction".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $status
 * @property integer $type
 * @property string $data
 * @property string $amount
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 */
abstract class Transaction extends ActiveRecord
{
    const TYPE_INVEST = 1;
    const TYPE_STRUCTURE_INVEST_PROFIT = 2;
    const TYPE_PROFIT_TARIFF = 3;

    const TYPE_WITHDRAWAL_PAYEER = 11;
    const TYPE_WITHDRAWAL_PERFECT = 12;
    const TYPE_WITHDRAWAL_ADV = 13;

    const TYPE_INVEST_PAYEER = 21;
    const TYPE_INVEST_PERFECT = 22;
    const TYPE_INVEST_ADV = 23;

    const STATUS_OK = 1;
    const STATUS_ERROR = 2;
    const STATUS_IN_PROCESS = 3;
    const STATUS_CANCELED = 4;


    const ERR_BAD_AMOUNT = 1;
    const ERR_BAD_HASH = 2;
    const ERR_WRONG_DATA = 4;
    const ERR_BAD_STATUS = 8;
    const ERR_CANT_OPEN_STREAM = 16;
    const ERR_UNKNOWN = 32;


    /**
     * @param int $to
     * @param $amount
     * @param $params
     */
    public function __construct($to = null, $amount = null, $params = [])
    {

        if (is_array($to) || is_null($to)) {
            parent::__construct($to);
            return;
        }

        parent::__construct($params);

        $this->type = static::typeId();
        $this->user_id = $to;
        $this->amount = $amount;
        $this->status = self::STATUS_IN_PROCESS;

        $this->data = json_encode(["3" => 2]);
    }

    /**
     * @return int
     */
    public static function typeId()
    {
        throw new \BadMethodCallException('You cannot use base class for this');
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transaction';
    }

    /**
     * @return mixed
     */
    public static function getTotalInvested()
    {
        return InvestTransaction::getAllValue();
    }

    /**
     * @return float
     */
    public static function getAllValue()
    {
        return static::activeQuery()->andWhere(['status'=>Transaction::STATUS_OK])->sum('amount');
    }

    protected static function activeQuery()
    {
        return static::find();
    }

    /**
     * @return mixed
     */
    public static function getTotalWithdrawal()
    {
        return static::find()
            ->where(['in', 'type', static::getWithdrawalTypes()])
            ->andWhere(['status'=>Transaction::STATUS_OK])
            ->sum('amount');
    }

    public static function getWithdrawalTypes()
    {
        return [
            static::TYPE_WITHDRAWAL_ADV,
            static::TYPE_WITHDRAWAL_PAYEER,
            static::TYPE_WITHDRAWAL_PERFECT
        ];
    }

    public static function getPaymentClassName($type)
    {
        return self::getPaySystems()[$type]['payment_class'];
    }

    public static function getPaySystems()
    {
        return [

            'payeer' => [
                'name' => 'Payeer',
                'pic' => '/img/landing-info-block-4-pay.png',
                'payment_class' => Payeer::className(),
                'color' => 'blue'
            ],

            'perfect_money' => [
                'name' => 'Perfect Money',
                'pic' => '/img/landing-info-block-4-pf.png',
                'payment_class' => PerfectMoney::className(),
                'color' => 'red'
            ],

            'adv' => [
                'name' => 'advcash',
                'pic' => '/img/landing-info-block-4-adv.png',
                'payment_class' => AdvCash::className(),
                'color' => 'green',
            ],

//            'bc' => ['name' => 'Bitcoin', 'pic' => '/img/landing-info-block-4-bc.png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            HtmlSettingsHelper::decimalRule('amount'),
            [['user_id'], 'required'],
            [['user_id', 'status', 'type', 'created_at', 'updated_at'], 'integer'],
            [['data'], 'string'],
            [['amount'], 'number'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }


    public static function getStatusNames() {
        return [
            self::STATUS_OK=>'Успешно',
            self::STATUS_ERROR=>'Ошибка',
            self::STATUS_IN_PROCESS=>'Обрабатывается',
            self::STATUS_CANCELED=>'Отменено'
        ];
    }

    public static function getTypesNames() {
        return [
            self::TYPE_INVEST=>'Ввод денег',
            self::TYPE_STRUCTURE_INVEST_PROFIT=>'Прибыль из структуры',
            self::TYPE_PROFIT_TARIFF=>'Прибыль из тарифов',
            self::TYPE_INVEST_ADV=>'Инвестирование ADV',
            self::TYPE_INVEST_PERFECT=>'Инвестирование Perfect Money',
            self::TYPE_INVEST_PAYEER=>'Инвестирование Payeer',
            self::TYPE_WITHDRAWAL_ADV=>'Вывод через ADV',
            self::TYPE_WITHDRAWAL_PERFECT=>'Вывод через Perfect Money',
            self::TYPE_WITHDRAWAL_PAYEER=>'Вывод через Payeer',
        ];
    }

    public static function getStatusName($name) {
        return static::getStatusNames()[$name];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'status' => 'Status',
            'type' => 'Type',
            'data' => 'Data',
            'amount' => 'Amount',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return array
     */
    public function getDataAsArray()
    {
        return json_decode($this->data, true);
    }

    /**
     * @return bool
     */
    public function executeTransaction()
    {

        return true;
    }

    /**
     * @return array
     */
    public function idToClass()
    {
        return [
            static::TYPE_INVEST => InvestTransaction::className(),
//            static::TYPE_WITHDRAWAL => InvestTransaction::className(),

        ];
    }

    public static function getClassToType($type) {

        switch ($type) {
            case self::TYPE_INVEST:
                return InvestTransaction::className();
            case self::TYPE_PROFIT_TARIFF:
                return TariffProfitTransaction::className();
            case self::TYPE_STRUCTURE_INVEST_PROFIT:
                return ReferralFromInvestTransaction::className();
            case self::TYPE_WITHDRAWAL_PAYEER:
            case self::TYPE_WITHDRAWAL_PERFECT:
            case self::TYPE_WITHDRAWAL_ADV:
                return FinanceWithdrawalMoneyTransaction::className();
            case self::TYPE_INVEST_PAYEER:
            case self::TYPE_INVEST_PERFECT:
            case self::TYPE_INVEST_ADV:
                return FinanceInvestMoneyTransaction::className();

        }

        throw new \BadMethodCallException('Class not found');

    }

    abstract public function description();


    public static function getPaySysClassFromSlag($slag) {
        return self::getPaySystems()[$slag]['payment_class'];
    }


    public static function getSlagFromType($type) {

       return array_keys(self::getPaySystems())[($type%10) - 1];
    }

    public static function getPaySysFromType($type) {




        return self::getPaySystems()[self::getSlagFromType($type)]['payment_class'];
    }

    public static function query() {
        return static::activeQuery();
    }

    /**
     * @param $id
     * @return Transaction
     */
    public static function getInstance($id) {
        $query = (new Query())
            ->where(['id'=>$id])
            ->from(Transaction::tableName())
            ->one();

        $className = static::getClassToType($query['type']);
        return $className::findOne(['id'=>$id]);

    }

}
