<?php

namespace common\models;

use common\helpers\SearchEditHelper;
use common\helpers\SearchFormatHelper;
use common\helpers\SearchRecord;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "article".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $title
 * @property string $pic
 * @property string $short
 * @property string $data
 * @property integer $views
 * @property integer $likes
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ArticleCategory $category
 */
class Article extends SearchRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article';
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'pic', 'short'], 'required'],
            [['category_id', 'views', 'likes'], 'integer'],
            [['data'], 'string'],
            [['title', 'pic', ], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ArticleCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Категория',
            'title' => 'Title',
            'pic' => 'Pic',
            'short' => 'Краткое содержание',
            'data' => 'Полное содержание страницы',
            'views' => 'Просмотров',
            'likes' => 'Лайки',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function adminEditTypes() {
        return [
            'category_id' => ['type'=>SearchEditHelper::TYPE_FOREIGN, 'values'=>ArticleCategory::find()->all()],
            'title' => SearchEditHelper::TYPE_TEXT_FIELD,
            'pic' => SearchEditHelper::TYPE_PIC_BOX,
            'short' => SearchEditHelper::TYPE_WSYG_EDITOR,
            'data' => SearchEditHelper::TYPE_WSYG_EDITOR,
        ];
    }



    public static function adminNewObj() {
        $nw = new static();
        $nw->pic = '/img/user.png';
        $nw->views = 0;
        $nw->likes = 0;
        return $nw;
    }


    public static function gridColumns() {
        return [ SearchFormatHelper::foreignFormat('category_id', ArticleCategory::className()), 'title', 'short', 'views', SearchFormatHelper::dateFormat('updated_at')];
    }

    public function getUrl() {
        return Url::to(['site/article', 'id'=>$this->id]);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ArticleCategory::className(), ['id' => 'category_id']);
    }
}
