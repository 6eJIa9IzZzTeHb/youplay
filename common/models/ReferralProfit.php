<?php

namespace common\models;

use common\helpers\SearchEditHelper;
use common\helpers\SearchRecord;
use yii\helpers\Url;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "referral_profit".
 *
 * @property integer $id
 * @property integer $lvl
 * @property double $percent
 * @property integer $type
 * @property integer $created_at
 * @property integer $updated_at
 */
class ReferralProfit extends SearchRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'referral_profit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lvl', 'type', 'lvl', 'percent'], 'required'],
            [['lvl', 'type'], 'integer'],
            [['percent'], 'number'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lvl' => 'Lvl',
            'percent' => 'Процент(0.01 = 1%)',
            'type' => 'Для VIP',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }


    public function adminEditTypes() {
        return [
            'lvl' => ['type'=>SearchEditHelper::TYPE_TEXT_FIELD],
            'percent' => SearchEditHelper::TYPE_TEXT_FIELD,
            'type' => SearchEditHelper::TYPE_CHECK_BOX,
        ];
    }

    public static function gridColumns() {
        return ['lvl', 'percent', 'type:boolean'];
    }

    public function getUrl() {
        return Url::to(['site/marketing']);
    }
}
