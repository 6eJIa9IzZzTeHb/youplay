<?php

namespace common\models\payment;


use common\models\Transaction;
use common\models\User;
use Yii;
use yii\helpers\Url;

class PerfectMoney extends IPaySystem {
    public static $PAY_SYSTEM = 'perfect_money';
    public static $Payer_Account="U15657867";
    private static $ALTERNATE_PHRASE = "76547TSXqTmDaMEQWtASyxrRX";
    private static $AccountID="7896816";
    private static $my_password="The.First69Alecom";

    public static function additionlPaymentCheckingUsingAPI(){

        $f=fopen('https://perfectmoney.is/acct/historycsv.asp?AccountID='.self::$AccountID.'&PassPhrase='.self::$ALTERNATE_PHRASE.'&startmonth='.date("m", $_POST['TIMESTAMPGMT']).'&startday='.date("d", $_POST['TIMESTAMPGMT']).'&startyear='.date("Y", $_POST['TIMESTAMPGMT']).'&endmonth='.date("m", $_POST['TIMESTAMPGMT']).'&endday='.date("d", $_POST['TIMESTAMPGMT']).'&endyear='.date("Y", $_POST['TIMESTAMPGMT']).'&paymentsreceived=1&batchfilter='.$_POST['PAYMENT_BATCH_NUM'], 'rb');
        if($f===false) return false;;

        $lines=array();
        while(!feof($f)) array_push($lines, trim(fgets($f)));

        fclose($f);

        if($lines[0]!='Time,Type,Batch,Currency,Amount,Fee,Payer Account,Payee Account,Payment ID,Memo'){
            return $lines[0];
        }else {

            $n = count($lines);
            if ($n != 2) return false;

            $item = explode(",", $lines[1], 10);
            if (count($item) != 10) return false;
            $item_named['Time'] = $item[0];
            $item_named['Type'] = $item[1];
            $item_named['Batch'] = $item[2];
            $item_named['Currency'] = $item[3];
            $item_named['Amount'] = $item[4];
            $item_named['Fee'] = $item[5];
            $item_named['Payer Account'] = $item[6];
            $item_named['Payee Account'] = $item[7];
            $item_named['Payment ID'] = $item[8];
            $item_named['Memo'] = $item[9];

            if ($item_named['Batch'] == $_POST['PAYMENT_BATCH_NUM'] && $_POST['PAYMENT_ID'] == $item_named['Payment ID'] && $item_named['Type'] == 'Income' && $_POST['PAYEE_ACCOUNT'] == $item_named['Payee Account'] && $_POST['PAYMENT_AMOUNT'] == $item_named['Amount'] && $_POST['PAYMENT_UNITS'] == $item_named['Currency'] && $_POST['PAYER_ACCOUNT'] == $item_named['Payer Account']) {
                return true;
            } else {
                return false;
            }
        }
    }

    public static function checkErrors($amount) {
        $err_no = 0;
        $string=
            $_POST['PAYMENT_ID'].':'.$_POST['PAYEE_ACCOUNT'].':'.
            $_POST['PAYMENT_AMOUNT'].':'.$_POST['PAYMENT_UNITS'].':'.
            $_POST['PAYMENT_BATCH_NUM'].':'.
            $_POST['PAYER_ACCOUNT'].':'.strtoupper(md5(static::$ALTERNATE_PHRASE)).':'.
            $_POST['TIMESTAMPGMT'];


        if (!in_array($_SERVER['REMOTE_ADDR'], array('77.109.141.170', '91.205.41.208', '94.242.216.60', '78.41.203.75'))) return Transaction::STATUS_ERROR;


//        if (!self::additionlPaymentCheckingUsingAPI()) {
//            $err_no = Transaction::$ERR_BAD_HASH;
//        }


        if ($amount != $_POST['PAYMENT_AMOUNT']) {
            $err_no |= Transaction::ERR_BAD_AMOUNT;
        }

        $hash=strtoupper(md5($string));
        if ($hash != $_POST['V2_HASH'])
        {
            $err_no |= Transaction::ERR_BAD_HASH;
        }

        if(!($_POST['PAYEE_ACCOUNT']==static::$Payer_Account && $_POST['PAYMENT_UNITS']=='USD'))
        {
            $err_no = Transaction::ERR_WRONG_DATA;
        }

        return $err_no;
    }


    public static function complete() {

        /**
         * @var $transaction Transaction
         */
        $transaction_id = intval($_POST['PAYMENT_ID']);
        $transaction = Transaction::getInstance($transaction_id);
        $err_no = static::checkErrors($transaction->amount);
        $status = Transaction::STATUS_ERROR;
        if (!$err_no) {
            $status = Transaction::STATUS_OK;
        }
        //$transaction_id = 1; $err_no = 0; $status = Transaction::$ACCEPTED;
        return ['tid'=>$transaction_id, 'status'=>$status, 'err_no'=>$err_no];
    }


    public static function withdrawal($id, $pay_system, $account) {


        $err_no = 0;
        $PAY_IN=1;
        $new_amount = Transaction::getInstance($id)->amount * 0.98;
        $f=fopen('https://perfectmoney.is/acct/confirm.asp?AccountID='.static::$AccountID.'&PassPhrase='.
            static::$my_password.'&Payer_Account='.static::$Payer_Account.
            '&Payee_Account='.$account.'&Amount='.(floor($new_amount*100)/100).'&PAY_IN='.$PAY_IN.'&PAYMENT_ID='.$id, 'rb');
        if($f===false)
        {
            $err_no = Transaction::ERR_CANT_OPEN_STREAM;
            return $err_no;
        }

        $out="";
        while(!feof($f)) $out.=fgets($f);
        fclose($f);

        // searching for hidden fields
        $res = preg_match_all("/<input name='(.*)' type='hidden' value='(.*)'>/", $out, $result, PREG_SET_ORDER);
        $status = $result[0][1];
//        var_dump($result); die();
        self::$serverAnswer['result'] = $result;
        if(!$res || $status == "ERROR") {
            $err_no = Transaction::ERR_BAD_STATUS;
        }

        return $err_no;
    }

    public static function getHash($t_id, $amount) {
        return password_hash(($t_id+intval($amount*100)*345)%1000000009, PASSWORD_BCRYPT);
    }

    public static function getCallBack($t_id, $amount) {
        return Url::to(['payment/callback1448', 'className'=>static::className(), 'hash'=>self::getHash($t_id, $amount)], true);
    }


    public static function getParams($amount, $payType, $transaction_id) {
        /**
         * @var $user User
         */
        $user = Yii::$app->user->identity;
        $desc = "Пополнение счёта Youplay. Имя:". $user->username;
        return [
            'method'=>'POST',
            'action'=> 'https://perfectmoney.is/api/step1.asp',
            'fields'=>[
                'PAYEE_ACCOUNT'=>static::$Payer_Account,
                'PAYEE_NAME'=>$desc,
                'PAYMENT_ID'=>$transaction_id,
                'PAYMENT_AMOUNT'=>$amount,
                'PAYMENT_UNITS'=>"USD",
                'STATUS_URL'=>static::getCallBack($transaction_id, $amount),
                'PAYMENT_URL'=>static::getPaymentUrl(),
                'PAYMENT_URL_METHOD'=>"POST",
                'NOPAYMENT_URL'=>static::getNoPaymentUrl($transaction_id),
                'NOPAYMENT_URL_METHOD'=>"POST",
                'SUGGESTED_MEMO'=>"",
                'BAGGAGE_FIELDS'=>"",
                'PAYMENT_METHOD'=>"Оплатить",
            ]
        ];
    }
};