<?php

namespace common\models\payment;
use common\models\Transaction;
use common\models\transactions\FinanceInvestMoneyTransaction;
use kartik\base\Widget;
use yii\base\Object;
use yii\helpers\Url;
use yii\web\User;

/**
 * Interface IPaySystem
 */
abstract class IPaySystem extends Object {
    public static $PAY_SYSTEM = "";
    public static $serverAnswer = [];
    /**
     * @param $amount float
     * @param $payType integer
     * @param $transaction_id integer
     * @return [] ['fields'-array_of_form_fields [['name'=>'amount', 'value'=>'35'], ...], 'action'-string]
     */
    abstract public static function getParams($amount, $payType, $transaction_id);

    /**
     * @param $transactiond_id
     * @param $pay_system
     * @param $account
     * @return mixed
     */
    abstract public static function withdrawal($transactiond_id, $pay_system, $account);

    /**
     * @return boolean|array
     */
    abstract public static function complete();

    /**
     * @return boolean
     */
    public static function parseCallback() {
        if ($result = static::complete()) {
            $transaction = Transaction::getInstance($result['tid']);
            $transaction->execute($result['status'], $result);
            $transaction->data = json_encode(array_merge($transaction->getDataAsArray(), $_POST));
            $transaction->save();

            return true;
        }
        return false;
    }

    public static function getCallBack($t_id, $amount) {
        return Url::to(['payment/callback1448', 'className'=>static::className()], true);
    }

    public static function getPaymentUrl() {
        return Url::to(['payment/payment-success'], true);
    }

    public static function getNoPaymentUrl($id) {
        return Url::to(['payment/payment-decline', 't_id'=>$id], true);
    }


    public function createTransaction($amount, $payType, $options = []) {
        /**
         * @var $user User
         */
        $user = \Yii::$app->user->identity;

        if ($payType>=20) {
            $transaction = new FinanceInvestMoneyTransaction($user->id, $amount, static::$PAY_SYSTEM);
        } else {
            $transaction = new FinanceInvestMoneyTransaction();
        }
        $transaction->save();
        return $transaction->id;
    }

    /**
     * @param $transaction_id
     * @returns boolean
     */
    public static function makeWithdrawal( $transaction_id ) {
        /**
         * @var $op Transaction
         */
        $op = Transaction::getInstance($transaction_id);
        $err_no = static::withdrawal($transaction_id, $op->getDataAsArray()['pay_system'], $op->getDataAsArray()['account']);

        $status = $err_no ? Transaction::STATUS_ERROR : Transaction::STATUS_OK;

        self::$serverAnswer['err_no'] = $err_no;
        $op->execute($status, self::$serverAnswer);

        return !$err_no;
    }

    /**
     * @param $amount float
     * @param $payType integer
     * @param $options []
     * @return $this
     */
    public static function sendQuery($amount, $payType, $options=[]) {

        $transaction_id = static::createTransaction($amount, $payType, $options);
        return static::getParams($amount, $payType, $transaction_id);
    }
};
