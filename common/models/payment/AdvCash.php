<?php

namespace common\models\payment;


use common\models\Transaction;
use common\models\User;
use Yii;
use yii\helpers\Url;
use common\models\payment\MerchantWebService;

class AdvCash extends IPaySystem {
    public static $PAY_SYSTEM = 'adv';
    public static $EMAIL = 'youplay.company.Real@gmail.com';
    public static $SHOP_NAME = 'You Play Online SCI';
    public static $API_NAME = 'You Play Online API';
    public static $SECRET = 'R25E234ZZZZAVASTHERELOL34';
    public static $API_PASS = "R25E234ZZZZAVASTHERELOL34";

    public static function checkErrors($amount) {
        $err_no = 0;

        $string=
            $_POST['ac_transfer'].':'.$_POST['ac_start_date'].':'.
            $_POST['ac_sci_name'].':'.$_POST['ac_src_wallet'].':'.
            $_POST['ac_dest_wallet'].':'.$_POST['ac_order_id'].':'.
            $_POST['ac_amount'].':'.$_POST['ac_merchant_currency'].':'.
            self::$SECRET;


        if ($amount != $_POST['ac_amount']) {
            $err_no |= Transaction::ERR_BAD_AMOUNT;
        }

        $hash=hash('sha256',$string);

        if ($hash != $_POST['ac_hash'])
        {
            $err_no |= Transaction::ERR_BAD_HASH;
        }


        return $err_no;
    }


    public static function complete() {

        /**
         * @var $transaction Transaction
         */
        $transaction_id = intval($_POST['ac_order_id']);
        $transaction = Transaction::getInstance($transaction_id);
        $err_no = static::checkErrors($transaction->amount);
        $status = Transaction::STATUS_ERROR;
        if (!$err_no) {
            $status = Transaction::STATUS_OK;
        }
        //$transaction_id = 1; $err_no = 0; $status = Transaction::$ACCEPTED;
        return ['tid'=>$transaction_id, 'status'=>$status, 'err_no'=>$err_no];
    }


    public static function withdrawal($id, $pay_system, $account) {

        $transaction = Transaction::getInstance($id);

        $err_no = 0;

        $merchantWebService = new MerchantWebService();

        $arg0 = new authDTO();
        $arg0->apiName = self::$API_NAME;
        $arg0->accountEmail = self::$EMAIL;
        $arg0->authenticationToken = $merchantWebService->getAuthenticationToken(self::$API_PASS);

        $arg1 = new sendMoneyRequest();
        $arg1->amount = $transaction->amount;
        $arg1->currency = "USD";
        $arg1->email = $account;
        $arg1->note = "You Play withdrawal";
        $arg1->savePaymentTemplate = false;

        $validationSendMoneyToAdvcashCard = new validationSendMoney();
        $validationSendMoneyToAdvcashCard->arg0 = $arg0;
        $validationSendMoneyToAdvcashCard->arg1 = $arg1;

        $sendMoneyToAdvcashCard = new sendMoney();
        $sendMoneyToAdvcashCard->arg0 = $arg0;
        $sendMoneyToAdvcashCard->arg1 = $arg1;

        try {
            $merchantWebService->validationSendMoneyToAdvcashCard($validationSendMoneyToAdvcashCard);
            $sendMoneyToAdvcashCardResponse = $merchantWebService->sendMoneyToAdvcashCard($sendMoneyToAdvcashCard);

            self::$serverAnswer['return'] = $sendMoneyToAdvcashCardResponse->return;
        } catch (\Exception $e) {
             self::$serverAnswer['error'] = $e->getMessage();
            $err_no = Transaction::ERR_BAD_STATUS;
        }

        return $err_no;
    }


    public static function getSign($amount, $orderId) {
        $arHash = array(
            self::$EMAIL,
            self::$SHOP_NAME,
            $amount,
            'USD',
            self::$SECRET,
            $orderId,
        );
        return (hash('sha256', implode(":", $arHash)));
    }


    public static function getCallBack($t_id, $amount) {
        return Url::to(['payment/callback1448', 'className'=>static::className(), 'hash'=>self::getHash($t_id, $amount)], true);
    }


    public static function getParams($amount, $payType, $transaction_id) {
        /**
         * @var $user User
         */
        $user = Yii::$app->user->identity;
        $desc = "Пополнение счёта Youplay. Имя: ". $user->username;
        $amount = number_format($amount, 2, '.', '');
        return [
            'method'=>'POST',
            'action'=> 'https://wallet.advcash.com/sci/',
            'fields'=>[
                'ac_account_email'=>static::$EMAIL,
                'ac_sci_name'=>static::$SHOP_NAME,
                'ac_order_id'=>$transaction_id,
                'ac_amount'=>$amount,
                'ac_currency'=>"USD",
               // 'ac_sign'=>static::getSign($amount, $transaction_id),
                'ac_comments'=>$desc,
                'PAYMENT_METHOD'=>"Оплатить",
            ]
        ];
    }
};