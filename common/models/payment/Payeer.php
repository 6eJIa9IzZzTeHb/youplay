<?php

namespace common\models\payment;

use common\models\Transaction;
use common\models\User;
use common\widgets\Alert;
use Yii;

class Payeer extends IPaySystem {
    public static $PAY_SYSTEM = 'payeer';
    public static $SHOP_ID = 419177683;
    private static $SHOP_SECRET = "youplayyouplayyouplayyouplay35";
    private static $API_ID = 414343372;
    private static $API_SECRET = "youplayyouplay228";
    private static $ACC = "P74505311";

    public static function checkPayment($amount) {
        sleep(1);
        $accountNumber = self::$ACC;
        $apiId = self::$API_ID;
        $apiKey = self::$API_SECRET;
        $payeer = new CPayeer($accountNumber, $apiId, $apiKey);
        $opId = $_POST['m_operation_id'];
        try {
            if ($payeer->isAuth()) {
                /**
                 * @var User $user
                 */
                $info = $payeer->getHistoryInfo($opId);

                $info = json_decode(json_encode($info))->info;
                if ($info->status == "execute" and floatval($info->sumOut) >= floatval($amount)) {
                    return true;
                }
            }
        } catch (\Exception $ex) {

        }
        return false;
    }


    public static function checkErrors($amount) {
        $err_no = 0;
       // if (!in_array($_SERVER['REMOTE_ADDR'], array('185.71.65.92', '185.71.65.189'))) $err_no|=Transaction::ERR_BAD_IP;

        if (!isset($_POST["m_operation_id"]) or !isset($_POST["m_sign"])) {
            $err_no |= Transaction::ERR_WRONG_DATA;
        }

        if ($amount != $_POST['m_amount']) {
            $err_no |= Transaction::ERR_BAD_AMOUNT;
        }

        $sign_hash = self::getSignPost();

        if ($_POST["m_sign"] != $sign_hash) {
            $err_no |= Transaction::ERR_BAD_HASH;
        }

        if ($_POST['m_status'] != "success")
        {
            $err_no |= Transaction::ERR_BAD_STATUS;
        }

       // if (!self::checkPayment($amount)) {
       //     return Transaction::$ERR_BAD_PAYMENT;
       // }

        return $err_no;
    }

    public static function complete() {
        /**
         * @var $transaction Transaction
         */
        $transaction_id = intval($_POST['m_orderid']);

        $transaction = Transaction::getInstance($transaction_id);
        $err_no = static::checkErrors($transaction->amount);
        $status = Transaction::STATUS_ERROR;
        if (!$err_no) {
            $status = Transaction::STATUS_OK;
        }
        return ['tid'=>$transaction_id, 'status'=>$status, 'err_no'=>$err_no];
    }


    public static function withdrawal($id, $pay_system, $account) {

        $transaction = Transaction::getInstance($id);
        $accountNumber = self::$ACC;
        $apiId = self::$API_ID;
        $apiKey = self::$API_SECRET;
        $payeer = new CPayeer($accountNumber, $apiId, $apiKey);
        if ($payeer->isAuth()){
            // инициализация вывода
            $initOutput = $payeer->initOutput(array(
                // id платежной системы полученный из списка платежных систем
                'ps' => '1136053',
                // счет, с которого будет списаны средства
                'curIn' => 'USD',
                // сумма вывода
                'sumOut' => $transaction->amount,
                // валюта вывода
                'curOut' => 'USD',
                // Аккаунт получателя платежа
                'param_ACCOUNT_NUMBER' => $account
            ));

            if ($initOutput){
                // Вывод средств
                $historyId = $payeer->output();
                self::$serverAnswer['result'] = $historyId;
                if ($historyId){
                    return 0;
                }
                else{
                    return Transaction::ERR_WRONG_DATA;
                }
            }else{
                return Transaction::ERR_CANT_OPEN_STREAM;
            }
        }

        return Transaction::ERR_UNKNOWN;
    }

    public static function getSignPost() {
        $arHash = array($_POST['m_operation_id'],
            $_POST['m_operation_ps'],
            $_POST['m_operation_date'],
            $_POST['m_operation_pay_date'],
            $_POST['m_shop'],
            $_POST['m_orderid'],
            $_POST['m_amount'],
            $_POST['m_curr'],
            $_POST['m_desc'],
            $_POST['m_status'],
            self::$SHOP_SECRET);

        return strtoupper(hash('sha256', implode(":", $arHash)));
    }

    public static function getSign($amount, $orderId, $desc) {
        $arHash = array(
            self::$SHOP_ID,
            $orderId,
            $amount,
            'USD',
            base64_encode($desc),
            self::$SHOP_SECRET
        );

        return strtoupper(hash('sha256', implode(":", $arHash)));
    }

    public static function getParams($amount, $payType, $transaction_id) {
        /**
         * @var $user User
         */
        $user = Yii::$app->user->identity;
        $desc = "Пополнение счёта Youplay. Имя:". $user->username;
        $amount = number_format($amount, 2, '.', '');
        return [
            'method'=>'GET',
            'action'=> 'https://payeer.com/merchant/',
            'fields'=>[
                'm_shop'=>static::$SHOP_ID,
                'm_orderid'=>$transaction_id,
                'm_amount'=>$amount,
                'm_curr'=>"USD",
                'm_desc'=>base64_encode($desc),
                'm_sign'=>static::getSign($amount, $transaction_id, $desc),
                'm_process'=>"Оплатить",
            ]
        ];
    }
};