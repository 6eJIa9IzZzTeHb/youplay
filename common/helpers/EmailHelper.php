<?php

namespace common\helpers;

use common\models\User;
use Yii;
use yii\helpers\Url;

class EmailHelper {

    /**
     * @param $template
     * @param $to
     * @param $subject
     * @param $params
     * @return bool
     */
    public static function sendEmail($template, $to, $subject, $params) {

        return  \Yii::$app->mailer->compose($template, $params)
                ->setTo($to)
                ->setFrom(["no-replay@you-play.online" => "YouPlay"])
                ->setSubject($subject)
                ->send();
    }


}