<?php

namespace common\helpers;

use yii\db\ActiveRecord;

abstract class SearchRecord extends ActiveRecord
{

    abstract  public function getUrl();

    /**
     * @return mixed
     * For example: return ['id'=>SearchEditHelper::TYPE_TEXT_FIELD]
     */
    abstract public function adminEditTypes();

    public static function adminNewObj() {
        return new static();
    }

    public function gridUrl() {

        return $this->getUrl();
    }

    public static $columnSuffix;

    public static function gridTableName()
    {
        return static::tableName();
    }

    public static function gridAttributeLabels()
    {
        return static::attributeLabels();
    }


    public static function gridRules()
    {

        return [
            [array_keys(static::gridAttributeLabels()), 'safe']
        ];
    }


    public static function getPreparedGridColumns()
    {

        $columns = static::gridColumns();
        foreach ($columns as &$value) {
            if (is_numeric($value)) {
                $value .= static::$columnSuffix;
            }
        }

        return $columns;
    }

    public static function gridColumns()
    {
        return ['id',
            ['attribute' => 'created_at',
                'format' => ['date', 'php:d/m/Y']
            ],
            ['attribute' => 'updated_at',
                'format' => ['date', 'php:d/m/Y']
            ]];
    }

    public static function gridStrictSearch()
    {
        return ['id', 'created_at', 'updated_at'];
    }


}


?>