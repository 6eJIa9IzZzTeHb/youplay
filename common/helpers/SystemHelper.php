<?php

namespace common\helpers;


use common\models\User;
use Faker\Provider\zh_TW\DateTime;
use phpnt\cropper\ImageLoadWidget;
use Yii;
use yii\base\Model;
use yii\helpers\BaseUrl;
use yii\helpers\Url;
use yii\web\Cookie;
use DateTime as DTime;
use yii\widgets\ActiveForm;

class SystemHelper {
    public static $refer = null;


    /**
     * @return User
     */
    public static function getRefer() {
        if (self::$refer) return self::$refer;
        $cookies = Yii::$app->request->cookies;
        $name = $cookies->getValue('refer', null);
        self::$refer = User::findByUsername($name);
        if (!self::$refer) {
            self::$refer = User::findIdentity(1);
        }
        return self::$refer;
    }

    public static function getName() {
        if ($refer = self::getRefer()) {
            return $refer->username;
        }
        return "";
    }

    public static function getId() {
        if ($refer = self::getRefer()) {
            return $refer->id;
        }
        return 1;
    }


    public static function setRefer($uname) {
        $cookies = Yii::$app->response->cookies;
        $cookies->add(new Cookie([
            'name' => 'refer',
            'value' => $uname,
        ]));
        self::$refer = User::findByUsername($uname);
    }

    /**
     * @param $form ActiveForm
     * @param $model
     * @param $field
     * @param $options
     * @return string
     * @throws \Exception
     */
    public static function getImageInput($model, $field, $options=[]) {

        $options = array_merge([
            'fieldOptions' => []
        ], $options);

        $defaultPic = file_exists('@frontend/web'.$model->$field)?$model->$field:'/img/user.png';




        return "<div class='hand picture-picker'>".
        ImageLoadWidget::widget(array_merge([
            'id' => 'load-user-'.$field,                                     // суффикс ID
            'object_id' => $model->getId($field),                                      // ID объекта
            'imagesObject' => $model->getPictures($field),                                // уже загруженные изображения
            'images_num' => 1,                                              // максимальное количество изображений
            'images_label' => $model->getLabel($field),                                        // метка для изображения
            'imageSmallWidth' => 200,                                       // ширина миниатюры
            'imageSmallHeight' => 200,                                      // высота миниатюры
            'imagePath' => '/img/upload/',                             // путь, куда будут записыватся изображения относительно алиаса
            'noImage' => $defaultPic,                                                 // 1 - no-logo, 2 - no-avatar, 3 - no-img или путь к другой картинке
            'buttonClass' => 'hidden',                                 // класс кнопки "обновить аватар"/"загрузить аватар" / по умолчанию btm btn-info
            'pluginOptions' => [                                            // настройки плагина
                'aspectRatio' => 151 / 160,                                       // установите соотношение сторон рамки обрезки. По умолчанию свободное отношение.
                'strict' => false,                                          // true - рамка не может вызодить за холст, false - может
                'guides' => true,                                           // показывать пунктирные линии в рамке
                'center' => true,                                           // показывать центр в рамке изображения изображения
                'autoCrop' => true,                                         // показывать рамку обрезки при загрузке
                'autoCropArea' => 1,                                      // площидь рамки на холсте изображения при autoCrop (1 = 100% - 0 - 0%)
                'dragCrop' => true,                                         // создание новой рамки при клики в свободное место хоста (false - нельзя)
                'movable' => true,                                          // перемещать изображение холста (false - нельзя)
                'rotatable' => true,                                        // позволяет вращать изображение
                'scalable' => true,                                         // мастабирование изображения
                'zoomable' => false,
            ]], $options['fieldOptions'])).'</div>';
    }
}