<?php

namespace common\helpers;

use common\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;

/**
 * Class TemplatesSearch
 * @package backend\models
 *
 */
class SearchModel extends ActiveRecord
{
    /** @var $model SearchRecord */
    public $model = null;
    /** @var $modelClass string */
    public static $modelClass = null;


    /**
     * @param string $modelClass
     * @param ActiveRecord $model
     * @param array $config
     */
    public function __construct($modelClass = null, $model = null, $config = []) {
        if (!is_string($modelClass)) {
            return new self::$modelClass($modelClass);

        }
        $this->model =  $model;
        self::$modelClass = $modelClass;


        return parent::__construct($config);
    }



    public static function tableName() {
        return call_user_func(self::$modelClass .'::gridTableName');
    }


    public function attributeLabels() {
        return call_user_func(self::$modelClass .'::gridAttributeLabels');
    }

    public function rules() {
        return call_user_func(self::$modelClass .'::gridRules');
    }


    public function gridColumns() {
        return call_user_func(self::$modelClass .'::getPreparedGridColumns');
    }


    /**
     * @return ActiveRecord
     */
    public function getDataModel() {
        return $this->model;
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $strict =  call_user_func(self::$modelClass .'::gridStrictSearch');;

        foreach ($this->activeAttributes() as $field) {
            if (!isset($this->$field)) continue;
            $value = $this->$field;

            if (strlen($this->$field) && substr($field, -3) == '_at') {

                $parts = preg_split('/[^\d]+/', $this->$field);
                $day = date('d');
                $month = date('m');
                $year = date('Y');
                $step = 0;
                foreach ($parts as $part) {
                    if ($len = strlen($part)) {
                        if ($len>2) {
                            $year = $part;
                        } else {
                            if ($step == 0) $day = $part;
                            if ($step == 1) $month = $part;
                            if ($step == 2) $year= $part;

                            $step++;
                        }
                    }
                }
                $value = strtotime($year.'-'.$month.'-'.$day);
                $query->andFilterWhere(['>=', 'created_at', $value]);
                $query->andFilterWhere(['<', 'created_at', $value+24*60*60]);
            } else if (!in_array($field, $strict)) {
                $query->andFilterWhere(['like', $field, $value]);
            } else {
                $query->andFilterWhere([$field => $value]);
            }
        }


        return $dataProvider;
    }
}
