<?php

namespace common\helpers;

class IpHelper {

    // Function to get the client IP address
    public static function getIp() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = null;
        return $ipaddress;
    }


    public static function getIpAsInt($ip=null)
    {
        $result = 0;
        if (!$ip) {
            $ip = self::getIp();
        }


        if (!$ip) return 0;

        $id = 0;
        foreach (explode('.', $ip) as $part) {
            $result = $result + intval($part) * (1 << (8 * $id));
            $id++;
        }

        return $result;
    }


    public static function getIpFromInt($int) {
        $result = "";

        for ($i = 0; $i<4; $i++) {
            if ($i) $result .= '.';
            $result .= ($int & ((1<<8)-1));
            $int >>= 8;
        }

        return $result;
    }
}