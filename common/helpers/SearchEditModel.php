<?php

namespace common\helpers;

use common\models\User;
use phpnt\cropper\models\Photo;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;

/**
 * Class TemplatesSearch
 * @package backend\models
 *
 */
class SearchEditModel extends ActiveRecord
{

    /** @var $modelClass string */
    public static $modelClass = null;
    /** @var $model SearchRecord */
    public $model = null;


    public $dateFormat = "d/m/Y";
    public $datePickerFormat = "dd/mm/yyyy";

    /**
     * @param string $modelClass
     * @param SearchRecord $model
     * @param array $config
     */
    public function __construct($modelClass, $model, $config = [])
    {

        $this->model = $model;
        self::$modelClass = $modelClass;


        return parent::__construct($config);
    }

    public static function tableName()
    {
        return call_user_func(self::$modelClass . '::gridTableName');
    }

    /**
     *
     */
    public function loadAttributes()
    {
        $this->setAttributes($this->model->getAttributes());
     //   var_dump($this->model);;
    }

    public function attributeLabels()
    {
        return $this->model->attributeLabels();
    }

    public function rules()
    {
        return $this->model->rules();
    }

    /**
     * @param array $data
     * @param string $formName
     * @returns bool
     */
    public function load($data, $formName = NULL)
    {
        if (empty($data[$this->formName()])) return false;
        return parent::load($data, $formName);
    }

    public function getPictureLabel($type) {
        $id = $this->model->id?$this->model->id:'0';
        return static::tableName().':'.$id.':'.$type;
    }

    /**
     * @param string $type
     * @return Photo[]
     */
    public function getTypePhotos($type)
    {
        return Photo::find()->where([
            'object_id' => $this->model->id,
            'deleted' => 0,
            'type' => $this->getPictureLabel($type)
        ])->all();
    }

    /**
     * @param null $attributeNames
     * @param bool $clearErrors
     * @returns bool
     */
    public function validate($attributeNames = null, $clearErrors = true)
    {

        foreach ($this->model->adminEditTypes() as $field=>$type) {
            if (is_array($type)) $type = $type['type'];
            if ($type == SearchEditHelper::TYPE_PIC_BOX) {
                $photos = $this->getTypePhotos($field);
                if (sizeof($photos)) {
                    $this->$field = $photos[0]->file;
                } else {
                    $this->$field = '/img/user.png';
                }
            }
            if ($type == SearchEditHelper::TYPE_DATE) {
                $day = $month = $year = "";
                for ($i = 0; $i<strlen($this->datePickerFormat); $i++) {
                    $val = $this->$field;
                    $c = $val[$i];
                    switch ($this->datePickerFormat[$i]) {
                        case 'd': $day .=  $c; break;
                        case 'm': $month .=  $c; break;
                        case 'y': $year .=  $c; break;
                        default: break;
                    }
                }
                $this->$field = strtotime($year."-".$month."-".$day);
            }
        }

        $this->model->setAttributes($this->getAttributes());
        $result = $this->model->validate();
        $this->addErrors($this->model->errors);
        //var_dump($this->model->getErrors());
        return $result;

    }



    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     */
    public function save($runValidation = true, $attributeNames = null)
    {

        $result = $this->model->save();
        $this->id = $this->model->id;

        return $result;
    }

}
