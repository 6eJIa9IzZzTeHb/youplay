<?php

namespace common\helpers;

use common\models\Transaction;
use common\models\transactions\FinanceWithdrawalMoneyTransaction;
use common\models\User;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use kartik\select2\Select2;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class SearchFormatHelper {

    /**
     * @param $column
     * @return array
     */
    public static function dateFormat($column) {
        return [
            'attribute'=>$column,
            'content'=>function($model, $key, $index, $column){
                $attr=$column->attribute;
                return "<span class='fa fa-calendar'> ". date('d-m-Y', $model->$attr).'</span>';
            }
        ];
    }

    /**
     * @param $column
     * @return array
     */
    public static function moneyFormat($column) {
        return [
            'attribute'=>$column,
            'content'=>function($model, $key, $index, $column){
                $attr=$column->attribute;
                return "<span class='fa fa-usd'> ".$model->$attr.'</span>';
            }
        ];
    }

    /**
     * @param $column
     * @return array
     */
    public static function transactionDescription($column) {
        return [
            'attribute'=>$column,
            'content'=>function($model, $key, $index, $column){
                $t=Transaction::getInstance($model->id);
                return $t->description();
            }
        ];
    }

    /**
     * @param $column
     * @return array
     */
    public static function transactionPaySystem($column) {
        return [
            'attribute'=>$column,
            'content'=>function($model, $key, $index, $column){
                /**
                 * @var $model FinanceWithdrawalMoneyTransaction
                 */
                return $model->getDataAsArray()['pay_system'];
            }
        ];
    }

    /**
     * @param $column
     * @param string $targetField
     * @return array
     */
    public static function foreignFormat($column, $modelName, $targetField="name") {
        $list = ArrayHelper::map($modelName::find()->asArray()->all(), 'id', $targetField);
        Yii::$app->view->params["{$column}_data"] = [$modelName, $targetField];

        return [
            'attribute'=>$column,
            'filter'=>$list,
            'value'=>function($model, $key, $index, $column){
                $field = $column->attribute;
                $data =  Yii::$app->view->params["{$field}_data"];
                $modelName = $data[0];
                $foreignModel = $modelName::findOne(['id'=>$model->$field]);
                $fieldName = $data[1];
                return $foreignModel->$fieldName;
            }
        ];
    }

    /**
     * @param $column
     * @param [] $list
     * @return array
     */
    public static function pickFormat($column, $list) {
        Yii::$app->view->params["{$column}_data"] = $list;

        return [
            'attribute'=>$column,
            'filter'=>$list,
            'value'=>function($model, $key, $index, $column){
                $attr=$column->attribute;
                return Yii::$app->view->params["{$attr}_data"][$model->$attr];
            }
        ];
    }
    /**
     * @param $column
     * @param [] $list
     * @param $isId
     * @return array
     */
    public static function select2($column, $list, $isId = false) {
        Yii::$app->view->params["{$column}_data"] = $list;
        return [
            'attribute'=>$column,
            'filter'=>$list,
            'width'=>'150px',
            'filterType'=>GridView::FILTER_SELECT2,
            'filterWidgetOptions'=> [
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ],
            'filterInputOptions'=>['placeholder'=>''],
            'value'=>function($model, $key, $index, $column){
                $attr=$column->attribute;
                return Yii::$app->view->params["{$attr}_data"][$model->$attr];
            }
        ];
    }

}