<?php

namespace common\helpers;

use frontend\helpers\HtmlThemeHelper;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\db\ActiveRecord;
use common\models\User;
use phpnt\cropper\ImageLoadWidget;
use PHPUnit\Framework\Exception;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\redactor\widgets\Redactor;

class SearchEditHelper
{
    const TYPE_TEXT_FIELD = 1;
    const TYPE_TEXT_AREA = 2;
    const TYPE_FOREIGN = 3;
    const TYPE_DATE = 4;
    const TYPE_CHECK_BOX = 5;
    const TYPE_PIC_BOX = 6;
    const TYPE_WSYG_EDITOR = 7;
    const TYPE_SELECT = 8;
    const TYPE_SELECT2 = 9;

    public static function typeLabels() {
        return  [
          self::TYPE_TEXT_FIELD=>'Текстовое Поле',
          self::TYPE_TEXT_AREA=>'Текст',
          self::TYPE_DATE=>'Дата',
          self::TYPE_CHECK_BOX=>'Да/Нет',
          self::TYPE_PIC_BOX=>'Картинка',
          self::TYPE_WSYG_EDITOR=>'Текстовый редактор',

//            self::TYPE_FOREIGN=>'SELECT',
//            self::TYPE_SELECT=>'SELECT',
//            self::TYPE_SELECT2=>'SELECT',
        ];
    }


    /**
     * @param $form
     * @param $model
     * @param $field
     * @param $type
     * @param $options
     * @returns string
     */
    public static function drawType($form, $model, $field, $type, $options = [])
    {

        if (is_array($type)) {
            $options = array_merge($options, $type);
            $type = $type['type'];
        }
        switch ($type) {
            case self::TYPE_TEXT_FIELD:
                return static::drawInput($form, $model, $field, $options);
                break;
            case self::TYPE_PIC_BOX:
                return static::drawImgInput($form, $model, $field, $options);
                break;
            case self::TYPE_TEXT_AREA:
                return static::drawTextArea($form, $model, $field, $options);
                break;
            case self::TYPE_FOREIGN:
                return static::drawForeignInput($form, $model, $field, $options);
                break;
            case self::TYPE_DATE:
                return static::drawDateInput($form, $model, $field, $options);
                break;
            case self::TYPE_CHECK_BOX:
                return static::drawCheckBoxInput($form, $model, $field, $options);
                break;
            case self::TYPE_WSYG_EDITOR:
                return static::drawWsygInput($form, $model, $field, $options);
                break;
            case self::TYPE_SELECT:
                return static::drawSelectInput($form, $model, $field, $options);
                break;
            case self::TYPE_SELECT2:
                return static::drawSelect2Input($form, $model, $field, $options);
                break;
        }

        throw new Exception($type . " - тип поля не поддерживается");
    }


    /**
     * @param $form ActiveForm
     * @param $model
     * @param $options
     * @returns string
     */
    public static function drawInput($form, $model, $field, $options = [])
    {

        $options = array_merge([
            'fieldOptions' => []
        ], $options);

        return $form->field($model, $field, $options['fieldOptions']);
    }


    /**
     * @param $form ActiveForm
     * @param $model SearchEditModel
     * @param $field string
     * @param $options
     * @returns string
     */
    public static function drawImgInput($form, $model, $field, $options = [])
    {

        $options = array_merge([
            'fieldOptions' => []
        ], $options);

        $defaultPic = file_exists('@frontend/web'.$model->$field)?$model->$field:'/img/user.png';

        return "<label>" . $model->getAttributeLabel($field) . "</label><div class='hand'>" .
        ImageLoadWidget::widget(array_merge([
            'id' => 'load-user-avatar',                                     // суффикс ID
            'object_id' => $id = $model->model->id?$model->model->id:0,                                      // ID объекта
            'imagesObject' => $model->getTypePhotos($field),                                // уже загруженные изображения
            'images_num' => 1,                                              // максимальное количество изображений
            'images_label' => $model->getPictureLabel($field),                                        // метка для изображения
            'imageSmallWidth' => 200,                                       // ширина миниатюры
            'imageSmallHeight' => 400,                                      // высота миниатюры
            'imagePath' => '/img/upload/',                             // путь, куда будут записыватся изображения относительно алиаса
            'noImage' => $defaultPic,                                                 // 1 - no-logo, 2 - no-avatar, 3 - no-img или путь к другой картинке
            'buttonClass' => 'hidden',                                 // класс кнопки "обновить аватар"/"загрузить аватар" / по умолчанию btm btn-info
            'pluginOptions' => [                                            // настройки плагина
         //       'aspectRatio' => 1 / 1,                                       // установите соотношение сторон рамки обрезки. По умолчанию свободное отношение.
                'strict' => false,                                          // true - рамка не может вызодить за холст, false - может
                'guides' => true,                                           // показывать пунктирные линии в рамке
                'center' => true,                                           // показывать центр в рамке изображения изображения
                'autoCrop' => true,                                         // показывать рамку обрезки при загрузке
                'autoCropArea' => 1,                                      // площидь рамки на холсте изображения при autoCrop (1 = 100% - 0 - 0%)
                'dragCrop' => true,                                         // создание новой рамки при клики в свободное место хоста (false - нельзя)
                'movable' => true,                                          // перемещать изображение холста (false - нельзя)
                'rotatable' => true,                                        // позволяет вращать изображение
                'scalable' => true,                                         // мастабирование изображения
                'zoomable' => false,
            ]], $options['fieldOptions'])).'</div>';

    }

    /**
     * @param $form ActiveForm
     * @param $model
     * @param $options
     * @returns string
     */
    public static function drawTextArea($form, $model, $field, $options = [])
    {
        $options = array_merge([
            'fieldOptions' => []
        ], $options);
        return $form->field($model, $field, $options['fieldOptions'])->textarea(['style' => 'resize:none;', "rows" => "6"]);
    }

    /**
     * @param $form ActiveForm
     * @param $model SearchEditModel
     * @param $options
     * @returns string
     */
    public static function drawForeignInput($form, $model, $field, $options = [])
    {
        $vars = [];
        $options = array_merge([
            'keyName' => 'id',
            'valueName' => 'name',
            'fieldOptions' => []
        ], $options);
        $key = $options['keyName'];
        $val = $options['valueName'];

        foreach ($options['values'] as $obj) {
            $vars[$obj->$key] = $obj->$val;
        }


        return $form->field($model, $field, $options['fieldOptions'])->widget(Select2::className(), [
            'pluginOptions' => [
                'allowClear' => true
            ],
            'options' => ['placeholder'=>''],
            'data'=>$vars

        ]);
    }

    /**
     * @param $form ActiveForm
     * @param $model SearchEditModel
     * @param $field
     * @param $options
     * @returns string
     */
    public static function drawDateInput($form, $model, $field, $options = [])
    {
        $options = array_merge([
            'values_only' => false,
            'data' => [],
            'fieldOptions'=>[]
        ], $options);
        $model->$field = date($model->dateFormat, $model->$field);
        return  $form->field($model, $field)->widget(DatePicker::classname(), [
            'options' => ['placeholder' => $model->getAttributeLabel($field)],
            'name' => 'birth_date',
            'pluginOptions' => array_merge([
                'autoclose'=>true,
                'format' => $model->datePickerFormat
            ], $options['fieldOptions'])
        ]);
    }

    /**
     * @param $form ActiveForm
     * @param $model SearchEditModel
     * @param $options
     * @returns string
     */
    public static function drawCheckBoxInput($form, $model, $field, $options = [])
    {
        $options = array_merge([
            'fieldOptions'=>[]
        ], $options);
        return "<div>".$model->getAttributeLabel($field)."</div>
        <div class='mt-20 inline-block'>".HtmlThemeHelper::checkboxField($form, $model, $field, array_merge(['labelOptions'=>['class' => 'hidden']], $options['fieldOptions']), false)."</div>";
    }

    /**
     * @param $form ActiveForm
     * @param $model
     * @param $options
     * @returns string
     */
    public static function drawWsygInput($form, $model, $field, $options = [])
    {
        return $form->field($model, $field)->widget(Redactor::className(), [
            'clientOptions' => [
                'imageManagerJson' => ['/redactor/upload/image-json'],
                'imageUpload' => ['/redactor/upload/image'],
                'fileUpload' => ['/redactor/upload/file'],

                'lang' => 'ru',
                'plugins' => ['clips', 'counter','definedlinks','filemanager','fontcolor','fontfamily','fontsize','fullscreen','imagemanager','limiter','table','textdirection','textexpander','video']
            ]
        ]);
    }


    /**
     * @param $form ActiveForm
     * @param $model SearchEditModel
     * @param $options
     * @returns string
     */
    public static function drawSelectInput($form, $model, $field, $options = [])
    {
        $vars = [];
        $options = array_merge([
            'values_only' => false,
            'data' => [],
            'fieldOptions'=>[]
        ], $options);

        if ($options['values_only']) {
            foreach ($options['data'] as $value) {
                $vars[$value] = $value;
            }
        } else {
            $vars = $options['data'];
        }


        return $form->field($model, $field, $options['fieldOptions'])->dropDownList($vars);
    }


    /**
     * @param $form ActiveForm
     * @param $model SearchEditModel
     * @param $options
     * @returns string
     */
    public static function drawSelect2Input($form, $model, $field, $options = [])
    {
        $vars = [];
        $options = array_merge([
            'values_only' => false,
            'data' => [],
            'fieldOptions'=>[]
        ], $options);

        if ($options['values_only']) {
            foreach ($options['data'] as $value) {
                $vars[$value] = $value;
            }
        } else {
            $vars = $options['data'];
        }


        return $form->field($model, $field, $options['fieldOptions'])->widget(Select2::className(), [
            'pluginOptions' => [
                'allowClear' => true
            ],
            'options' => ['placeholder'=>''],
            'data'=>$vars
        ]);
    }
}