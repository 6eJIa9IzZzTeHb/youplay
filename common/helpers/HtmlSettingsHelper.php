<?php

namespace common\helpers;

use common\models\User;
use Yii;
use yii\authclient\widgets\AuthChoice;
use yii\helpers\Url;

class HtmlSettingsHelper {

    public static function getCharset() {
        return \Yii::$app->charset;
    }


    public static function getLang() {
        return \Yii::$app->language;
    }

    public static function getPingBackUrl() {
        return Url::home();
    }

    public static function getFavicon() {
        return "/img/icon.png";
    }

    public static function getPageClass() {
        return "page-".self::getViewName();
    }

    public static function getViewName() {
        return  Yii::$app->controller->id.'-'.Yii::$app->controller->action->id;
    }

    public static function getMainLogoUrl() {
        return "img/logo.png";
    }

    public static function getUserProfileUrl() {
        /**
         * @var $user User
         */
        $user = Yii::$app->user->identity;
        return $user->getUrl();
    }


    public static function toDBDecimalDBFormat($value) {
        return number_format($value, 4, '.', '');
    }

    public static function decimalRule($values) {
        return [$values, 'filter', 'filter'=>function($value) {
           return HtmlSettingsHelper::toDBDecimalDBFormat($value);
        }];
    }
}