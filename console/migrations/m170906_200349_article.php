<?php

use yii\db\Migration;

class m170906_200349_article extends Migration
{
    const TABLE_NAME = '{{%article}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'title' => $this->string(),
            'pic' => $this->string()->notNull(),
            'short' => $this->text(),
            'data' => $this->text(),
            'views' => $this->integer()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk-article-category_id-article_category-id', self::TABLE_NAME, 'category_id', 'article_category', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-article-category_id-article_category-id', self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
    }
}
