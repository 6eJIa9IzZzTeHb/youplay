<?php

use yii\db\Migration;

class m170906_200449_tariff_to_user extends Migration
{
    const TABLE_NAME = '{{%tariff_to_user}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'tariff_id' => $this->integer()->notNull(),
            'profit' => $this->decimal(12,4),
            'cost' => $this->decimal(12,4),
            'status' => $this->integer()->defaultValue(0),
            'expired_at' => $this->integer()->notNull(),
            'cron_profit_at' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk-tariff_to_user-user_id-user-id', self::TABLE_NAME, 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-tariff_to_user-tariff_id-tariff-id', self::TABLE_NAME, 'tariff_id', 'tariff', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-tariff_to_user-user_id-user-id', self::TABLE_NAME);
        $this->dropForeignKey('fk-tariff_to_user-tariff_id-tariff-id', self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
    }
}
