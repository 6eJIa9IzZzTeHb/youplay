<?php

use yii\db\Migration;

class m170906_200339_article_categories extends Migration
{
    const TABLE_NAME = '{{%article_category}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'count' => $this->integer()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
