<?php

use yii\db\Migration;

class m170907_150857_user_ip extends Migration
{
    const TABLE_NAME = '{{%user_ip}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'ip' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk-user_ip-user_id-user-id', self::TABLE_NAME, 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-user_ip-user_id-user-id', self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
    }
}
