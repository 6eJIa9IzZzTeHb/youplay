<?php

use yii\db\Migration;

class m170906_200138_option extends Migration
{
    const TABLE_NAME = '{{%option}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'name' => $this->string(),
            'value' => $this->text(),
            'type' => $this->integer()->defaultValue(1),
            'slug' => $this->string(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk-option-category_id-option_category-id', self::TABLE_NAME, 'category_id', 'option_category', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-option-category_id-option_category-id', self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
    }
}
