<?php

use yii\db\Migration;

class m170928_030322_user_like_article extends Migration
{
    const TABLE_NAME = '{{%user_like_article}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'article_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk-user_like_article-user_id-user-id', self::TABLE_NAME, 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-user_like_article-article_id-user-id', self::TABLE_NAME, 'article_id', 'article', 'id', 'CASCADE', 'CASCADE');

        $this->addColumn('article', 'likes', $this->integer()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-user_like_article-user_id-user-id', self::TABLE_NAME);
        $this->dropForeignKey('fk-user_like_article-article_id-user-id', self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);

        $this->dropColumn('article', 'likes');
    }
}
