<?php

use yii\db\Migration;

class m170923_074212_user_paysystems extends Migration
{
    const TABLE_NAME = '{{%user_pay_sys}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'source' => $this->string()->notNull(),
            'source_id' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk-user_pay_sys_-user_id-user-id', self::TABLE_NAME, 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-user_pay_sys_-user_id-user-id', self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
    }
}
