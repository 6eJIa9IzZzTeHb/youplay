<?php

use yii\db\Migration;

class m171017_140933_verify extends Migration
{
    const TABLE_NAME = '{{%user}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'is_verified', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'is_verified');
    }
}
