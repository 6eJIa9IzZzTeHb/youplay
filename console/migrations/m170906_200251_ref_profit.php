<?php

use yii\db\Migration;

class m170906_200251_ref_profit extends Migration
{
    const TABLE_NAME = '{{%referral_profit}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'lvl' => $this->integer()->notNull(),
            'percent' => $this->float()->comment('0.01 = 1%'),
            'type' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
