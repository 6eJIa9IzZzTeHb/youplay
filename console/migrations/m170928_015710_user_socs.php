<?php

use yii\db\Migration;

class m170928_015710_user_socs extends Migration
{
    const TABLE_NAME = '{{%user_socs}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'source' => $this->string()->notNull(),
            'source_id' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk-user_socs_-user_id-user-id', self::TABLE_NAME, 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-user_socs_-user_id-user-id', self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
    }
}
