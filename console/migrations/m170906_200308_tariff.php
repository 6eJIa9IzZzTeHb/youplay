<?php

use yii\db\Migration;

class m170906_200308_tariff extends Migration
{
    const TABLE_NAME = '{{%tariff}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'pic' => $this->string()->defaultValue('/img/user.png'),
            'start_price' => $this->integer(),
            'limit' => $this->integer(),
            'desc' => $this->string(),
            'percent' => $this->float()->notNull(),
            'time' => $this->integer()->notNull(),
            'times' => $this->integer()->defaultValue(0),
            'back_dep' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
