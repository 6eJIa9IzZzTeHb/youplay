<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{

    const TABLE_NAME = '{{%user}}';


    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'phone' => $this->string(),
            'pic' => $this->string(),


            'role'=> $this->integer(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'admin_expired_at' => $this->integer()->notNull(),
            'email_expired_at' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),

            'refer' => $this->integer(),
            'structure_size' => $this->integer(),

            'balance' => $this->decimal(12,4),
            'structure_invest' => $this->decimal(12,4),
            'total_profit' => $this->decimal(12,4),
            'profit_invest' => $this->decimal(12,4),
            'withdraw' => $this->decimal(12,4),

        ], $tableOptions);


        $this->addForeignKey('fk-refer-user-id', self::TABLE_NAME, 'refer', self::TABLE_NAME, 'id');
    }

    public function down()
    {
        $this->dropForeignKey('fk-refer-user-id', self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
    }
}
