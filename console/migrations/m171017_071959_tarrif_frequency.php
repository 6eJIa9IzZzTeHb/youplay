<?php

use yii\db\Migration;

class m171017_071959_tarrif_frequency extends Migration
{
    const TABLE_NAME = '{{%tariff}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'end_price', $this->decimal(10, 4));
        $this->addColumn(self::TABLE_NAME, 'frequency', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'end_price');
        $this->dropColumn(self::TABLE_NAME, 'frequency');
    }
}
