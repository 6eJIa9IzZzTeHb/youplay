<?php

use yii\db\Migration;

class m170906_200146_faq extends Migration
{
    const TABLE_NAME = '{{%faq}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'value' => $this->text(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }

}
