<?php

use yii\db\Migration;

class m170906_200127_opt_category extends Migration
{
    const TABLE_NAME = '{{%option_category}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }

}
