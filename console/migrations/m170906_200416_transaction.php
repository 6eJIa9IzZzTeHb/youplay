<?php

use yii\db\Migration;

class m170906_200416_transaction extends Migration
{
    const TABLE_NAME = '{{%transaction}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'status' => $this->integer(),
            'type' => $this->integer(),
            'data' => $this->text(),
            'amount' => $this->decimal(12, 4),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk-transaction-user_id-user-id', self::TABLE_NAME, 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-transaction-user_id-user-id', self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
    }
}
